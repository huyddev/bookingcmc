package com.cmcglobal.msbs.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
@JsonInclude(Include.NON_NULL)
public class RoleDTO extends AuditingDTO{
	private int id;
	private String name;
	private List<UserDTO> users;
	private String status;
	private UserDTO createBy;
	private UserDTO updateBy;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<UserDTO> getUsers() {
		return users;
	}

	public void setUsers(List<UserDTO> users) {
		this.users = users;
	}

	public UserDTO getCreateBy() {
		return createBy;
	}

	public void setCreateBy(UserDTO createBy) {
		this.createBy = createBy;
	}

	public UserDTO getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(UserDTO updateBy) {
		this.updateBy = updateBy;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
