package com.cmcglobal.msbs.controller;

import static com.cmcglobal.msbs.utils.Constants.STATUS_APPROVE;
import static com.cmcglobal.msbs.utils.Constants.STATUS_REJECT;
import static com.cmcglobal.msbs.utils.Error.ERROR_UPDATE;
import static com.cmcglobal.msbs.utils.Error.OBJECT_NULL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cmcglobal.msbs.config.JwtTokenUtils;
import com.cmcglobal.msbs.entity.UserBook;
import com.cmcglobal.msbs.entity.BookItem;
import com.cmcglobal.msbs.entity.BookType;
import com.cmcglobal.msbs.entity.User;
import com.cmcglobal.msbs.model.ApiResponse;
import com.cmcglobal.msbs.model.UserBookDTO;
import com.cmcglobal.msbs.model.DateRequest;
import com.cmcglobal.msbs.model.ErrorObject;
import com.cmcglobal.msbs.model.Mail;
import com.cmcglobal.msbs.model.UserDTO;
import com.cmcglobal.msbs.service.UserBookService;
import com.cmcglobal.msbs.service.BookTypeService;
import com.cmcglobal.msbs.service.MailService;
import com.cmcglobal.msbs.service.UserService;

@CrossOrigin(origins = "*")
@RestController
public class UserBookController {
	@Autowired
	private UserBookService userBookService;

	@Autowired
	private BookTypeService bookTypeService;

	@Autowired
	private UserService userService;

	@Autowired
	private JwtTokenUtils JwtTokenUtils;

	@Autowired
	private MailService emailService;

	@GetMapping(value = "/list-user-book")
	private ResponseEntity<ApiResponse> getAll(HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		if (userBookService.getAll() != null) {
			response.setData(userBookService.getAll());
			response.setMessage("GET ALL");
		} else {
			ErrorObject error = new ErrorObject(1, "list null");
			response.setError(error);
			response.setMessage("error usesr book");
		}
		return new ResponseEntity<ApiResponse>(response,
				userBookService.getAll() != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@GetMapping(value = "/user-book")
	private ResponseEntity<ApiResponse> getById(@RequestParam("id") Integer id) {
		ApiResponse response = new ApiResponse();
		UserBookDTO userBookDTO = userBookService.getById(id);
		if (userBookDTO != null) {
			response.setData(userBookDTO);
			response.setMessage("GET BY ID");
		} else {
			ErrorObject error = new ErrorObject(1, "Object is null");
			response.setError(error);
			response.setMessage("error");
		}
		return new ResponseEntity<ApiResponse>(response,
				userBookDTO != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping(value = "/user-book")
	private ResponseEntity<ApiResponse> create(@RequestBody UserBookDTO userBookDTO, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		String headers = request.getHeader("Authorization");
		String token = null;
		if (headers != null && headers.startsWith("Token ")) {
			token = headers.substring(6);
		}
		int user_id = JwtTokenUtils.getIdFromToken(token);
		UserDTO user = userService.getById(user_id);
		userBookDTO.setUser(user);
		UserBook userBook = userBookService.save(userBookDTO);
		response.setData(userBookService.getById(userBook.getId()));
		response.setMessage("CREATE USER BOOK");
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	@PutMapping(value = "/user-book")
	private ResponseEntity<ApiResponse> update(@RequestBody UserBookDTO userBookDTO, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		UserBook userBook = userBookService.update(userBookDTO);
		response.setData(userBook);
		response.setMessage("UPDATE USER BOOK");
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	@DeleteMapping(value = "/user-book")
	private ResponseEntity<ApiResponse> delete(@RequestBody UserBookDTO userBook) {
		ApiResponse response = new ApiResponse();
		userBookService.delete(userBook);
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("delete", "OK");
		response.setData(data);
		response.setMessage("DELETE USER BOOK");
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	@PostMapping(value = "/user-book-by-date")
	private ResponseEntity<ApiResponse> getUserBookByDate(@RequestBody DateRequest date) {
		ApiResponse response = new ApiResponse();
		List<UserBookDTO> listUserBook = null;
		if (userBookService.getByDate(date.getDate()) == null) {
			ErrorObject error = OBJECT_NULL;
			response.setError(error);
			response.setMessage("error");
		} else {
			listUserBook = userBookService.getByDate(date.getDate());
			response.setData(listUserBook);
			response.setMessage("GET BY DATE");
		}

		return new ResponseEntity<ApiResponse>(response,
				listUserBook != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping(value = "/user-book-by-month")
	private ResponseEntity<ApiResponse> getUserBookByMonth(@RequestBody DateRequest date) {
		ApiResponse response = new ApiResponse();
		List<UserBookDTO> listUserBook = null;
		if (userBookService.getByMonth(date.getDate()) == null) {
			ErrorObject error = OBJECT_NULL;
			response.setError(error);
			response.setMessage("error");
		} else {
			listUserBook = userBookService.getByMonth(date.getDate());
			response.setData(listUserBook);
			response.setMessage("GET BY MONTH");
		}
		System.out.println(response);
		return new ResponseEntity<ApiResponse>(response,
				listUserBook != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping(value = "/user-book-by-week")
	private ResponseEntity<ApiResponse> getUserBookByWeek(@RequestBody DateRequest date) {
		ApiResponse response = new ApiResponse();
		List<UserBookDTO> listUserBook = null;
		if (userBookService.getByWeek(date.getDate()) == null) {
			ErrorObject error = OBJECT_NULL;
			response.setError(error);
			response.setMessage("error");
		} else {
			listUserBook = userBookService.getByWeek(date.getDate());
			response.setData(listUserBook);
			response.setMessage("GET BY WEEK");
		}
		return new ResponseEntity<ApiResponse>(response,
				listUserBook != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping(value = "/admin/user-book-by-month")
	private ResponseEntity<ApiResponse> getUserBookAdminByMonth(@RequestBody DateRequest date) {
		ApiResponse response = new ApiResponse();
		List<UserBookDTO> listUserBook = null;
		if (userBookService.getByMonth(date.getDate(), PageRequest.of(date.getSelectIndex(), date.getSize())) == null) {
			ErrorObject error = OBJECT_NULL;
			response.setError(error);
			response.setMessage("error");
		} else {
			listUserBook = userBookService.getByMonth(date.getDate(),
					PageRequest.of(date.getSelectIndex(), date.getSize()));
			Map<String, Object> data = new HashMap<>();
			data.put("list", listUserBook);
			int sizePages = 0;
			int count = userBookService.getCountByMonth(date.getDate());
			if (count % date.getSize() == 0) {
				sizePages = count / date.getSize();
			} else {
				sizePages = (count / date.getSize()) + 1;
			}

			data.put("sizePages", sizePages);
			response.setData(data);
			response.setMessage("GET BY MONTH");
		}

		return new ResponseEntity<ApiResponse>(response,
				listUserBook != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping(value = "/admin/user-book-by-week")
	private ResponseEntity<ApiResponse> getUserBookAdminByWeek(@RequestBody DateRequest date) {
		ApiResponse response = new ApiResponse();
		List<UserBookDTO> listUserBook = null;
		if (userBookService.getByWeek(date.getDate(), PageRequest.of(date.getSelectIndex(), date.getSize())) == null) {
			ErrorObject error = OBJECT_NULL;
			response.setError(error);
			response.setMessage("error");
		} else {
			listUserBook = userBookService.getByWeek(date.getDate(),
					PageRequest.of(date.getSelectIndex(), date.getSize()));
			Map<String, Object> data = new HashMap<>();
			data.put("list", listUserBook);
			int sizePages = 0;
			int count = userBookService.getCountByWeek(date.getDate());
			if (count % date.getSize() == 0) {
				sizePages = count / date.getSize();
			} else {
				sizePages = (count / date.getSize()) + 1;
			}

			data.put("sizePages", sizePages);
			response.setData(data);
			response.setMessage("GET BY WEEK");
		}

		return new ResponseEntity<ApiResponse>(response,
				listUserBook != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping(value = "/admin/user-book-by-date")
	private ResponseEntity<ApiResponse> getUserBookAdminByDate(@RequestBody DateRequest date) {
		ApiResponse response = new ApiResponse();
		List<UserBookDTO> listUserBook = null;
		if (userBookService.getByDate(date.getDate(), PageRequest.of(date.getSelectIndex(), date.getSize())) == null) {
			ErrorObject error = OBJECT_NULL;
			response.setError(error);
			response.setMessage("error");
		} else {
			listUserBook = userBookService.getByDate(date.getDate(),
					PageRequest.of(date.getSelectIndex(), date.getSize()));
			Map<String, Object> data = new HashMap<>();
			data.put("list", listUserBook);
			int sizePages = 0;
			int count = userBookService.getCountByBookingDate(date.getDate());
			if (count % date.getSize() == 0) {
				sizePages = count / date.getSize();
			} else {
				sizePages = (count / date.getSize()) + 1;
			}

			data.put("sizePages", sizePages);
			response.setData(data);
			response.setMessage("GET BY DATE");
		}

		return new ResponseEntity<ApiResponse>(response,
				listUserBook != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@GetMapping(value = "/user-book-history")
	private ResponseEntity<ApiResponse> UserBookHistory(HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		final String requestTokenHeader = request.getHeader("Authorization");
		String jwtToken = null;
		if (requestTokenHeader != null && requestTokenHeader.startsWith("Token "))
			jwtToken = requestTokenHeader.substring(6);
		int user_id = JwtTokenUtils.getIdFromToken(jwtToken);
		User user = userService.findById(user_id);
		List<UserBookDTO> userBooks = userBookService.getByUser(user);
		if (userBooks != null) {
			response.setData(userBooks);
			response.setMessage("BOOKING HISTORY");
		} else {
			response.setError(OBJECT_NULL);
			response.setMessage("error");
		}
		return new ResponseEntity<ApiResponse>(response,
				userBooks != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@GetMapping(value = "/user-book-update-status")
	private ResponseEntity<ApiResponse> updateStatusUserBook(@RequestParam("id") Integer id,
			@RequestParam("status") String status, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		final String header = request.getHeader("Authorization");
		String jwtToken = null;
		if (header != null && header.startsWith("Token ")) {
			jwtToken = header.substring(6);
		}
		int user_id = JwtTokenUtils.getIdFromToken(jwtToken);
		UserBookDTO userBook = userBookService.getById(id);
		UserDTO admin = userService.getById(user_id);
		userBook.setAdmin(admin);
		if (status.equals(STATUS_APPROVE)) {
			BookItem bookItem = new BookItem();
			bookItem.setId(userBook.getBookItem().getId());
			List<UserBookDTO> listUserBook = userBookService.getByTimeFromAndTimeTo(userBook.getTimeFrom(),
					userBook.getTimeTo(), bookItem);
			for (UserBookDTO b : listUserBook) {
				b.setAdmin(admin);
				userBookService.updateStatus(b, STATUS_REJECT);
			}
		}
		userBookService.updateStatus(userBook, status);
		response.setData(userBookService.getAll());
		response.setMessage("UPDATE STATUS");
		return new ResponseEntity<ApiResponse>(response,
				userBook != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@GetMapping(value = "/admin/user-book-update-status")
	private ResponseEntity<ApiResponse> updateStatusUserBookAdmin(@RequestParam("id") Integer id,
			@RequestParam("status") String status, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		final String header = request.getHeader("Authorization");
		String jwtToken = null;
		if (header != null && header.startsWith("Token ")) {
			jwtToken = header.substring(6);
		}
		int user_id = JwtTokenUtils.getIdFromToken(jwtToken);
		UserBookDTO userBook = userBookService.getById(id);
		UserDTO admin = userService.getById(user_id);
		UserDTO user = userService.getById(userBook.getUser().getId());
		userBook.setAdmin(admin);

		if (status.equals(STATUS_APPROVE)) {
			BookItem bookItem = new BookItem();
			bookItem.setId(userBook.getBookItem().getId());
			List<UserBookDTO> listUserBook = userBookService.getByTimeFromAndTimeTo(userBook.getTimeFrom(),
					userBook.getTimeTo(), bookItem);
			for (UserBookDTO b : listUserBook) {
				b.setAdmin(admin);
				userBookService.updateStatus(b, STATUS_REJECT);
				// send mail
				if (b.getId() != userBook.getId()) {
					Mail mail = setDataMail(userBook, user, admin);
					emailService.sendBookingMessage(mail);
				}
			}
			userBookService.updateStatus(userBook, status);
			// send mail
			Mail mail = setDataMail(userBook, user, admin);
			emailService.sendBookingMessage(mail);
			response.setData(
					userBookService.getByTimeFromAndTimeTo(userBook.getTimeFrom(), userBook.getTimeTo(), bookItem));
			response.setMessage("UPDATE STATUS");
		} else {
			userBookService.updateStatus(userBook, status);
			// send mail
			Mail mail = setDataMail(userBook, user, admin);
			emailService.sendBookingMessage(mail);
			List<UserBookDTO> list = new ArrayList<UserBookDTO>();
			list.add(userBookService.getById(userBook.getId()));
			response.setData(list);
			response.setMessage("UPDATE STATUS");
		}
		return new ResponseEntity<ApiResponse>(response,
				userBook != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping(value = "/user-book-by-time")
	private ResponseEntity<ApiResponse> getUserBookByTime(@RequestBody UserBook b) {
		ApiResponse response = new ApiResponse();
		List<UserBookDTO> list = null;
		b = userBookService.findById(b.getId());
		if (userBookService.getByTimeFromAndTimeTo(b.getTimeFrom(), b.getTimeTo(), b.getBookItem()) != null) {
			list = userBookService.getByTimeFromAndTimeTo(b.getTimeFrom(), b.getTimeTo(), b.getBookItem());
			response.setData(list);
			response.setMessage("OK");
		} else {
			response.setError(OBJECT_NULL);
			response.setMessage("error");
		}
		return new ResponseEntity<ApiResponse>(response,
				list != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@GetMapping(value = "/get-by-booktype")
	private ResponseEntity<ApiResponse> getByBookType(@RequestParam("id") Integer id) {
		ApiResponse response = new ApiResponse();
		try {
			BookType bookType = bookTypeService.findById(id);
			List<UserBookDTO> list = userBookService.getByBookType(bookType);
			response.setData(list);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("Booktype or list Userbook null" + e);
			response.setError(OBJECT_NULL);
		}
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	private Mail setDataMail(UserBookDTO userBook, UserDTO user, UserDTO admin) {
		Mail mail = new Mail();
		mail.setFrom("CMC_Global");
		mail.setTo(user.getMail());
		mail.setSubject("User Book");
		Map<String, Object> model = new HashMap<>();
		model.put("user", user);
		model.put("admin", admin);
		model.put("status", userBook.getStatus());
		model.put("room", userBook.getBookItem());
		model.put("rejectReason", userBook.getRejectReason());
		model.put("signature", "CMC Global");
		mail.setModel(model);
		return mail;
	}
	
	@PostMapping(value = "get-by-date-item") 
	private ResponseEntity<ApiResponse> getByDateAndItem(@RequestBody DateRequest date){
		//Danh cho mobile
		ApiResponse response = new ApiResponse();
		List<UserBookDTO> listUserBook = null;
		if (userBookService.getByDate(date.getDate()) == null) {
			ErrorObject error = OBJECT_NULL;
			response.setError(error);
			response.setMessage("error");
		} else {
			listUserBook = userBookService.getByDate(date.getDate(), date.getBookItemId());
			response.setData(listUserBook);
			response.setMessage("GET BY DATE");
		}

		return new ResponseEntity<ApiResponse>(response,
				listUserBook != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
