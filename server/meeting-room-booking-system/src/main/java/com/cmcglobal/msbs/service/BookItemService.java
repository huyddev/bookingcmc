package com.cmcglobal.msbs.service;

import java.util.List;

import com.cmcglobal.msbs.entity.BookItem;
import com.cmcglobal.msbs.model.BookItemDTO;
import com.cmcglobal.msbs.model.BookTypeDTO;

public interface BookItemService {
	
	BookItem save(BookItemDTO bookItemDTO);

	BookItem update(BookItemDTO bookItemDTO);
	
	BookItem update(BookItem bookItem);

	void delete(BookItemDTO bookItemDTO);

	BookItem findById(int id);

	BookItemDTO getById(int id);

	List<BookItemDTO> getAll();
	
	int getNumberBookItem();
	
	int getNumberBookItemByStatus(String status);
	
	List<BookItemDTO> getByBookType(BookTypeDTO bookTypeDTO);
}
