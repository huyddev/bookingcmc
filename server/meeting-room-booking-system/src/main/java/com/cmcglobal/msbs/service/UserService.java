package com.cmcglobal.msbs.service;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.cmcglobal.msbs.entity.User;
import com.cmcglobal.msbs.model.GroupDTO;
import com.cmcglobal.msbs.model.UserDTO;

@Service
public interface UserService {

	User save(UserDTO user);

	User save(UserDTO user, HttpServletRequest request);

	User update(UserDTO user);

	User updateUser(UserDTO userDTO);

	User changePass(UserDTO userDTO);

	void delete(UserDTO userDTO);

	UserDTO getById(int id);

	User updateImage(UserDTO userDTO);

	UserDTO getByMail(String mail);

	UserDTO getUserPasswordByMail(String mail);

	List<UserDTO> getAll();

	User findByMail(String mail);

	User findById(int id);

	List<UserDTO> searchUserByName(String name);
	
	List<UserDTO> checkUserMail(String name);

	boolean checkMail(String mail);

	int getCountUser();

	int getCountUserActive();

	UserDTO getUserByToken(String token);

	void forgotPassword(UserDTO userDTO, HttpServletRequest request);
	
	List<UserDTO> getUserByTimeAndGroup(GroupDTO group, Date timeFrom, Date timeTo);
	
	List<UserDTO> getByTime(Date timeFrom, Date timeTo);
	
	int getCountUserByTimeGroupStatus(GroupDTO group, Date timeFrom, Date timeTo, String status);
	
	int getCountUserByTimeStatus(Date timeFrom, Date timeTo, String status);
}
