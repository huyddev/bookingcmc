package com.cmcglobal.msbs.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cmcglobal.msbs.entity.Post;

@Repository
public interface PostDao extends JpaRepository<Post, Integer> {
	@Query("SELECT u FROM Post u WHERE u.status = 'ACTIVE'")
	List<Post> findActiveStatus();
}
