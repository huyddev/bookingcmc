package com.cmcglobal.msbs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmcglobal.msbs.dao.PostDao;
import com.cmcglobal.msbs.entity.Post;
import com.cmcglobal.msbs.entity.User;
import com.cmcglobal.msbs.model.PostDTO;
import com.cmcglobal.msbs.model.UserDTO;
import com.cmcglobal.msbs.service.PostService;
import com.cmcglobal.msbs.service.UserService;

import static com.cmcglobal.msbs.utils.Constants.STATUS_INACTIVE;
import static com.cmcglobal.msbs.utils.Constants.STATUS_ACTIVE;

@Service(value = "postService")
public class PostServiceImpl implements PostService {

	@Autowired
	private PostDao postDao;

	@Autowired
	private UserService userService;

	@Override
	public Post save(PostDTO postDTO) {
		postDTO.setStatus(STATUS_ACTIVE);
		if (postDao.save(convertEntity(postDTO)) != null) {
			return convertEntity(postDTO);
		}
		return null;
	}

	@Override
	public Post update(PostDTO postDTO) {
		postDTO.setCreateTime(findById(postDTO.getId()).getCreatedDate());
		if (postDao.save(convertEntity(postDTO)) != null) {
			return convertEntity(postDTO);
		}
		return null;
	}

	@Override
	public void delete(PostDTO postDTO) {
		postDTO.setStatus(STATUS_INACTIVE);
		postDao.save(convertEntity(postDTO));
	}

	@Override
	public PostDTO getById(int id) {
		return convertDTO(findById(id));
	}

	@Override
	public Post findById(int id) {
		return postDao.findById(id).get();
	}

	@Override
	public List<PostDTO> getAll() {
		if (postDao.findAll() != null) {
			List<PostDTO> postDTOs = new ArrayList<>();
			for (Post post : postDao.findAll()) {
				postDTOs.add(convertDTO(post));
			}
			return postDTOs;
		}
		return null;
	}

	@Override
	public List<PostDTO> getAllActive() {
		if (postDao.findActiveStatus() != null) {
			List<PostDTO> postDTOs = new ArrayList<>();
			for (Post post : postDao.findActiveStatus()) {
				postDTOs.add(convertDTO(post));
			}
			return postDTOs;
		}
		return null;
	}

	private PostDTO convertDTO(Post post) {
		PostDTO postDTO = new PostDTO();
		postDTO.setId(post.getId());
		postDTO.setTitle(post.getTitle());
		if (post.getAuthor() != null) {
			User createBy = userService.findById(post.getAuthor().getId());
			UserDTO author = new UserDTO(createBy.getId(), createBy.getFullname());
			author.setAvata(createBy.getAvata());
			postDTO.setAuthor(author);
		}
		if (post.getUpdateBy() != null) {
			User updateBy = userService.findById(post.getUpdateBy().getId());
			postDTO.setUpdateBy(new UserDTO(updateBy.getId(), updateBy.getFullname()));
		}
		postDTO.setBrief(post.getBrief());
		postDTO.setContent(post.getContent());
		postDTO.setCreateTime(post.getCreatedDate());
		postDTO.setUpdateTime(post.getLastModifiedDate());
		postDTO.setStatus(post.getStatus());
		postDTO.setImage(post.getImage());
		return postDTO;
	}

	private Post convertEntity(PostDTO postDTO) {
		Post post = new Post();
		post.setId(postDTO.getId());
		post.setTitle(postDTO.getTitle());
		if (postDTO.getAuthor() != null) {
			User createBy = userService.findById(postDTO.getAuthor().getId());
			post.setAuthor(new User(createBy.getId(), createBy.getFullname()));
		}
		if (postDTO.getUpdateBy() != null) {
			User updateBy = userService.findById(postDTO.getUpdateBy().getId());
			post.setUpdateBy(new User(updateBy.getId(), updateBy.getFullname()));
		}
		post.setBrief(postDTO.getBrief());
		post.setContent(postDTO.getContent());
		post.setCreatedDate(postDTO.getCreateTime());
		post.setLastModifiedDate(post.getLastModifiedDate());
		post.setStatus(postDTO.getStatus());
		post.setImage(postDTO.getImage());
		return post;
	}

}
