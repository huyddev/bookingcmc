package com.cmcglobal.msbs.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmcglobal.msbs.dao.GroupDao;
import com.cmcglobal.msbs.entity.Group;
import com.cmcglobal.msbs.entity.User;
import com.cmcglobal.msbs.model.GroupDTO;
import com.cmcglobal.msbs.model.UserDTO;
import com.cmcglobal.msbs.service.GroupService;
import com.cmcglobal.msbs.service.UserService;
import static com.cmcglobal.msbs.utils.Constants.STATUS_ACTIVE;
import static com.cmcglobal.msbs.utils.Constants.STATUS_INACTIVE;

@Service
public class GroupServiceImpl implements GroupService {

	@Autowired
	private GroupDao groupDao;

	@Autowired
	private UserService userService;

	@Override
	public Group save(GroupDTO groupDTO) {
		groupDTO.setStatus(STATUS_ACTIVE);
		if (groupDao.save(convertEntity(groupDTO)) != null) {
			return convertEntity(groupDTO);
		}
		return null;
	}

	@Override
	public Group update(GroupDTO groupDTO) {
		groupDTO.setCreateTime(findById(groupDTO.getId()).getCreatedDate());
		if (groupDao.save(convertEntity(groupDTO)) != null) {
			return convertEntity(groupDTO);
		}
		return null;
	}

	@Override
	public void delete(GroupDTO groupDTO) {
		groupDTO.setStatus(STATUS_INACTIVE);
		groupDao.save(convertEntity(groupDTO));
	}

	@Override
	public Group findById(int id) {
		return groupDao.findById(id).get();
	}

	@Override
	public GroupDTO getById(int id) {
		return convertDTO(findById(id));
	}

	@Override
	public List<GroupDTO> getByName(String name) {
		if (groupDao.getByName(name) != null) {
			List<GroupDTO> groups = new ArrayList<>();
			for (Group group : groupDao.getByName(name)) {
				groups.add(convertDTO(group));
			}
			return groups;
		}
		return null;
	}

	@Override
	public List<GroupDTO> getAll() {
		if (groupDao.findAll() != null) {
			List<GroupDTO> groups = new ArrayList<>();
			for (Group group : groupDao.findAll()) {
				groups.add(convertDTO(group));
			}
			return groups;
		}
		return null;
	}

	private GroupDTO convertDTO(Group group) {
		GroupDTO groupDTO = new GroupDTO();
		groupDTO.setId(group.getId());
		groupDTO.setName(group.getName());
		if (group.getCreateBy() != null) {
			User createBy = userService.findById(group.getCreateBy().getId());
			groupDTO.setCreateBy(new UserDTO(createBy.getId(), createBy.getFullname()));
		}
		if (group.getUpdateBy() != null) {
			User updateBy = userService.findById(group.getUpdateBy().getId());
			groupDTO.setUpdateBy(new UserDTO(updateBy.getId(), updateBy.getFullname()));
		}
		if (group.getManager() != null) {
			groupDTO.setManagerId(new UserDTO(group.getManager().getId(), group.getManager().getFullname()));
		}
		groupDTO.setCreateTime(group.getCreatedDate());
		groupDTO.setUpdateTime(group.getLastModifiedDate());
		if (group.getListUser() != null) {
			List<UserDTO> users = new ArrayList<>();
			for (User user : group.getListUser()) {
				if (userService.findById(user.getId()) != null) {
					User userEntity = userService.findById(user.getId());
					users.add(new UserDTO(userEntity.getId(), userEntity.getFullname()));
				}
			}
			groupDTO.setListUser(users);
		}
		if (group.getParentGroup() != null) {
			groupDTO.setParentGroup(new GroupDTO(group.getParentGroup().getId(), group.getParentGroup().getName()));
		}
		groupDTO.setStatus(group.getStatus());
		if (group.getSubGroup() != null) {
			List<GroupDTO> listGroup = new ArrayList<>();
			for (Group subGroup : group.getSubGroup()) {
				listGroup.add(convertDTO(subGroup));
			}
			groupDTO.setSubGroup(listGroup);
		}
		return groupDTO;
	}

	private Group convertEntity(GroupDTO groupDTO) {
		Group group = new Group();
		group.setId(groupDTO.getId());
		group.setName(groupDTO.getName());
		if (groupDTO.getCreateBy() != null) {
			User createBy = userService.findById(groupDTO.getCreateBy().getId());
			group.setCreateBy(new User(createBy.getId(), createBy.getFullname()));
		}
		if (groupDTO.getUpdateBy() != null) {
			User updateBy = userService.findById(groupDTO.getUpdateBy().getId());
			group.setUpdateBy(new User(updateBy.getId(), updateBy.getFullname()));
		}
		if (groupDTO.getManagerId() != null) {
			group.setManager(new User(groupDTO.getManagerId().getId(), groupDTO.getManagerId().getFullname()));
		}
		if (groupDTO.getListUser() != null) {
			List<User> users = new ArrayList<>();
			for (UserDTO userDTO : groupDTO.getListUser()) {
				if (userService.findById(userDTO.getId()) != null) {
					User userEntity = userService.findById(userDTO.getId());
					users.add(new User(userEntity.getId(), userEntity.getFullname()));
				}
			}
			group.setListUser(users);
		}
		group.setCreatedDate(groupDTO.getCreateTime());
		group.setLastModifiedDate(group.getLastModifiedDate());
		if (groupDTO.getParentGroup() != null) {
			group.setParentGroup(new Group(groupDTO.getParentGroup().getId(), groupDTO.getParentGroup().getName()));
		}
		if (groupDTO.getSubGroup() != null) {
			List<Group> listGroup = new ArrayList<>();
			for (GroupDTO subGroup : groupDTO.getSubGroup()) {
				listGroup.add(groupDao.findById(subGroup.getId()).get());
			}
			group.setSubGroup(listGroup);
		}
		group.setStatus(groupDTO.getStatus());
		return group;
	}
}
