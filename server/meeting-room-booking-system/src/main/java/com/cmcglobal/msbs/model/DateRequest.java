package com.cmcglobal.msbs.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author LuongChinh
 *
 */
public class DateRequest {
	private String date;
	private int size;
	private int selectIndex;
	private int bookItemId;

	public Date getDate() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = format.parse(this.date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getSelectIndex() {
		return selectIndex;
	}

	public void setSelectIndex(int selectIndex) {
		this.selectIndex = selectIndex;
	}

	public int getBookItemId() {
		return bookItemId;
	}

	public void setBookItemId(int bookItemId) {
		this.bookItemId = bookItemId;
	}

}
