package com.cmcglobal.msbs.service;

import java.util.List;

import com.cmcglobal.msbs.entity.BookType;
import com.cmcglobal.msbs.model.BookTypeDTO;

public interface BookTypeService {
	BookType save(BookTypeDTO bookTypeDTO);

	BookType update(BookTypeDTO bookTypeDTO);

	void delete(BookTypeDTO bookTypeDTO);

	BookType findById(int id);

	BookTypeDTO getById(int id);

	List<BookTypeDTO> getAll();
	
	List<BookType> findAll();
	
	List<BookTypeDTO> getByStatus(String status);
	
	List<BookType> findByStatus(String status);
}
