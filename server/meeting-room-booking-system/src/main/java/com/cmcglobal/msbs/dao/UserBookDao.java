package com.cmcglobal.msbs.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cmcglobal.msbs.entity.BookItem;
import com.cmcglobal.msbs.entity.BookType;
import com.cmcglobal.msbs.entity.Group;
import com.cmcglobal.msbs.entity.User;
import com.cmcglobal.msbs.entity.UserBook;

public interface UserBookDao extends JpaRepository<UserBook, Integer> {

	@Query("SELECT u FROM UserBook u WHERE u.bookingDate = ?1 ORDER BY u.createdDate ASC")
	List<UserBook> findByBookingDate(Date date);

	@Query("SELECT u FROM UserBook u WHERE u.bookingDate = ?1 ORDER BY u.createdDate ASC")
	List<UserBook> findByBookingDate(Date date, Pageable page);

	@Query("SELECT u FROM UserBook u WHERE MONTH(u.bookingDate) = MONTH(?1) ORDER BY u.createdDate ASC")
	List<UserBook> findUserBookByMonth(Date date);

	@Query("SELECT u FROM UserBook u WHERE MONTH(u.bookingDate) = MONTH(?1) ORDER BY u.createdDate ASC")
	List<UserBook> findByBookingMonth(Date date, Pageable page);

	@Query("SELECT u FROM UserBook u WHERE WEEKOFYEAR(u.bookingDate) = WEEKOFYEAR(?1) " + "ORDER BY u.createdDate ASC")
	List<UserBook> findUserBookByWeek(Date date);

	@Query("SELECT u FROM UserBook u WHERE WEEKOFYEAR(u.bookingDate) = WEEKOFYEAR(?1) " + "ORDER BY u.createdDate ASC")
	List<UserBook> findByBookingWeek(Date date, Pageable page);

	@Query("SELECT count(u) FROM UserBook u WHERE u.bookingDate = ?1")
	Integer getCountByBookingDate(Date date);

	@Query("SELECT count(u) FROM UserBook u WHERE WEEK(u.bookingDate) = WEEK(?1)")
	Integer getCountByWeek(Date date);
	@Query("SELECT count(u) FROM UserBook u WHERE MONTH(u.bookingDate) = MONTH(?1)")
	Integer getCountByMonth(Date date);

	@Query("SELECT count(u) FROM UserBook u WHERE u.bookingDate = ?1 AND u.status = ?2")
	Integer getCountByBookingDateStatus(Date date, String status);

	@Query("SELECT count(u) FROM UserBook u WHERE WEEK(u.bookingDate) = WEEK(?1) AND u.status = ?2")
	Integer getCountByWeekStatus(Date date, String status);

	@Query("SELECT count(u) FROM UserBook u WHERE MONTH(u.bookingDate) = MONTH(?1) AND u.status = ?2")
	Integer getCountByMonthStatus(Date date, String status);

	@Query("SELECT u FROM UserBook u WHERE ((u.timeFrom >= ?1 AND u.timeFrom < ?2) OR (u.timeTo > ?1 AND u.timeTo <= ?2)) AND u.bookItem = ?3 ORDER BY u.createdDate ASC")
	List<UserBook> findByTimeFromAndTimeTo(Date timeFrom, Date timeTo, BookItem bookItem);

	@Query("SELECT u FROM UserBook u WHERE u.user = ?1 AND u.status = ?2")
	List<UserBook> findByUser(User user, String status);

	@Query("SELECT u FROM UserBook u WHERE u.user = ?1")
	List<UserBook> findByUser(User user);
	@Query("SELECT count(u) FROM UserBook u")
	int getCountUserBook();

	@Query("SELECT u FROM UserBook u "
			+ "WHERE u.user.group = ?1 AND u.createdDate >= ?2 AND u.createdDate <= ?3 AND u.bookItem.bookType = ?4")
	List<UserBook> findByTimeAndGroup(Group group, Date timeFrom, Date timeTo, BookType bookType);

	@Query("SELECT u FROM UserBook u WHERE u.createdDate >= ?1 AND u.createdDate <= ?2 AND u.bookItem.bookType = ?3")
	List<UserBook> findByTime(Date timeFrom, Date timeTo, BookType bookType);

	@Query("SELECT count(u) FROM UserBook u WHERE u.user.group = ?1 AND u.createdDate >= ?2 "
			+ "AND u.createdDate <= ?3 AND u.status = ?4 AND u.bookItem.bookType = ?5")
	Integer getCountUserBookByTimeGroupStatus(Group group, Date timeFrom, Date timeTo, String status,
			BookType bookType);

	@Query("SELECT count(u) FROM UserBook u WHERE u.createdDate >= ?1 AND u.createdDate <= ?2 AND u.status = ?3 AND u.bookItem.bookType = ?4")
	Integer getCountUserBookByTimeStatus(Date timeFrom, Date timeTo, String status, BookType bookType);

	@Query("SELECT SUM(u.hours) FROM UserBook u WHERE u.user.group = ?1 AND u.createdDate >= ?2 "
			+ "AND u.createdDate <= ?3 AND u.status = ?4 AND u.bookItem.bookType = ?5")
	Integer getHourseUserBookGroupStatus(Group group, Date timeFrom, Date timeTo, String status, BookType bookType);

	@Query("SELECT SUM(u.hours) FROM UserBook u WHERE u.createdDate >= ?1 AND u.createdDate <= ?2 AND u.status = ?3 AND u.bookItem.bookType = ?4")
	Integer getHourseUserBookStatus(Date timeFrom, Date timeTo, String status, BookType bookType);

	@Query("SELECT SUM(u.hours) FROM UserBook u WHERE u.user.group = ?1 AND u.createdDate >= ?2 "
			+ "AND u.createdDate <= ?3 AND u.bookItem.bookType = ?4")
	Integer getHourseUserBookGroup(Group group, Date timeFrom, Date timeTo, BookType bookType);

	@Query("SELECT SUM(u.hours) FROM UserBook u WHERE u.createdDate >= ?1 AND u.createdDate <= ?2 AND u.bookItem.bookType = ?3")
	Integer getHourseUserBook(Date timeFrom, Date timeTo, BookType bookType);

	@Query("SELECT u FROM UserBook u WHERE u.bookItem.bookType = ?1")
	List<UserBook> getByBookType(BookType bookType);
	
	@Query("SELECT u FROM UserBook u WHERE u.bookingDate = ?1 AND u.bookItem = ?2 AND u.status <> 'REJECT'")
	List<UserBook> getByDateAndItem(Date date, BookItem bookItem);
}
