package com.cmcglobal.msbs.controller;

import static com.cmcglobal.msbs.utils.Constants.STATUS_ACTIVE;
import static com.cmcglobal.msbs.utils.Error.OBJECT_NULL;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cmcglobal.msbs.config.JwtTokenUtils;
import com.cmcglobal.msbs.entity.BookType;
import com.cmcglobal.msbs.model.ApiResponse;
import com.cmcglobal.msbs.model.BookTypeDTO;
import com.cmcglobal.msbs.model.ErrorObject;
import com.cmcglobal.msbs.model.UserDTO;
import com.cmcglobal.msbs.service.BookItemService;
import com.cmcglobal.msbs.service.BookTypeService;
import com.cmcglobal.msbs.service.UserService;
@CrossOrigin(origins = "*")
@RestController
public class BookTypeController {
	private final int ID_null = 1;
	@Autowired
	private BookTypeService bookTypeService;
	@Autowired
	private BookItemService bookItemService;
	@Autowired
	private UserService userService;
	@Autowired
	private JwtTokenUtils jwtTokenUtils;

	@GetMapping(value = "/list-book-type")
	private ResponseEntity<ApiResponse> getAll() {
		ApiResponse response = new ApiResponse();
		List<BookTypeDTO> listBookTypeDTO = bookTypeService.getAll();
		if (listBookTypeDTO != null) {
			response.setData(listBookTypeDTO);
			response.setMessage("GET ALL");
		} else {
			ErrorObject error = new ErrorObject(1, "List null");
			response.setError(error);
			response.setMessage("error");
		}
		return new ResponseEntity<ApiResponse>(response,
				listBookTypeDTO != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@GetMapping(value = "/list-book-type-active")
	private ResponseEntity<ApiResponse> getAllByStatus() {
		ApiResponse response = new ApiResponse();
		List<BookTypeDTO> listBookTypeDTO = bookTypeService.getByStatus(STATUS_ACTIVE);
		if (listBookTypeDTO != null) {
			response.setData(listBookTypeDTO);
			response.setMessage("GET ALL BOOKTYPE ACTIVE");
		} else {
			response.setError(OBJECT_NULL);
			response.setMessage("error");
		}
		return new ResponseEntity<ApiResponse>(response,
				listBookTypeDTO != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@GetMapping(value = "/book-type")
	private ResponseEntity<ApiResponse> getById(@RequestParam("id") Integer id) {
		ApiResponse response = new ApiResponse();
		if (bookTypeService.getById(id) != null) {
			response.setData(bookTypeService.getById(id));
			response.setMessage("GET BY ID");
		} else {
			response.setError(OBJECT_NULL);
			response.setMessage("error");
		}

		return new ResponseEntity<ApiResponse>(response,
				bookTypeService.getById(id) != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping(value = "/admin/book-type")
	private ResponseEntity<ApiResponse> create(@RequestBody BookTypeDTO bookTypeDTO, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		String headers = request.getHeader("Authorization");
		String token = null;
		if (headers != null && headers.startsWith("Token ")) {
			token = headers.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		UserDTO user = userService.getById(user_id);
		bookTypeDTO.setCreateBy(user);
		bookTypeService.save(bookTypeDTO);

		response.setData(bookTypeService.getById(bookTypeDTO.getId()));
		response.setMessage("CREATE BOOKTYPE");
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	@PutMapping(value = "/admin/book-type")
	private ResponseEntity<ApiResponse> update(@RequestBody BookTypeDTO bookTypeDTO, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		String headers = request.getHeader("Authorization");
		String token = null;
		if (headers != null && headers.startsWith("Token ")) {
			token = headers.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		UserDTO user = userService.getById(user_id);
		bookTypeDTO.setUpdateBy(user);
		BookType bookTypeEntity = bookTypeService.update(bookTypeDTO);
		response.setData(bookItemService.findById(bookTypeEntity.getId()));
		response.setMessage("UPDATE BOOK-TYPE");
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	@DeleteMapping(value = "/admin/book-type")
	private ResponseEntity<ApiResponse> delete(@RequestParam(name = "id") Integer id, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();

		String header = request.getHeader("Authorization");
		String token = null;
		if (header != null && header.startsWith("Token ")) {
			token = header.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		BookTypeDTO bookTypeDTO = bookTypeService.getById(id);
		bookTypeDTO.setUpdateBy(userService.getById(user_id));
		bookTypeService.delete(bookTypeDTO);
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("delete", "OK");
		response.setData(data);
		response.setMessage("DELETE BOOK-TYPE");
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

}