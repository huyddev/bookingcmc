package com.cmcglobal.msbs.controller;

import static com.cmcglobal.msbs.utils.Constants.STATUS_ACTIVE;
import static com.cmcglobal.msbs.utils.Error.OBJECT_DTT;
import static com.cmcglobal.msbs.utils.Error.OBJECT_NULL;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cmcglobal.msbs.config.JwtTokenUtils;
import com.cmcglobal.msbs.entity.User;
import com.cmcglobal.msbs.model.ApiResponse;
import com.cmcglobal.msbs.model.ErrorObject;
import com.cmcglobal.msbs.model.ForgotPassword;
import com.cmcglobal.msbs.model.UserDTO;
import com.cmcglobal.msbs.service.UserService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class AuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private BCryptPasswordEncoder bcryptPassword;

	@Autowired
	private JwtTokenUtils jwtTokenUtil;

	@Resource(name = "userService")
	private UserDetailsService userDetailsService;

	@Autowired
	private UserService userService;

	@PostMapping(value = "/register")
	public ResponseEntity<ApiResponse> register(@RequestBody UserDTO userRegister, HttpServletRequest request)
			throws AuthenticationException {
		User user = userService.save(userRegister, request);
		ApiResponse response = new ApiResponse();
		if (user != null) {
			response.setData(user);
			response.setMessage("User Register");
		} else {
			response.setError(OBJECT_DTT);
			response.setMessage("error");
		}

		return new ResponseEntity<ApiResponse>(response,
				user != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@GetMapping(value = "/confirm-register")
	private ResponseEntity<ApiResponse> confirmRegisterEmail(@RequestParam String token) {
		ApiResponse response = new ApiResponse();
		UserDTO userDTO = null;
		try {
			userDTO = userService.getUserByToken(token);
			userDTO.setStatus(STATUS_ACTIVE);
			userDTO.setToken(null);
//			userDTO.setExpiryToken(null);
			if (userService.update(userDTO) != null) {
				Map<String, Integer> data = new HashMap<>();
				data.put("verify", 1);
				response.setData(data);
				response.setMessage("CONFIRM REGISTER");
			} else {
				Map<String, Integer> data = new HashMap<>();
				data.put("verify", 0);
				response.setData(data);
				response.setMessage("error");
			}
		} catch (Exception e) {
			System.err.println(e);
			response.setError(OBJECT_NULL);
			response.setMessage("error");
		}

		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);

	}

	@PostMapping(value = "/authenticate")
	public ResponseEntity<ApiResponse> createAuthenticationToken(@RequestBody UserDTO userAuth) throws Exception {
		authenticate(userAuth.getMail(), userAuth.getPassword());
		final UserDetails userDetails = userDetailsService.loadUserByUsername(userAuth.getMail());
		final String token = jwtTokenUtil.generateToken(userService.findByMail(userAuth.getMail()));
		UserDTO user = userService.getByMail(userAuth.getMail());
		ApiResponse response = new ApiResponse();
		Map<String, Object> data = new HashMap<String, Object>();
		if (userDetails != null) {
			data.put("user", user);
			data.put("token", token);
		}
		response.setData(data);
		response.setMessage("Data user and Token");
		return new ResponseEntity<ApiResponse>(response,
				user != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping(value = "/forgot-password")
	private ResponseEntity<ApiResponse> forgotPassword(@RequestBody UserDTO userDTO, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		UserDTO user = null;
		try {
			user = userService.getUserPasswordByMail(userDTO.getMail());
			userService.forgotPassword(user, request);
			Map<String, Object> data = new HashMap<>();
			data.put("status", "success");
			response.setData(data);
			response.setMessage("Gửi yêu cầu thành công");
		} catch (Exception e) {
			System.err.println(e);
			response.setError(OBJECT_NULL);
		}
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	@PostMapping(value = "/confirm-forgot-password")
	private ResponseEntity<ApiResponse> forgotPasswrod(@RequestBody ForgotPassword fp) {
		ApiResponse response = new ApiResponse();
		UserDTO user = null;
		try {
			user = userService.getUserByToken(fp.getToken());
			user.setPassword(bcryptPassword.encode(fp.getNewPass()));
			user.setToken(null);
			User userUpdate = userService.update(user);
			if (userUpdate != null) {
				Map<String, Object> data = new HashMap<>();
				data.put("status", "success");
				response.setData(data);
			} else {
				Map<String, Object> data = new HashMap<>();
				data.put("status", "failed");
				response.setData(data);
			}
		} catch (Exception e) {
			System.err.println(e);
			response.setError(OBJECT_NULL);
			response.setMessage("error");
		}

		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (Exception e) {
			System.err.println(e);
			throw new Exception("ERROR AHTHENTICATE +_+: ", e);
		}
	}
}
