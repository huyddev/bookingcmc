package com.cmcglobal.msbs.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cmcglobal.msbs.config.JwtTokenUtils;
import com.cmcglobal.msbs.entity.Post;
import com.cmcglobal.msbs.model.ApiResponse;
import com.cmcglobal.msbs.model.ErrorObject;
import com.cmcglobal.msbs.model.PostDTO;
import com.cmcglobal.msbs.model.UserDTO;
import com.cmcglobal.msbs.service.PostService;
import com.cmcglobal.msbs.service.UserService;
import com.cmcglobal.msbs.utils.FileStore;

@CrossOrigin(origins = "*")
@RestController
public class PostController {

	private static final Logger logger = LoggerFactory.getLogger(PostController.class);

	@Autowired
	private PostService postService;

	@Autowired
	private UserService userService;

	@Autowired
	private JwtTokenUtils jwtTokenUtils;

	@GetMapping(value = "admin/posts")
	private ResponseEntity<ApiResponse> getAll() {
		ApiResponse response = new ApiResponse();
		if (postService.getAll() != null) {
			response.setData(postService.getAll());
			response.setMessage("GET ALL");
		} else {
			ErrorObject error = new ErrorObject(1, "LIST POST NOT FOUND");
			response.setError(error);
			response.setMessage("error");
		}
		return new ResponseEntity<ApiResponse>(response,
				postService.getAll() != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@GetMapping(value = "/post/list")
	private ResponseEntity<ApiResponse> getAllActive() {
		ApiResponse response = new ApiResponse();
		if (postService.getAllActive() != null) {
			response.setData(postService.getAllActive());
			response.setMessage("GET ALL");
		} else {
			ErrorObject error = new ErrorObject(1, "LIST POST NOT FOUND");
			response.setError(error);
			response.setMessage("error");
		}
		return new ResponseEntity<ApiResponse>(response,
				postService.getAll() != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@GetMapping(value = "/post")
	private ResponseEntity<ApiResponse> getById(@RequestParam("id") Integer id) {
		ApiResponse response = new ApiResponse();
		if (postService.getById(id) != null) {
			response.setData(postService.getById(id));
			response.setMessage("GET BY ID");
		} else {
			ErrorObject error = new ErrorObject(1, "POST NOT FOUND");
			response.setError(error);
			response.setMessage("error");
		}
		return new ResponseEntity<ApiResponse>(response,
				postService.getById(id) != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping(value = "/admin/post")
	private ResponseEntity<ApiResponse> create(@ModelAttribute PostDTO postDTO, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		String headers = request.getHeader("Authorization");
		String token = null;
		if (headers != null && headers.startsWith("Token ")) {
			token = headers.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		UserDTO user = userService.getById(user_id);
		postDTO.setAuthor(user);
		try {
			// save image
			final String UPLOAD_FOLDER = FileStore.UPLOAD_FOLDER;
			if (postDTO.getImageFile() != null && !postDTO.getImageFile().isEmpty()) {
				String image = System.currentTimeMillis() + "-post.jpg";
				Path pathAvatar = Paths.get(UPLOAD_FOLDER + File.separator + image);
				Files.write(pathAvatar, postDTO.getImageFile().getBytes());
				postDTO.setImage(image);
			}
		} catch (IOException e) {
			logger.error("Upload file error");
		}
		Post post = postService.save(postDTO);
		response.setData(post);
		response.setMessage("CREATE POST");
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	@PutMapping(value = "/admin/post")
	private ResponseEntity<ApiResponse> update(@ModelAttribute PostDTO postDTO, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		String headers = request.getHeader("Authorization");
		String token = null;
		if (headers != null && headers.startsWith("Token ")) {
			token = headers.substring(6);
		}
		try {
			// save image
			final String UPLOAD_FOLDER = FileStore.UPLOAD_FOLDER;
			if (postDTO.getImageFile() != null && !postDTO.getImageFile().isEmpty()) {
				String image = System.currentTimeMillis() + "-post.jpg";
				Path pathAvatar = Paths.get(UPLOAD_FOLDER + File.separator + image);
				Files.write(pathAvatar, postDTO.getImageFile().getBytes());
				postDTO.setImage(image);
			}
		} catch (IOException e) {
			logger.error("Upload file error");
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		UserDTO user = userService.getById(user_id);
		postDTO.setUpdateBy(user);
		Post post = postService.update(postDTO);
		response.setData(post);
		response.setMessage("UPDATE POST");
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	@DeleteMapping(value = "/admin/post")
	private ResponseEntity<ApiResponse> delete(@RequestParam(name = "id") Integer id, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		String header = request.getHeader("Authorization");
		String token = null;
		if (header != null && header.startsWith("Token ")) {
			token = header.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		PostDTO postDTO = postService.getById(id);
		postDTO.setUpdateBy(userService.getById(user_id));
		postService.delete(postDTO);
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("DELETE", "OK");
		response.setData(data);
		response.setMessage("DELETE POST");
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}
}
