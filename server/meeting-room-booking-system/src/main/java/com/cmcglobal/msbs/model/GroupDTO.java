package com.cmcglobal.msbs.model;

import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class GroupDTO extends AuditingDTO {
	private int id;
	private String name;
	private GroupDTO parentGroup;
	private List<GroupDTO> subGroup;
	private List<UserDTO> listUser;
	private String status;
	private UserDTO createBy;
	private UserDTO updateBy;
	private UserDTO manager;

	public GroupDTO() {

	}

	public GroupDTO(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public GroupDTO getParentGroup() {
		return parentGroup;
	}

	public void setParentGroup(GroupDTO parentGroup) {
		this.parentGroup = parentGroup;
	}

	public List<GroupDTO> getSubGroup() {
		return subGroup;
	}

	public void setSubGroup(List<GroupDTO> subGroup) {
		this.subGroup = subGroup;
	}

	public List<UserDTO> getListUser() {
		return listUser;
	}

	public void setListUser(List<UserDTO> listUser) {
		this.listUser = listUser;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public UserDTO getCreateBy() {
		return createBy;
	}

	public void setCreateBy(UserDTO createBy) {
		this.createBy = createBy;
	}

	public UserDTO getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(UserDTO updateBy) {
		this.updateBy = updateBy;
	}
	
	public UserDTO getManagerId() {
		return manager;
	}

	public void setManagerId(UserDTO manager) {
		this.manager = manager;
	}

}
