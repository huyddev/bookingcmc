package com.cmcglobal.msbs.utils;

public class FileStorageException extends RuntimeException {

	private static final long serialVersionUID = -2885282048136217001L;

	public FileStorageException(String message) {
		super(message);
	}

	public FileStorageException(String message, Throwable cause) {
		super(message, cause);
	}
}
