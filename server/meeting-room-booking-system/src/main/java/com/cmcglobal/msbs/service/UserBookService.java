package com.cmcglobal.msbs.service;

import java.awt.print.Book;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;

import java.util.List;

import com.cmcglobal.msbs.entity.UserBook;
import com.cmcglobal.msbs.entity.BookItem;
import com.cmcglobal.msbs.entity.BookType;
import com.cmcglobal.msbs.entity.Group;
import com.cmcglobal.msbs.entity.User;
import com.cmcglobal.msbs.model.BookTypeDTO;
import com.cmcglobal.msbs.model.GroupDTO;
import com.cmcglobal.msbs.model.UserBookDTO;

public interface UserBookService {

	UserBook save(UserBookDTO userBookDTO);

	UserBook update(UserBookDTO userBookDTO);

	UserBook updateStatus(UserBookDTO userBook, String status);

	void delete(UserBookDTO userBookDTO);

	UserBook findById(int id);

	UserBookDTO getById(int id);

	List<UserBookDTO> getAll();

	List<UserBookDTO> getByDate(Date date);

	List<UserBookDTO> getByDate(Date date, Pageable page);

	List<UserBookDTO> getByDate(Date date, int bookItemId);
	
	List<UserBookDTO> getByMonth(Date date);

	List<UserBookDTO> getByMonth(Date date, Pageable page);

	List<UserBookDTO> getByWeek(Date date);

	List<UserBookDTO> getByWeek(Date date, Pageable page);

	int getCountByBookingDate(Date date);

	int getCountByWeek(Date date);
	
	int getCountByMonth(Date date);

	int getCountByStatus(Date date, String status);

	int getCountByWeekStatus(Date date, String status);
	
	int getCountByMonthStatus(Date date, String status);

	List<UserBookDTO> getByTimeFromAndTimeTo(Date timeFrom, Date timeTo, BookItem room);

	List<UserBookDTO> getByUser(User user, String bookingStatus);
	
	List<UserBookDTO> getByUser(User user);

	int getTotalPages(int elementByPage);

	List<UserBookDTO> getByTimeAndGroup(GroupDTO group, Date timeFrom, Date timeTo, BookType bookType);

	List<UserBookDTO> getByTime(Date timeFrom, Date timeTo, BookType bookType);

	int getCountUserBookByTimeGroupStatus(GroupDTO group, Date timeFrom, Date timeTo, String status, BookType bookType);

	int getCountUserBookByTimeStatus(Date timeFrom, Date timeTo, String status, BookType bookType);

	int getHourseUserBookGroupStatus(GroupDTO group, Date timeFrom, Date timeTo, String status, BookType bookType);

	int getHourseUserBookStatus(Date timeFrom, Date timeTo, String status, BookType bookType);

	int getHourseUserBookGroup(GroupDTO group, Date timeFrom, Date timeTo, BookType bookType);

	int getHourseUserBook(Date timeFrom, Date timeTo, BookType bookType);
	
	List<UserBookDTO> getByBookType(BookType bookType);

}
