package com.cmcglobal.msbs.model;

import java.util.Date;
import java.util.List;

public class Dashboard {
	private GroupDTO group;
	private Date timeFrom;
	private Date timeTo;
	private BookTypeDTO bookType;
	private List<?> list;
	private int total;
	private int totalActive;
	private int totalInactive;
	private int totalHourse;
	private int totalHourseActive;
	private int totalHourseInactive;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getTotalActive() {
		return totalActive;
	}

	public void setTotalActive(int totalActive) {
		this.totalActive = totalActive;
	}

	public int getTotalInactive() {
		return totalInactive;
	}

	public void setTotalInactive(int totalInactive) {
		this.totalInactive = totalInactive;
	}

	public int getTotalHourse() {
		return totalHourse;
	}

	public void setTotalHourse(int totalHourse) {
		this.totalHourse = totalHourse;
	}

	public GroupDTO getGroup() {
		return group;
	}

	public void setGroup(GroupDTO group) {
		this.group = group;
	}

//	public Date getTimeFrom() {
//		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//		Date date = null;
//		try {
//			date = format.parse(this.timeFrom);
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		return date;
//	}
//
//	public void setTimeFrom(String timeFrom) {
//		this.timeFrom = timeFrom;
//	}
//
//	public Date getTimeTo() {
//		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//		Date date = null;
//		try {
//			date = format.parse(this.timeTo);
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		return date;
//	}
//
//	public void setTimeTo(String timeTo) {
//		this.timeTo = timeTo;
//	}

	public List<?> getList() {
		return list;
	}

	public Date getTimeFrom() {
		return timeFrom;
	}

	public void setTimeFrom(Date timeFrom) {
		this.timeFrom = timeFrom;
	}

	public Date getTimeTo() {
		return timeTo;
	}

	public void setTimeTo(Date timeTo) {
		this.timeTo = timeTo;
	}

	public void setList(List<?> list) {
		this.list = list;
	}

	public BookTypeDTO getBookType() {
		return bookType;
	}

	public void setBookType(BookTypeDTO bookTypeDTO) {
		this.bookType = bookTypeDTO;
	}

	public int getTotalHourseActive() {
		return totalHourseActive;
	}

	public void setTotalHourseActive(int totalHourseActive) {
		this.totalHourseActive = totalHourseActive;
	}

	public int getTotalHourseInactive() {
		return totalHourseInactive;
	}

	public void setTotalHourseInactive(int totalHourseInactive) {
		this.totalHourseInactive = totalHourseInactive;
	}

}
