package com.cmcglobal.msbs.service;

import com.cmcglobal.msbs.model.Mail;

public interface MailService {
	void sendForgetPasswordEmail(Mail mail);

	void sendUserRegistrationEmail(Mail mail);
	
	void sendBookingMessage(Mail mail);
}
