package com.cmcglobal.msbs.service.impl;

import static com.cmcglobal.msbs.utils.Constants.STATUS_ACTIVE;
import static com.cmcglobal.msbs.utils.Constants.STATUS_INACTIVE;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmcglobal.msbs.dao.BookItemDao;
import com.cmcglobal.msbs.entity.BookItem;
import com.cmcglobal.msbs.entity.BookType;
import com.cmcglobal.msbs.entity.User;
import com.cmcglobal.msbs.model.BookItemDTO;
import com.cmcglobal.msbs.model.BookTypeDTO;
import com.cmcglobal.msbs.service.BookItemService;
import com.cmcglobal.msbs.service.BookTypeService;
import com.cmcglobal.msbs.service.UserService;

@Service
public class BookItemServiceImpl implements BookItemService {

	@Autowired
	private BookItemDao bookItemDao;

	@Autowired
	private UserService userService;

	@Autowired
	private BookTypeService bookTypeService;

	@Override
	public BookItem save(BookItemDTO bookItemDTO) {
		bookItemDTO.setStatus(STATUS_ACTIVE);
		BookItem bookItem = bookItemDao.save(convertEntity(bookItemDTO));
		if (bookItem != null) {
			bookItemDTO.setId(bookItem.getId());
			return convertEntity(bookItemDTO);
		}
		return null;
	}

	@Override
	public BookItem update(BookItemDTO bookItemDTO) {
		BookItem bookItem = findById(bookItemDTO.getId());
		if (bookItem != null) {
			bookItemDTO.setCreateTime(findById(bookItemDTO.getId()).getCreatedDate());
			bookItem = bookItemDao.save(convertEntity(bookItemDTO));
			if (bookItem != null) {
				bookItemDTO.setId(bookItem.getId());
				return convertEntity(bookItemDTO);
			}
		}
		return null;
	}

	@Override
	public void delete(BookItemDTO bookItemDTO) {
		if(findById(bookItemDTO.getId())!=null) {
			bookItemDTO.setStatus(STATUS_INACTIVE);
			bookItemDao.save(convertEntity(bookItemDTO));
		}
	}

	@Override
	public BookItem findById(int id) {
		return bookItemDao.findById(id).get();
	}

	@Override
	public BookItemDTO getById(int id) {
		return convertDTO(findById(id));
	}

	@Override
	public List<BookItemDTO> getAll() {
		if (bookItemDao.findAll() != null) {
			List<BookItemDTO> bookItems = new ArrayList<>();
			for (BookItem bookItem : bookItemDao.findAll()) {
				bookItems.add(convertDTO(bookItem));
			}
			return bookItems;
		}
		return null;
	}

	private BookItem convertEntity(BookItemDTO bookItemDTO) {
		BookItem bookItem = new BookItem();
		bookItem.setId(bookItemDTO.getId());
		bookItem.setName(bookItemDTO.getName());
		bookItem.setDescription(bookItemDTO.getDescription());
		if (bookItemDTO.getCreateBy() != null) {
			User createBy = userService.findById(bookItemDTO.getCreateBy().getId());
			bookItem.setCreateBy(createBy);
		}
		if (bookItemDTO.getUpdateBy() != null) {
			User updateBy = userService.findById(bookItemDTO.getUpdateBy().getId());
			bookItem.setUpdateBy(updateBy);
		}
		bookItem.setCreatedDate(bookItemDTO.getCreateTime());
		bookItem.setLastModifiedDate(bookItemDTO.getUpdateTime());
		bookItem.setStatus(bookItemDTO.getStatus());
		if (bookItemDTO.getBookType() != null) {
			BookType bookType = bookTypeService.findById(bookItemDTO.getBookType().getId());
			bookItem.setBookType(bookType);
		}
		return bookItem;
	}

	private BookItemDTO convertDTO(BookItem bookItem) {
		BookItemDTO bookItemDTO = new BookItemDTO();
		bookItemDTO.setId(bookItem.getId());
		bookItemDTO.setName(bookItem.getName());
		bookItemDTO.setDescription(bookItem.getDescription());
		if (bookItemDTO.getCreateBy() != null) {
			User createBy = userService.findById(bookItem.getCreateBy().getId());
			bookItem.setCreateBy(new User(createBy.getId(), createBy.getFullname()));
		}
		if (bookItemDTO.getUpdateBy() != null) {
			User updateBy = userService.findById(bookItem.getUpdateBy().getId());
			bookItem.setUpdateBy(new User(updateBy.getId(), updateBy.getFullname()));
		}
		bookItemDTO.setCreateTime(bookItem.getCreatedDate());
		bookItemDTO.setUpdateTime(bookItem.getLastModifiedDate());
		bookItemDTO.setStatus(bookItem.getStatus());
		if (bookItem.getBookType() != null) {
			BookType BookingType = bookTypeService.findById(bookItem.getBookType().getId());
			bookItemDTO.setBookType(new BookTypeDTO(BookingType.getId(), BookingType.getName(), BookingType.getDescription()));
		}
		return bookItemDTO;
	}

	@Override
	public int getNumberBookItem() {
		return bookItemDao.getNumberBookItem();
	}

	@Override
	public int getNumberBookItemByStatus(String status) {
		return bookItemDao.getNumberBookItemByStatus(status);
	}

	@Override
	public BookItem update(BookItem bookItem) {
		BookItem r = bookItemDao.save(bookItem);
		if (r != null) {
			return bookItem;
		}
		return null;
	}

	@Override
	public List<BookItemDTO> getByBookType(BookTypeDTO bookTypeDTO) {
		List<BookItemDTO> list = new ArrayList<>();
		BookType bookType = new BookType();
		bookType.setId(bookTypeDTO.getId());
		List<BookItem> listEntity = bookItemDao.getByBookType(bookType);
		for (BookItem bookItem : listEntity) {
			list.add(convertDTO(bookItem));
		}
		return list;
	}
}
