package com.cmcglobal.msbs.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cmcglobal.msbs.entity.BookItem;
import com.cmcglobal.msbs.entity.BookType;
@Repository
public interface BookItemDao extends JpaRepository<BookItem, Integer>{

	@Query("SELECT count(u) FROM BookItem u")
	int getNumberBookItem();
	
	@Query("SELECT count(u) FROM BookItem u WHERE u.status = ?1")
	int getNumberBookItemByStatus(String status);
	
	@Query("SELECT u FROM BookItem u WHERE u.bookType = ?1")
	List<BookItem> getByBookType(BookType bookType);
}
