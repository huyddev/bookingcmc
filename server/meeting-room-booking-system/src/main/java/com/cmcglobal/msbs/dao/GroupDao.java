package com.cmcglobal.msbs.dao;

import java.util.List;
import static com.cmcglobal.msbs.utils.Constants.ID_GROUP_NULL;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cmcglobal.msbs.entity.Group;
@Repository
public interface GroupDao extends JpaRepository<Group, Integer>{
	
	
	@Query("SELECT g FROM Group g WHERE g.name LIKE ?1")
	List<Group> getByName(String name);
	
	@Query("SELECT g FROM Group g WHERE g.id <> "+ID_GROUP_NULL)
	List<Group> findAll();
}
