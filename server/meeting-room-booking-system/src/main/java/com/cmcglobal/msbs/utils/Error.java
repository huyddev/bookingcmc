package com.cmcglobal.msbs.utils;

import com.cmcglobal.msbs.model.ErrorObject;

public class Error {
	// error
	public static final ErrorObject OBJECT_NULL = new ErrorObject(1, "OBJECT NOT FOUND");
	public static final ErrorObject OBJECT_DTT = new ErrorObject(2, "OBJECT ĐÃ TỒN TẠI");
	public static final ErrorObject ERROR_UPDATE = new ErrorObject(3, "UPDATE FAILED");
}
