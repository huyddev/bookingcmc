package com.cmcglobal.msbs.utils;

public class Constants {
	public static final long ACCESS_TOKEN_VALIDITY_SECONDS =  5 * 60 * 60;
	public static final String SIGNING_KEY = "chinhdz";
	public static final String TOKEN_PREFIX = "Token ";
	public static final String HEADER_STRING = "Authorization";
	//status
	public static final String STATUS_PENDING = "PENDING";
	public static final String STATUS_REJECT = "REJECT";
	public static final String STATUS_APPROVE = "APPROVE";
	public static final String STATUS_ACTIVE = "ACTIVE";
	public static final String STATUS_INACTIVE = "INACTIVE";
	//img
	public static final String IMG_DEFAULT = "1570763793139-user.jpg";
	
	public static final int ID_BOOKTYPE_NULL = 1;
	public static final int ID_GROUP_NULL = 5;
}
