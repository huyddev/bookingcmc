package com.cmcglobal.msbs.service.impl;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmcglobal.msbs.dao.RoleDao;
import com.cmcglobal.msbs.entity.Role;
import com.cmcglobal.msbs.entity.User;
import com.cmcglobal.msbs.model.RoleDTO;
import com.cmcglobal.msbs.service.RoleService;
import com.cmcglobal.msbs.service.UserService;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	RoleDao roleDao;

	@Autowired
	UserService userService;

	@Override
	public RoleDTO getById(int id) {
		Role roleEntity = roleDao.getOne(id);
		RoleDTO roleDTO = new RoleDTO();
		roleDTO.setId(id);
		roleDTO.setName(roleEntity.getName());
		roleDTO.setStatus(roleEntity.getStatus());
		return roleDTO;
	}

	@Override
	public Role findById(int id) {
		return roleDao.findById(id).get();
	}

	@Override
	public Role save(RoleDTO roleDTO) {
		return roleDao.save(convertEntity(roleDTO));
	}

	@Override
	public Role update(RoleDTO roleDTO) {
		roleDTO.setCreateTime(findById(roleDTO.getId()).getCreatedDate());
		if (roleDao.save(convertEntity(roleDTO)) != null) {
			return convertEntity(roleDTO);
		}
		return null;
	}

	@Override
	public void delete(RoleDTO roleDTO) {
		roleDao.delete(findById(roleDTO.getId()));

	}

	private Role convertEntity(RoleDTO roleDTO) {
		Role role = new Role();
		role.setId(roleDTO.getId());
		role.setName(roleDTO.getName());
		if (roleDTO.getCreateBy() != null) {
			User createBy = userService.findById(roleDTO.getCreateBy().getId());
			role.setCreateBy(new User(createBy.getId(), createBy.getFullname()));
		}
		if (roleDTO.getUpdateBy() != null) {
			User updateBy = userService.findById(roleDTO.getUpdateBy().getId());
			role.setUpdateBy(new User(updateBy.getId(), updateBy.getFullname()));
		}
		role.setCreatedDate(roleDTO.getCreateTime());
		role.setLastModifiedDate(roleDTO.getUpdateTime());
		role.setStatus(roleDTO.getStatus());
		return role;
	}

	@Override
	public List<RoleDTO> getAll() {
		if (roleDao.findAll() != null) {
			List<RoleDTO> roleDTOs = new ArrayList<>();
			for (Role role : roleDao.findAll()) {
				roleDTOs.add(convertDTO(role));
			}
			return roleDTOs;
		}
		return null;
	}

	private RoleDTO convertDTO(Role role) {
		RoleDTO roleDTO = new RoleDTO();
		roleDTO.setId(role.getId());
		roleDTO.setName(role.getName());
		if (roleDTO.getCreateBy() != null) {
			User createBy = userService.findById(role.getCreateBy().getId());
			role.setCreateBy(new User(createBy.getId(), createBy.getFullname()));
		}
		if (roleDTO.getUpdateBy() != null) {
			User updateBy = userService.findById(role.getUpdateBy().getId());
			role.setUpdateBy(new User(updateBy.getId(), updateBy.getFullname()));
		}
		roleDTO.setCreateTime(role.getCreatedDate());
		roleDTO.setUpdateTime(role.getLastModifiedDate());
		roleDTO.setStatus(role.getStatus());
		return roleDTO;
	}

}
