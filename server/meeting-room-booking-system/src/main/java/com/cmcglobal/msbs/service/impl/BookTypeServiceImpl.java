package com.cmcglobal.msbs.service.impl;

import static com.cmcglobal.msbs.utils.Constants.STATUS_ACTIVE;
import static com.cmcglobal.msbs.utils.Constants.STATUS_INACTIVE;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmcglobal.msbs.dao.BookTypeDao;
import com.cmcglobal.msbs.entity.BookItem;
import com.cmcglobal.msbs.entity.BookType;
import com.cmcglobal.msbs.entity.User;
import com.cmcglobal.msbs.model.BookItemDTO;
import com.cmcglobal.msbs.model.BookTypeDTO;
import com.cmcglobal.msbs.model.UserDTO;
import com.cmcglobal.msbs.service.BookItemService;
import com.cmcglobal.msbs.service.BookTypeService;
import com.cmcglobal.msbs.service.UserService;;

@Service
public class BookTypeServiceImpl implements BookTypeService {

	@Autowired
	private BookTypeDao bookTypeDao;

	@Autowired
	private BookItemService bookItemService;

	@Autowired
	private UserService userService;

	@Override
	public BookType save(BookTypeDTO bookTypeDTO) {
		bookTypeDTO.setStatus(STATUS_ACTIVE);
		if (bookTypeDao.save(convertEntity(bookTypeDTO)) != null) {
			return convertEntity(bookTypeDTO);
		}
		return null;
	}

	@Override
	public BookType update(BookTypeDTO bookTypeDTO) {
		bookTypeDTO.setCreateTime(getById(bookTypeDTO.getId()).getCreateTime());

		BookType bookType = findById(bookTypeDTO.getId());
		if (bookType != null) {
			bookType = bookTypeDao.save(convertEntity(bookTypeDTO));
			bookTypeDTO.setId(bookType.getId());
			return convertEntity(bookTypeDTO);
		}
		return null;
	}

	@Override
	public void delete(BookTypeDTO bookTypeDTO) {
		bookTypeDTO.setCreateTime(findById(bookTypeDTO.getId()).getCreatedDate());
		bookTypeDTO.setStatus(STATUS_INACTIVE);
		bookTypeDao.save(convertEntity(bookTypeDTO));
	}

	@Override
	public BookType findById(int id) {
		return bookTypeDao.findById(id).get();
	}

	@Override
	public BookTypeDTO getById(int id) {
		return convertDTO(findById(id));
	}

	@Override
	public List<BookTypeDTO> getAll() {
		List<BookType> bookTypeEntity = null;
		if (bookTypeDao.findAll() != null) {
			List<BookTypeDTO> bookTypes = new ArrayList<>();
			bookTypeEntity = bookTypeDao.findAll();
			for (BookType bookType : bookTypeEntity) {
				bookTypes.add(convertDTO(bookType));
			}
			return bookTypes;
		}
		return null;
	}

	private BookType convertEntity(BookTypeDTO bookTypeDTO) {
		BookType bookType = new BookType();
		bookType.setId(bookTypeDTO.getId());
		bookType.setDescription(bookTypeDTO.getDescription());
		bookType.setName(bookTypeDTO.getName());
		if (bookTypeDTO.getCreateBy() != null) {
			User createBy = userService.findById(bookTypeDTO.getCreateBy().getId());
			bookType.setCreateBy(new User(createBy.getId(), createBy.getFullname()));
		}
		if (bookTypeDTO.getUpdateBy() != null) {
			User updateBy = userService.findById(bookTypeDTO.getUpdateBy().getId());
			bookType.setUpdateBy(new User(updateBy.getId(), updateBy.getFullname()));
		}
		if (bookTypeDTO.getBookItems() != null) {
			List<BookItem> bookItems = null;
			for (BookItemDTO bookItemDTO : bookTypeDTO.getBookItems()) {
				bookItems = new ArrayList<>();
				if (bookItemService.findById(bookItemDTO.getId()) != null) {
					BookItem bookItemEntity = bookItemService.findById(bookItemDTO.getId());
					bookItems.add(bookItemEntity);
				}
			}
			bookType.setBookItems(bookItems);
		}
		bookType.setCreatedDate(bookTypeDTO.getCreateTime());
		bookType.setLastModifiedDate(bookTypeDTO.getUpdateTime());
		bookType.setStatus(bookTypeDTO.getStatus());
		return bookType;
	}

	private BookTypeDTO convertDTO(BookType bookType) {
		BookTypeDTO bookTypeDTO = new BookTypeDTO();
		bookTypeDTO.setId(bookType.getId());
		bookTypeDTO.setName(bookType.getName());
		bookTypeDTO.setDescription(bookType.getDescription());
		bookTypeDTO.setCreateTime(bookType.getCreatedDate());
		bookTypeDTO.setUpdateTime(bookType.getLastModifiedDate());
		if (bookType.getBookItems() != null) {
			List<BookItemDTO> listBookItemDTO = new ArrayList<>();
			for (BookItem bookItem : bookType.getBookItems()) {
				if (bookItemService.findById(bookItem.getId()) != null) {
					BookItem bookItemEntity = bookItemService.findById(bookItem.getId());
					BookItemDTO bookItemDTO = new BookItemDTO();
					bookItemDTO.setId(bookItemEntity.getId());
					bookItemDTO.setName(bookItemEntity.getName());
					listBookItemDTO.add(bookItemDTO);
				}
			}
			bookTypeDTO.setBookItems(listBookItemDTO);
		}
		if (bookType.getCreateBy() != null) {
			UserDTO createBy = userService.getById(bookType.getCreateBy().getId());
			bookTypeDTO.setCreateBy(new UserDTO(createBy.getId(), createBy.getFullname()));
		}
		if (bookType.getUpdateBy() != null) {
			UserDTO updateBy = userService.getById(bookType.getUpdateBy().getId());
			bookTypeDTO.setUpdateBy(new UserDTO(updateBy.getId(), updateBy.getFullname()));
		}
		bookTypeDTO.setStatus(bookType.getStatus());
		return bookTypeDTO;
	}

	@Override
	public List<BookType> findAll() {
		return bookTypeDao.findAll();
	}

	@Override
	public List<BookTypeDTO> getByStatus(String status) {
		List<BookTypeDTO> listDTO = null;
		try {
			listDTO = new ArrayList<>();
			List<BookType> listEntity = bookTypeDao.findByStatus(status);
			for (BookType bookType : listEntity) {
				listDTO.add(convertDTO(bookType));
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("List booktype null" + e);
		}
		return listDTO;
	}

	@Override
	public List<BookType> findByStatus(String status) {
		return bookTypeDao.findByStatus(status);
	}

}
