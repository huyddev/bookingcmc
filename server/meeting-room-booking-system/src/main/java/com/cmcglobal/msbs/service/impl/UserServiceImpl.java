package com.cmcglobal.msbs.service.impl;

import static com.cmcglobal.msbs.utils.Constants.IMG_DEFAULT;
import static com.cmcglobal.msbs.utils.Constants.STATUS_INACTIVE;
import static com.cmcglobal.msbs.utils.Constants.STATUS_PENDING;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.cmcglobal.msbs.dao.UserDao;
import com.cmcglobal.msbs.entity.Group;
import com.cmcglobal.msbs.entity.Role;
import com.cmcglobal.msbs.entity.User;
import com.cmcglobal.msbs.model.GroupDTO;
import com.cmcglobal.msbs.model.Mail;
import com.cmcglobal.msbs.model.RoleDTO;
import com.cmcglobal.msbs.model.UserDTO;
import com.cmcglobal.msbs.service.GroupService;
import com.cmcglobal.msbs.service.MailService;
import com.cmcglobal.msbs.service.RoleService;
import com.cmcglobal.msbs.service.UserService;

@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {
	private final int ROLE_DEFAULT = 2;

	@Autowired
	private UserDao userDao;

	@Autowired
	private GroupService groupService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private BCryptPasswordEncoder bcryptPassword;

	@Autowired
	private MailService emailService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userDao.findByMail(username);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(user.getMail(), user.getPassword(),
				user.getAuthorities());
	}

	@Override
	public User save(UserDTO user, HttpServletRequest request) {
		if (checkMail(user.getMail())) {
			user.setPassword(bcryptPassword.encode(user.getPassword()));
			if (user.getListRole() == null) {
				List<RoleDTO> roles = new ArrayList<>();
				roles.add(roleService.getById(ROLE_DEFAULT));
				user.setListRole(roles);
			}
			user.setStatus(STATUS_PENDING);
			user.setAvata(IMG_DEFAULT);
			user.setToken(UUID.randomUUID().toString());
//			user.setExpiryToken(120);
			// send mail
			Mail mail = new Mail();
			mail.setFrom("CMC_Global");
			mail.setTo(user.getMail());
			mail.setSubject("User Registration");
			Map<String, Object> model = new HashMap<>();
			model.put("token", user);
			model.put("signature", "CMC Global");
			String url = "http://localhost:4200";
			model.put("resetUrl", url + "/confirm-register?token=" + user.getToken());
			mail.setModel(model);
			emailService.sendUserRegistrationEmail(mail);
			// save database
			userDao.save(convertEntity(user));
			return convertEntity(user);
		} else {
			return null;
		}

	}

	@Override
	public User updateUser(UserDTO userDTO) {
		User user = findById(userDTO.getId());
		if(user!=null) {
			userDTO.setPassword(user.getPassword());
			userDao.save(convertEntity(userDTO));
			return convertEntity(userDTO);
		}else {	
			return null;
		}
	}

	@Override
	public User findByMail(String username) {
		return userDao.findByMail(username);
	}

	@Override
	public User findById(int id) {
		return userDao.findById(id).get();
	}

	@Override
	public void delete(UserDTO userDTO) {
		userDTO.setStatus(STATUS_INACTIVE);
		userDao.save(convertEntity(userDTO));
	}

	@Override
	public UserDTO getById(int id) {
		return convertDTO(findById(id));
	}

	@Override
	public UserDTO getByMail(String mail) {
		return convertDTO(userDao.findByMail(mail));
	}

	@Override
	public List<UserDTO> getAll() {
		List<UserDTO> usersDTO = new ArrayList<UserDTO>();
		List<User> usersEntity = userDao.findAll();
		for (User user : usersEntity) {
			usersDTO.add(convertDTO(user));
		}
		return usersDTO;
	}

	@Override
	public List<UserDTO> searchUserByName(String name) {
		List<User> users = userDao.findLikeFullname(name);
		List<UserDTO> listUserDTO = new ArrayList<>();
		if (users != null) {
			for (User user : users) {
				listUserDTO.add(convertDTO(user));
			}
		}
		return listUserDTO;
	}

	@Override
	public List<UserDTO> checkUserMail(String name) {
		List<User> users = userDao.checkUserMail(name);
		List<UserDTO> userDTOs = new ArrayList<>();
		if (users != null) {
			for (User user : users) {
				userDTOs.add(convertDTO(user));
			}
		}
		return userDTOs;
	}

	private User convertEntity(UserDTO user) {
		User userEntity = new User();
		userEntity.setId(user.getId());
		userEntity.setFullname(user.getFullname());
		userEntity.setMail(user.getMail());
		userEntity.setPassword(user.getPassword());
		userEntity.setAvata(user.getAvata());
		userEntity.setPhone(user.getPhone());
		userEntity.setStatus(user.getStatus());
		if (user.getGroup() != null) {
			Group group = groupService.findById(user.getGroup().getId());
			userEntity.setGroup(new Group(group.getId(), group.getName()));
		}
		if (user.getCreateBy() != null) {
			User createBy = findById(user.getCreateBy().getId());
			userEntity.setCreateBy(new User(createBy.getId(), createBy.getFullname()));
		}
		if (user.getUpdateBy() != null) {
			User updateBy = findById(user.getUpdateBy().getId());
			userEntity.setUpdateBy(new User(updateBy.getId(), updateBy.getFullname()));
		}
		userEntity.setCreatedDate(user.getCreateTime());
		userEntity.setLastModifiedDate(user.getUpdateTime());
		userEntity.setToken(user.getToken());
//		userEntity.setExpiryToken(user.getExpiryToken());
		if (user.getListRole() != null) {
			List<Role> roles = new ArrayList<>();
			for (RoleDTO roleDTO : user.getListRole()) {
				Role role = roleService.findById(roleDTO.getId());
				Role roleEntity = new Role();
				roleEntity.setId(role.getId());
				roleEntity.setName(role.getName());
				roles.add(roleEntity);
			}
			userEntity.setListRole(roles);
		}
		return userEntity;
	}

	private UserDTO convertDTO(User userEntity) {
		UserDTO userDTO = new UserDTO();
		userDTO.setId(userEntity.getId());
		userDTO.setMail(userEntity.getMail());
		userDTO.setFullname(userEntity.getFullname());
		userDTO.setAvata(userEntity.getAvata());
		userDTO.setPhone(userEntity.getPhone());
		if (userEntity.getCreateBy() != null) {
			User createBy = findById(userEntity.getCreateBy().getId());
			userDTO.setCreateBy(new UserDTO(createBy.getId(), createBy.getFullname()));
		}
		if (userEntity.getUpdateBy() != null) {
			User updateBy = findById(userEntity.getUpdateBy().getId());
			userDTO.setUpdateBy(new UserDTO(updateBy.getId(), updateBy.getFullname()));
		}
		if (groupService.findById(userEntity.getGroup().getId()) != null) {
			Group group = groupService.findById(userEntity.getGroup().getId());
			userDTO.setGroup(new GroupDTO(group.getId(), group.getName()));
		}
		userDTO.setCreateTime(userEntity.getCreatedDate());
		userDTO.setUpdateTime(userEntity.getLastModifiedDate());
		userDTO.setToken(userEntity.getToken());
//		userDTO.setExpiryToken(userEntity.getExpiryToken());
		if (userEntity != null) {
			List<RoleDTO> roles = new ArrayList<>();
			for (Role role : userEntity.getListRole()) {
				Role roleEntity = roleService.findById(role.getId());
				RoleDTO roleDTO = new RoleDTO();
				roleDTO.setId(roleEntity.getId());
				roleDTO.setName(roleEntity.getName());
				roles.add(roleDTO);
			}
			userDTO.setListRole(roles);
		}
		userDTO.setStatus(userEntity.getStatus());
		return userDTO;
	}

	@Override
	public boolean checkMail(String mail) {
		if (userDao.findByMail(mail) != null) {
			return false;
		}
		return true;
	}

	@Override
	public User save(UserDTO user) {
		user.setAvata(IMG_DEFAULT);
		userDao.save(convertEntity(user));
		return convertEntity(user);

	}

	@Override
	public int getCountUser() {
		return userDao.getCountUser();
	}

	@Override
	public int getCountUserActive() {
		return userDao.getCountUserActive();
	}

	@Override
	public User updateImage(UserDTO userDTO) {
		User user = findById(userDTO.getId());
		if (user != null) {
			user.setFullname(userDTO.getFullname());
			user.setPhone(userDTO.getPhone());
			user.setGroup(groupService.findById(userDTO.getGroupId()));
			if(userDTO.getAvata()!=null) {
				user.setAvata(userDTO.getAvata());
			}
			User userEntity = userDao.save(user);
			
			return userEntity;
		}
		return null;
	}

	@Override
	public User changePass(UserDTO userDTO) {
		User user = findById(userDTO.getId());
		if (user != null) {
			if (userDTO.getPassword() != null) {
				user.setPassword(userDTO.getPassword());
			}
			User userEntity = userDao.save(user);
			User result = new User();
			result.setPassword(userEntity.getPassword());
			return result;
		}
		return null;
	}

	@Override
	public UserDTO getUserByToken(String token) {
		User user = null;
		if (userDao.findByToken(token) != null) {
			user = userDao.findByToken(token);
			UserDTO userDTO = convertDTO(user);
			userDTO.setPassword(user.getPassword());
			return userDTO;
		}
		return null;
	}

	@Override
	public void forgotPassword(UserDTO userDTO, HttpServletRequest request) {
		userDTO.setToken(UUID.randomUUID().toString());
		Mail email = new Mail();
		email.setFrom("chinh12091997@gmail.com");
		email.setTo(userDTO.getMail());
		email.setSubject("FORGOT PASSWORD");
		Map<String, Object> model = new HashMap<>();
		model.put("user", userDTO);
		model.put("signature", "CMC Global");
		String url = "http://localhost:4200";
		model.put("resetUrl", url + "/confirm-forgot-password?token=" + userDTO.getToken());
		email.setModel(model);
		emailService.sendForgetPasswordEmail(email);

		update(userDTO);
	}

	@Override
	public User update(UserDTO user) {
		if (findById(user.getId()) != null) {
			userDao.save(convertEntity(user));
			return convertEntity(user);
		}
		return null;
	}

	@Override
	public UserDTO getUserPasswordByMail(String mail) {
		User user = findByMail(mail);
		UserDTO userDTO = convertDTO(user);
		userDTO.setPassword(user.getPassword());
		return userDTO;
	}

	@Override
	public List<UserDTO> getUserByTimeAndGroup(GroupDTO group, Date timeFrom, Date timeTo) {
		List<UserDTO> list = new ArrayList<>();
		List<User> users = null;
		try {
			users = userDao.getByTime(timeFrom, timeTo);
			Group g = new Group();
			g.setId(group.getId());
			for (User user : users) {
				list.add(convertDTO(user));
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<UserDTO> getByTime(Date timeFrom, Date timeTo) {
		List<UserDTO> list = new ArrayList<>();
		List<User> users = null;
		try {
			users = userDao.getByTime(timeFrom, timeTo);
			for (User user : users) {
				list.add(convertDTO(user));
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int getCountUserByTimeGroupStatus(GroupDTO group, Date timeFrom, Date timeTo, String status) {
		return userDao.getCountUserByTimeGroupStatus(group, timeFrom, timeTo, status);
	}

	@Override
	public int getCountUserByTimeStatus(Date timeFrom, Date timeTo, String status) {
		return userDao.getCountUserByTimeStatus(timeFrom, timeTo, status);
	}

}
