package com.cmcglobal.msbs.controller;

import static com.cmcglobal.msbs.utils.Constants.STATUS_ACTIVE;
import static com.cmcglobal.msbs.utils.Error.OBJECT_DTT;
import static com.cmcglobal.msbs.utils.Error.OBJECT_NULL;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.cmcglobal.msbs.config.JwtTokenUtils;
import com.cmcglobal.msbs.entity.User;
import com.cmcglobal.msbs.model.ApiResponse;
import com.cmcglobal.msbs.model.ErrorObject;
import com.cmcglobal.msbs.model.UserDTO;
import com.cmcglobal.msbs.service.UserService;
import com.cmcglobal.msbs.service.impl.FileStorageService;
import com.cmcglobal.msbs.utils.FileStorageProperties;
import com.cmcglobal.msbs.utils.FileStore;

@CrossOrigin(origins = "*")
@RestController
public class UserController{
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private UserService userService;

	@Autowired
	private BCryptPasswordEncoder bcryptPassword;

	@Autowired
	private JwtTokenUtils jwtTokenUtils;

	@Autowired
	private FileStorageService fileStorageService;

	/* USER BY PAGE ADMIN */
	@RequestMapping(value = "/admin/user", method = RequestMethod.POST)
	public ResponseEntity<ApiResponse> createUser(@RequestBody UserDTO userDTO, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		userDTO.setPassword(bcryptPassword.encode(userDTO.getPassword()));
		String header = request.getHeader("Authorization");
		String token = null;
		if (header != null && header.startsWith("Token ")) {
			token = header.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		UserDTO admin = userService.getById(user_id);
		userDTO.setUpdateBy(admin);
		userDTO.setStatus(STATUS_ACTIVE);
		User userEntity = userService.save(userDTO);
		if (userEntity != null) {
			response.setData(userEntity);
			response.setMessage("CreateUser");
		} else {
			ErrorObject error = OBJECT_DTT;
			response.setError(error);
			response.setMessage("error");
		}
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/admin/user", method = RequestMethod.DELETE)
	public ResponseEntity<ApiResponse> deleteUser(@RequestParam(name = "id") Integer id, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();

		String header = request.getHeader("Authorization");
		String token = null;
		if (header != null && header.startsWith("Token ")) {
			token = header.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		UserDTO userDTO = userService.getById(user_id);
		UserDTO userUpdate = userService.getById(id);
		userUpdate.setUpdateBy(userDTO);
		userService.delete(userUpdate);
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("delete", "OK");
		response.setData(data);
		response.setMessage("DeleteUser");

		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	/* SHARED */

	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public ResponseEntity<ApiResponse> getUserById(@RequestParam(name = "id") int id) {
		UserDTO userDTO = null;
		ApiResponse response = new ApiResponse();
		if (userService.getById(id) != null) {
			userDTO = userService.getById(id);
			response.setData(userDTO);
			response.setMessage("GetUser");
		} else {
			ErrorObject error = OBJECT_NULL;
			response.setError(error);
			response.setMessage("error");
		}
		return new ResponseEntity<ApiResponse>(response,
				userDTO != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@GetMapping(value = "/users")
	public ResponseEntity<ApiResponse> getAllUser() {
		List<UserDTO> users = null;
		ApiResponse response = new ApiResponse();
		if (userService.getAll() != null) {
			users = userService.getAll();
			response.setData(users);
			response.setMessage("LIST USER");
		} else {
			ErrorObject error = OBJECT_NULL;
			response.setError(error);
			response.setMessage("error");
		}
		return new ResponseEntity<ApiResponse>(response,
				users != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping(name = "/users/search")
	public ResponseEntity<ApiResponse> searchUserByName(@RequestParam String search) {
		ApiResponse response = new ApiResponse();
		List<UserDTO> userDTO = null;
		if (userService.searchUserByName(search) != null) {
			userDTO = new ArrayList<>();
			userDTO = userService.searchUserByName(search);
			response.setData(userDTO);
			response.setMessage("SEARCH USER");
		} else {
			ErrorObject error = OBJECT_NULL;
			response.setError(error);
			response.setMessage("error");
		}
		return new ResponseEntity<ApiResponse>(response,
				userDTO != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);

	}

	@PutMapping(value = "/mail/check")
	public ResponseEntity<ApiResponse> checkMail(@RequestBody UserDTO userDTO) {
		ApiResponse response = new ApiResponse();
		List<UserDTO> userDTOs = null;
		String mail = userDTO.getMail();
		if (userService.checkUserMail(mail) != null) {
			userDTOs = new ArrayList<>();
			userDTOs = userService.checkUserMail(userDTO.getMail());
			if (userDTOs.size() > 0) {
				response.setData(mail);
				response.setMessage("THIS EMAIL IS ALREADY REGISTERED");
			}
		}
		System.out.println(userService.checkUserMail(mail));
		if (userService.checkUserMail(mail).isEmpty()) {
			response.setData(mail);
			response.setMessage("YOU CAN USE THIS MAIL");
		}
		return new ResponseEntity<ApiResponse>(response,
				userDTO != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@RequestMapping(value = "/user", method = RequestMethod.PUT)
	public ResponseEntity<ApiResponse> updateUser(@RequestBody UserDTO userDTO, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();

		String header = request.getHeader("Authorization");
		String token = null;
		if (header != null && header.startsWith("Token ")) {
			token = header.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		userDTO.setUpdateBy(userService.getById(user_id));
		User user = userService.updateUser(userDTO);

		response.setData(userService.getById(user.getId()));
		response.setMessage("UpdateUser");

		return new ResponseEntity<ApiResponse>(response,
				userDTO != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);

	}

	@RequestMapping(value = "/user/avatar", method = RequestMethod.PUT)
	public ResponseEntity<ApiResponse> updateUserImage(@ModelAttribute UserDTO userDTO, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		String headers = request.getHeader("Authorization");
		String token = null;
		if (headers != null && headers.startsWith("Token ")) {
			token = headers.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		userDTO.setId(user_id);
		try {
			// save image
			final String UPLOAD_FOLDER = FileStore.UPLOAD_FOLDER;
			if (userDTO.getImageFile() != null || !userDTO.getImageFile().isEmpty()) {
				String image = System.currentTimeMillis() + "-user.jpg";
				Path pathAvatar = Paths.get(UPLOAD_FOLDER + File.separator + image);
				Files.write(pathAvatar, userDTO.getImageFile().getBytes());
				userDTO.setAvata(image);
			}
		} catch (IOException e) {
			logger.error("Upload file error");
		}
		User userEntity = userService.updateImage(userDTO);
		response.setData(userService.getById(user_id));
		response.setMessage("UpdateUser");

		return new ResponseEntity<ApiResponse>(response,
				userEntity == null ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.OK);
	}

	@RequestMapping(value = "/user/change-password", method = RequestMethod.PUT)
	public ResponseEntity<ApiResponse> updateUserPassWord(@RequestBody UserDTO userDTO, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		String headers = request.getHeader("Authorization");
		String token = null;
		if (headers != null && headers.startsWith("Token ")) {
			token = headers.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		UserDTO user = userService.getById(user_id);
		user.setPassword(bcryptPassword.encode(userDTO.getPassword()));
		User userEntity = userService.changePass(user);

		response.setData(userEntity);
		response.setMessage("Password changed");

		return new ResponseEntity<ApiResponse>(response,
				userEntity == null ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.OK);
	}

	@RequestMapping(value = "/user/check-password", method = RequestMethod.PUT)
	public ResponseEntity<ApiResponse> checkUserPassWord(@RequestBody UserDTO userDTO, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		String headers = request.getHeader("Authorization");
		String token = null;
		if (headers != null && headers.startsWith("Token ")) {
			token = headers.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		UserDTO user = userService.getById(user_id);
		User userEntity = userService.findById(user_id);
		boolean check = bcryptPassword.matches(userDTO.getPassword(), userEntity.getPassword());
		if (check == true) {
			response.setMessage("PASSWORD VERIFIED");
			response.setData(userDTO.getPassword());
		} else {
			response.setMessage("PASSWORD IS NOT SAME");
			response.setData(userDTO.getPassword());
		}
		return new ResponseEntity<ApiResponse>(response,
				userDTO == null ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.OK);
	}

}
