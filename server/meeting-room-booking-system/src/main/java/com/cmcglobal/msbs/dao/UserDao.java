package com.cmcglobal.msbs.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cmcglobal.msbs.entity.Group;
import com.cmcglobal.msbs.entity.User;
import com.cmcglobal.msbs.model.GroupDTO;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {

	@Query("SELECT u FROM User u WHERE u.mail = ?1")
	User findByMail(String mail);

	@Query("SELECT u FROM User u WHERE u.fullname LIKE ?1")
	List<User> findLikeFullname(String name);

	@Query("SELECT count(u) FROM User u")
	int getCountUser();

	@Query("SELECT count(u) FROM User u WHERE u.status = 'ACTIVE'")
	int getCountUserActive();

	@Query("SELECT u FROM User u WHERE u.token = ?1")
	User findByToken(String token);

	@Query("SELECT u FROM User u WHERE u.mail = ?1")
	List<User> checkUserMail(String mail);

	@Query("SELECT u FROM User u WHERE u.group = ?1 AND u.createdDate >= ?2 AND u.createdDate <= ?3")
	List<User> getByTimeAndGroup(Group group, Date timeFrom, Date timeTo);

	@Query("SELECT u FROM User u WHERE u.createdDate >= ?1 AND u.createdDate <= ?2")
	List<User> getByTime(Date timeFrom, Date timeTo);

	@Query("SELECT count(u) FROM User u WHERE u.group = ?1 AND u.createdDate >= ?2 AND u.createdDate <= ?3 AND u.status = ?4")
	Integer getCountUserByTimeGroupStatus(GroupDTO group, Date timeFrom, Date timeTo, String status);

	@Query("SELECT count(u) FROM User u WHERE u.createdDate >= ?1 AND u.createdDate <= ?2 AND u.status = ?3")
	Integer getCountUserByTimeStatus(Date timeFrom, Date timeTo, String status);
}
