package com.cmcglobal.msbs.service;

import java.util.List;

import com.cmcglobal.msbs.entity.Role;
import com.cmcglobal.msbs.model.RoleDTO;

public interface RoleService {
	Role save(RoleDTO roleDTO);

	Role update(RoleDTO roleDTO);

	void delete(RoleDTO roleDTO);

	RoleDTO getById(int id);

	List<RoleDTO> getAll();

	Role findById(int id);

}
