package com.cmcglobal.msbs.service.impl;

import static com.cmcglobal.msbs.utils.Constants.STATUS_PENDING;

import java.awt.print.Book;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.w3c.dom.stylesheets.LinkStyle;

import com.cmcglobal.msbs.dao.UserBookDao;
import com.cmcglobal.msbs.entity.UserBook;
import com.cmcglobal.msbs.entity.Group;
import com.cmcglobal.msbs.entity.BookItem;
import com.cmcglobal.msbs.entity.BookType;
import com.cmcglobal.msbs.entity.User;
import com.cmcglobal.msbs.model.UserBookDTO;
import com.cmcglobal.msbs.model.GroupDTO;
import com.cmcglobal.msbs.model.BookItemDTO;
import com.cmcglobal.msbs.model.BookTypeDTO;
import com.cmcglobal.msbs.model.UserDTO;
import com.cmcglobal.msbs.service.UserBookService;
import com.cmcglobal.msbs.service.GroupService;
import com.cmcglobal.msbs.service.BookItemService;
import com.cmcglobal.msbs.service.UserService;

@Service
public class UserBookServiceImpl implements UserBookService {

	@Autowired
	private UserBookDao userBookDao;

	@Autowired
	private UserService userService;

	@Autowired
	private BookItemService bookItemService;

	@Autowired
	private GroupService groupService;

	@Override
	public UserBook save(UserBookDTO userBookDTO) {
		userBookDTO.setStatus(STATUS_PENDING);
		UserBook userBook = userBookDao.save(convertEntity(userBookDTO));
		userBookDTO.setId(userBook.getId());
		return convertEntity(userBookDTO);
	}

	@Override
	public UserBook update(UserBookDTO userBookDTO) {
		userBookDTO.setCreateTime(getById(userBookDTO.getId()).getCreateTime());
		if (userBookDao.save(convertEntity(userBookDTO)) != null) {
			return convertEntity(userBookDTO);
		}
		return null;
	}

	@Override
	public void delete(UserBookDTO userBookDTO) {
		userBookDao.deleteById(userBookDTO.getId());
	}

	@Override
	public UserBook findById(int id) {
		return userBookDao.findById(id).get();
	}

	@Override
	public UserBookDTO getById(int id) {
		return convertDTO(findById(id));
	}

	@Override
	public List<UserBookDTO> getAll() {
		List<UserBookDTO> list = new ArrayList<>();
		List<UserBook> listUserBook = null;
		try {
			listUserBook = userBookDao.findAll();
			for (UserBook userBook : listUserBook) {
				list.add(convertDTO(userBook));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<UserBookDTO> getByDate(Date date) {
		List<UserBookDTO> listDTO = null;
		try {
			listDTO = new ArrayList<>();
			List<UserBook> listEntity = userBookDao.findByBookingDate(date);
			for (UserBook userBook : listEntity) {
				listDTO.add(convertDTO(userBook));
			}
			return listDTO;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listDTO;
	}

	@Override
	public List<UserBookDTO> getByMonth(Date date, Pageable page) {
		List<UserBookDTO> listDTO = null;
		try {
			listDTO = new ArrayList<>();
			List<UserBook> listEntity = userBookDao.findByBookingMonth(date, page);
			for (UserBook userBook : listEntity) {
				listDTO.add(convertDTO(userBook));
			}
			return listDTO;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listDTO;
	}

	private UserBook convertEntity(UserBookDTO userBookDTO) {
		UserBook userBook = new UserBook();
		userBook.setId(userBookDTO.getId());
		if (userService.findById(userBookDTO.getUser().getId()) != null) {
			User user = userService.findById(userBookDTO.getUser().getId());
			userBook.setUser(user);
		}
		if (userBookDTO.getAdmin() != null) {
			User user = userService.findById(userBookDTO.getAdmin().getId());
			userBook.setAdmin(user);
		}
		userBook.setTitle(userBookDTO.getTitle());
		BookItem bookItem = bookItemService.findById(userBookDTO.getBookItemId());
		if (bookItem != null) {
			BookItem bookItemEntity = new BookItem();
			bookItemEntity.setId(bookItem.getId());
			bookItemEntity.setName(bookItem.getName());
			bookItemEntity.setBookType(new BookType(bookItem.getBookType().getId(), bookItem.getBookType().getName(),
					bookItem.getBookType().getDescription()));
			userBook.setBookItem(bookItemEntity);
		}
		userBook.setCreatedDate(userBookDTO.getCreateTime());
		userBook.setLastModifiedDate(userBookDTO.getUpdateTime());
		userBook.setBookingDate(userBookDTO.getBookingDate());
		userBook.setTimeFrom(userBookDTO.getTimeFrom());
		userBook.setTimeTo(userBookDTO.getTimeTo());
		userBook.setPurpose(userBookDTO.getPurpose());
		userBook.setStatus(userBookDTO.getStatus());
		userBook.setRejectReason(userBookDTO.getRejectReason());
		userBook.setHours();
		return userBook;
	}

	private UserBookDTO convertDTO(UserBook userBook) {
		UserBookDTO userBookDTO = new UserBookDTO();
		userBookDTO.setId(userBook.getId());
		if (userService.findById(userBook.getUser().getId()) != null) {
			User user = userService.findById(userBook.getUser().getId());
			Group group = groupService.findById(userBook.getUser().getGroup().getId());
			UserDTO userDTO = new UserDTO();
			userDTO.setId(user.getId());
			userDTO.setFullname(user.getFullname());
			userDTO.setGroup(new GroupDTO(group.getId(), group.getName()));
			userBookDTO.setUser(userDTO);
		}
		if (userBook.getAdmin() != null) {
			if (userService.findById(userBook.getAdmin().getId()) != null) {
				User admin = userService.findById(userBook.getAdmin().getId());
				userBookDTO.setAdmin(new UserDTO(admin.getId(), admin.getFullname()));
			}
		}
		userBookDTO.setTitle(userBook.getTitle());
		BookItem bookItem = bookItemService.findById(userBook.getBookItem().getId());
		if (bookItem != null) {
			userBookDTO.setBookItemId(bookItem.getId());
			BookItemDTO bookItemDTO = new BookItemDTO();
			bookItemDTO.setId(bookItem.getId());
			bookItemDTO.setName(bookItem.getName());
			bookItemDTO.setDescription(bookItem.getDescription());
			bookItemDTO.setBookType(new BookTypeDTO(bookItem.getBookType().getId(), bookItem.getBookType().getName(),
					bookItem.getBookType().getDescription()));
			userBookDTO.setBookItem(bookItemDTO);
		}
		userBookDTO.setHours(userBook.getHours());
		userBookDTO.setCreateTime(userBook.getCreatedDate());
		userBookDTO.setUpdateTime(userBook.getLastModifiedDate());
		userBookDTO.setBookingDate(userBook.getBookingDate());
		userBookDTO.setTimeFrom(userBook.getTimeFrom());
		userBookDTO.setTimeTo(userBook.getTimeTo());
		userBookDTO.setPurpose(userBook.getPurpose());
		userBookDTO.setRejectReason(userBook.getRejectReason());
		userBookDTO.setStatus(userBook.getStatus());
		return userBookDTO;
	}

	@Override
	public int getCountByBookingDate(Date date) {
		return userBookDao.getCountByBookingDate(date);
	}

	@Override
	public int getCountByMonth(Date date) {
		return userBookDao.getCountByMonth(date);
	}

	@Override
	public int getCountByStatus(Date date, String status) {
		return userBookDao.getCountByBookingDateStatus(date, status);
	}

	@Override
	public int getCountByMonthStatus(Date date, String status) {
		return userBookDao.getCountByMonthStatus(date, status);
	}

	@Override
	public List<UserBookDTO> getByMonth(Date date) {
		if (userBookDao.findUserBookByMonth(date) != null) {
			List<UserBookDTO> list = new ArrayList<UserBookDTO>();
			List<UserBook> userBooks = userBookDao.findUserBookByMonth(date);
			for (UserBook userBook : userBooks) {
				list.add(convertDTO(userBook));
			}
			return list;
		}
		return null;
	}

	@Override
	public List<UserBookDTO> getByWeek(Date date) {
		if (userBookDao.findUserBookByWeek(date) != null) {
			List<UserBookDTO> list = new ArrayList<>();
			for (UserBook userBook : userBookDao.findUserBookByWeek(date)) {
				list.add(convertDTO(userBook));
			}
			return list;
		}
		return null;
	}

	@Override
	public UserBook updateStatus(UserBookDTO userBook, String status) {
		userBook.setStatus(status);
		userBookDao.save(convertEntity(userBook));
		return convertEntity(userBook);
	}

	@Override
	public List<UserBookDTO> getByTimeFromAndTimeTo(Date timeFrom, Date timeTo, BookItem bookItem) {

		List<UserBookDTO> list = new ArrayList<>();
		List<UserBook> listEntity = userBookDao.findByTimeFromAndTimeTo(timeFrom, timeTo, bookItem);
		if (listEntity != null) {
			for (UserBook userBook : listEntity) {
				list.add(convertDTO(userBook));
			}
		}
		return list;
	}

	@Override
	public List<UserBookDTO> getByUser(User user, String bookingStatus) {
		List<UserBookDTO> list = new ArrayList<>();
		if (userBookDao.findByUser(user, bookingStatus) != null) {
			for (UserBook userBook : userBookDao.findByUser(user, bookingStatus)) {
				list.add(convertDTO(userBook));
			}
		}
		return list;
	}

	@Override
	public int getTotalPages(int elementByPage) {
		return (userBookDao.getCountUserBook() / elementByPage) + 1;
	}

	@Override
	public List<UserBookDTO> getByDate(Date date, Pageable page) {
		if (userBookDao.findByBookingDate(date, page) != null) {
			List<UserBookDTO> list = new ArrayList<>();
			for (UserBook userBook : userBookDao.findByBookingDate(date, page)) {
				list.add(convertDTO(userBook));
			}
			return list;
		}
		return null;
	}

	@Override
	public List<UserBookDTO> getByWeek(Date date, Pageable page) {
		if (userBookDao.findByBookingWeek(date, page) != null) {
			List<UserBookDTO> list = new ArrayList<>();
			for (UserBook userBook : userBookDao.findByBookingWeek(date, page)) {
				list.add(convertDTO(userBook));
			}
			return list;
		}
		return null;
	}

	@Override
	public List<UserBookDTO> getByTimeAndGroup(GroupDTO groupDTO, Date timeFrom, Date timeTo, BookType bookType) {
		List<UserBookDTO> list = new ArrayList<>();
		List<UserBook> userBooks = null;
		try {
			Group g = new Group();
			g.setId(groupDTO.getId());
			userBooks = userBookDao.findByTimeAndGroup(g, timeFrom, timeTo, bookType);
			for (UserBook userBook : userBooks) {
				list.add(convertDTO(userBook));
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<UserBookDTO> getByTime(Date timeFrom, Date timeTo, BookType bookType) {
		List<UserBookDTO> list = new ArrayList<>();
		List<UserBook> userBooks = null;
		try {
			userBooks = userBookDao.findByTime(timeFrom, timeTo, bookType);
			for (UserBook userBook : userBooks) {
				list.add(convertDTO(userBook));
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int getCountUserBookByTimeGroupStatus(GroupDTO group, Date timeFrom, Date timeTo, String status,
			BookType bookType) {
		int count = 0;
		try {
			Group g = new Group();
			g.setId(group.getId());
			count = userBookDao.getCountUserBookByTimeGroupStatus(g, timeFrom, timeTo, status, bookType);
		} catch (Exception e) {
		}
		return count;
	}

	@Override
	public int getCountUserBookByTimeStatus(Date timeFrom, Date timeTo, String status, BookType bookType) {
		int count = 0;
		try {
			if (userBookDao.getCountUserBookByTimeStatus(timeFrom, timeTo, status, bookType) != 0) {
				count = userBookDao.getCountUserBookByTimeStatus(timeFrom, timeTo, status, bookType);
			}
		} catch (Exception e) {
		}
		return count;
	}

	@Override
	public int getHourseUserBookGroupStatus(GroupDTO group, Date timeFrom, Date timeTo, String status,
			BookType bookType) {
		int count = 0;
		try {
			Group g = new Group();
			g.setId(group.getId());
			count = userBookDao.getHourseUserBookGroupStatus(g, timeFrom, timeTo, status, bookType);
		} catch (Exception e) {
		}
		return count;
	}

	@Override
	public int getHourseUserBookStatus(Date timeFrom, Date timeTo, String status, BookType bookType) {
		int count = 0;
		try {
			count = userBookDao.getHourseUserBookStatus(timeFrom, timeTo, status, bookType);
		} catch (Exception e) {
			System.err.println("Get hourse userbook by status" + e);
		}
		return count;
	}

	@Override
	public int getHourseUserBookGroup(GroupDTO group, Date timeFrom, Date timeTo, BookType bookType) {
		int count = 0;
		try {
			Group g = new Group();
			g.setId(group.getId());
			count = userBookDao.getHourseUserBookGroup(g, timeFrom, timeTo, bookType);
		} catch (Exception e) {
		}
		return count;
	}

	@Override
	public int getHourseUserBook(Date timeFrom, Date timeTo, BookType bookType) {
		int count = 0;
		try {
			if (userBookDao.getHourseUserBook(timeFrom, timeTo, bookType) != 0) {
				count = userBookDao.getHourseUserBook(timeFrom, timeTo, bookType);
			}
		} catch (Exception e) {
		}
		return count;
	}

	@Override
	public List<UserBookDTO> getByUser(User user) {
		List<UserBookDTO> userBooks = new ArrayList<>();
		List<UserBook> userBooksEntity = null;
		try {
			userBooksEntity = userBookDao.findByUser(user);
			for (UserBook userBook : userBooksEntity) {
				userBooks.add(convertDTO(userBook));
			}
			return userBooks;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int getCountByWeek(Date date) {
		return userBookDao.getCountByWeek(date);
	}

	@Override
	public int getCountByWeekStatus(Date date, String status) {
		return userBookDao.getCountByWeekStatus(date, status);
	}

	@Override
	public List<UserBookDTO> getByBookType(BookType bookType) {
		List<UserBookDTO> listDTO = null;
		try {
			listDTO = new ArrayList<>();
			List<UserBook> listEntity = userBookDao.getByBookType(bookType);
			for (UserBook userBook : listEntity) {
				listDTO.add(convertDTO(userBook));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listDTO;
	}

	@Override
	public List<UserBookDTO> getByDate(Date date, int bookItemId) {
		List<UserBookDTO> listDTO = null;
		try {
			listDTO = new ArrayList<>();
			BookItem bookItem = bookItemService.findById(bookItemId);
			List<UserBook> listEntity = userBookDao.getByDateAndItem(date, bookItem);
			for (UserBook userBook : listEntity) {
				listDTO.add(convertDTO(userBook));
			}
			return listDTO;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listDTO;
	}
}
