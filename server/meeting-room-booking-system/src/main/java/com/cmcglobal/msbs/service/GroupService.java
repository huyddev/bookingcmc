package com.cmcglobal.msbs.service;

import java.util.List;

import com.cmcglobal.msbs.entity.Group;
import com.cmcglobal.msbs.model.GroupDTO;

public interface GroupService {
	Group save(GroupDTO groupDTO);

	Group update(GroupDTO groupDTO);

	void delete(GroupDTO groupDTO);

	Group findById(int id);

	GroupDTO getById(int id);

	List<GroupDTO> getByName(String name);

	List<GroupDTO> getAll();

}
