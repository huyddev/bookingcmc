package com.cmcglobal.msbs.controller;

import static com.cmcglobal.msbs.utils.Constants.STATUS_ACTIVE;
import static com.cmcglobal.msbs.utils.Constants.STATUS_INACTIVE;
import static com.cmcglobal.msbs.utils.Constants.STATUS_APPROVE;
import static com.cmcglobal.msbs.utils.Constants.STATUS_REJECT;
import static com.cmcglobal.msbs.utils.Constants.STATUS_PENDING;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cmcglobal.msbs.entity.BookType;
import com.cmcglobal.msbs.model.ApiResponse;
import com.cmcglobal.msbs.model.Dashboard;
import com.cmcglobal.msbs.model.UserBookDTO;
import com.cmcglobal.msbs.model.UserDTO;
import com.cmcglobal.msbs.service.BookTypeService;
import com.cmcglobal.msbs.service.UserBookService;
import com.cmcglobal.msbs.service.UserService;

@CrossOrigin(origins = "*")
@RestController
public class DashboardController {
	@Autowired
	private UserService userService;
	@Autowired
	private UserBookService userBookService;
	@Autowired
	private BookTypeService bookTypeService;

	@PostMapping(value = "/admin/dashboard-get-all")
	public ResponseEntity<ApiResponse> getAllNumberDashboard(@RequestBody Dashboard dashboard) {
		ApiResponse response = new ApiResponse();
		// get number user
		if (dashboard.getGroup() != null) {
			List<Dashboard> listDashboard = new ArrayList<>();
			// users
			Dashboard user = new Dashboard();
			List<UserDTO> users = userService.getUserByTimeAndGroup(dashboard.getGroup(), dashboard.getTimeFrom(),
					dashboard.getTimeTo());
			if (users != null) {
				user.setList(users);
				user.setTotal(users.size());
				user.setTotalActive(userService.getCountUserByTimeStatus(dashboard.getTimeFrom(), dashboard.getTimeTo(),
						STATUS_ACTIVE));
				user.setTotalInactive(userService.getCountUserByTimeStatus(dashboard.getTimeFrom(),
						dashboard.getTimeTo(), STATUS_INACTIVE));
				listDashboard.add(user);
			} else {
				listDashboard.add(user);
			}

			// UserBook theo Booktype
			for (BookType b : bookTypeService.findByStatus(STATUS_ACTIVE)) {
				Dashboard userBook = new Dashboard();
				List<UserBookDTO> userBooks = userBookService.getByTimeAndGroup(dashboard.getGroup(),
						dashboard.getTimeFrom(), dashboard.getTimeTo(), b);
				userBook.setList(userBooks);
				userBook.setBookType(bookTypeService.getById(b.getId()));
				userBook.setTotalActive(userBookService.getCountUserBookByTimeGroupStatus(dashboard.getGroup(),
						dashboard.getTimeFrom(), dashboard.getTimeTo(), STATUS_APPROVE, b));
				userBook.setTotalInactive(userBookService.getCountUserBookByTimeGroupStatus(dashboard.getGroup(),
						dashboard.getTimeFrom(), dashboard.getTimeTo(), STATUS_REJECT, b));
				userBook.setTotalHourse(userBookService.getHourseUserBookGroup(dashboard.getGroup(),
						dashboard.getTimeFrom(), dashboard.getTimeTo(), b));
				userBook.setTotalHourseActive(userBookService.getHourseUserBookGroupStatus(dashboard.getGroup(),
						dashboard.getTimeFrom(), dashboard.getTimeTo(), STATUS_APPROVE, b));
				userBook.setTotalHourseInactive(userBookService.getHourseUserBookGroupStatus(dashboard.getGroup(),
						dashboard.getTimeFrom(), dashboard.getTimeTo(), STATUS_REJECT, b));
				listDashboard.add(userBook);
			}
			response.setData(listDashboard);
		} else {
			List<Dashboard> listDashboard = new ArrayList<>();
			// users
			Dashboard user = new Dashboard();
			List<UserDTO> users = userService.getByTime(dashboard.getTimeFrom(), dashboard.getTimeTo());
			if (users != null) {
				user.setList(users);
				user.setTotal(users.size());
				user.setTotalActive(userService.getCountUserByTimeStatus(dashboard.getTimeFrom(), dashboard.getTimeTo(),
						STATUS_ACTIVE));
				user.setTotalInactive(userService.getCountUserByTimeStatus(dashboard.getTimeFrom(),
						dashboard.getTimeTo(), STATUS_INACTIVE));
				listDashboard.add(user);
			} else {
				listDashboard.add(user);
			}
			// UserBook theo Booktype
			for (BookType b : bookTypeService.findByStatus(STATUS_ACTIVE)) {
				Dashboard userBook = new Dashboard();
				List<UserBookDTO> userBooks = userBookService.getByTime(dashboard.getTimeFrom(), dashboard.getTimeTo(),
						b);
				userBook.setList(userBooks);
				userBook.setBookType(bookTypeService.getById(b.getId()));
				userBook.setTotal(userBooks.size());
				userBook.setTotalActive(userBookService.getCountUserBookByTimeStatus(dashboard.getTimeFrom(),
						dashboard.getTimeTo(), STATUS_APPROVE, b));
				userBook.setTotalInactive(userBookService.getCountUserBookByTimeStatus(dashboard.getTimeFrom(),
						dashboard.getTimeTo(), STATUS_REJECT, b));
				userBook.setTotalHourse(
						userBookService.getHourseUserBook(dashboard.getTimeFrom(), dashboard.getTimeTo(), b));
				userBook.setTotalHourseActive(userBookService.getHourseUserBookStatus(dashboard.getTimeFrom(),
						dashboard.getTimeTo(), STATUS_APPROVE, b));
				userBook.setTotalHourseInactive(userBookService.getHourseUserBookStatus(dashboard.getTimeFrom(),
						dashboard.getTimeTo(), STATUS_REJECT, b));
				listDashboard.add(userBook);
			}
			response.setData(listDashboard);
		}

		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}
}
