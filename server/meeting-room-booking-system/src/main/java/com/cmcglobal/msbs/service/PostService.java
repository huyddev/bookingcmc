package com.cmcglobal.msbs.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cmcglobal.msbs.entity.Post;
import com.cmcglobal.msbs.model.PostDTO;

@Service
public interface PostService {
	Post save(PostDTO postDTO);

	Post update(PostDTO postDTO);

	void delete(PostDTO postDTO);

	PostDTO getById(int id);

	Post findById(int id);

	List<PostDTO> getAll();

	List<PostDTO> getAllActive();
}
