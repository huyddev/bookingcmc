package com.cmcglobal.msbs.model;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class UserDTO extends AuditingDTO {
	private int id;
	private String mail;
	private String fullname;
	private String password;
	private String phone;
	private String avata;
	private GroupDTO group;
	private int groupId;
	private List<RoleDTO> listRole;
	private String status;
	private UserDTO createBy;
	private UserDTO updateBy;
	private MultipartFile imageFile;
	private String token;
//	private Date expiryToken;

	public UserDTO() {

	}

	public UserDTO(int id, String fullname) {
		super();
		this.id = id;
		this.fullname = fullname;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserDTO getCreateBy() {
		return createBy;
	}

	public void setCreateBy(UserDTO createBy) {
		this.createBy = createBy;
	}

	public UserDTO getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(UserDTO updateBy) {
		this.updateBy = updateBy;
	}

	public List<RoleDTO> getListRole() {
		return listRole;
	}

	public void setListRole(List<RoleDTO> listRole) {
		this.listRole = listRole;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAvata() {
		return avata;
	}

	public void setAvata(String avata) {
		this.avata = avata;
	}

	public GroupDTO getGroup() {
		return group;
	}

	public void setGroup(GroupDTO group) {
		this.group = group;
	}

	public MultipartFile getImageFile() {
		return imageFile;
	}

	public void setImageFile(MultipartFile imageFile) {
		this.imageFile = imageFile;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

//	public Date getExpiryToken() {
//		return expiryToken;
//	}
//
//	public void setExpiryToken(Date expiryToken) {
//		this.expiryToken = expiryToken;
//	}
//
//	public void setExpiryToken(int minutes) {
//		Calendar now = Calendar.getInstance();
//		now.add(Calendar.MINUTE, minutes);
//		this.expiryToken = now.getTime();
//	}
//
//	public boolean isExpired() {
//		return new Date().after(this.expiryToken);
//	}

}
