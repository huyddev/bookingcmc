package com.cmcglobal.msbs.service;

import java.util.Date;

import com.cmcglobal.msbs.entity.Group;
import com.cmcglobal.msbs.model.Dashboard;

public interface DashboardService {
	Dashboard getData(Group group, Date timeFrom, Date timeTo);
}
