package com.cmcglobal.msbs.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cmcglobal.msbs.config.JwtTokenUtils;
import com.cmcglobal.msbs.entity.Group;
import com.cmcglobal.msbs.model.ApiResponse;
import com.cmcglobal.msbs.model.ErrorObject;
import com.cmcglobal.msbs.model.GroupDTO;
import com.cmcglobal.msbs.model.UserDTO;
import com.cmcglobal.msbs.service.GroupService;
import com.cmcglobal.msbs.service.UserService;

@CrossOrigin(origins = "*")
@RestController
public class GroupController {
	
	private final int ID_null = 5;
	@Autowired
	private GroupService groupService;
	
	@Autowired
	private UserService userService;

	@Autowired
	private JwtTokenUtils jwtTokenUtils;
	
	@GetMapping(value = "/groups")
	private ResponseEntity<ApiResponse> getAll() {
		ApiResponse response = new ApiResponse();
		if (groupService.getAll() != null) {
			response.setData(groupService.getAll());
			response.setMessage("GET ALL");
		} else {
			ErrorObject error = new ErrorObject(1, "List group not found");
			response.setError(error);
			response.setMessage("error");
		}
		return new ResponseEntity<ApiResponse>(response,
				groupService.getAll() != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@GetMapping(value = "/group")
	private ResponseEntity<ApiResponse> getById(@RequestParam("id") Integer id) {
		ApiResponse response = new ApiResponse();
		if (groupService.getById(id) != null) {
			response.setData(groupService.getById(id));
			response.setMessage("GET BY ID");
		} else {
			ErrorObject error = new ErrorObject(1, "group not found");
			response.setError(error);
			response.setMessage("error");
		}
		return new ResponseEntity<ApiResponse>(response,
				groupService.getById(id) != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping(value = "/admin/groups")
	private ResponseEntity<ApiResponse> create(@RequestBody GroupDTO groupDTO, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		String headers = request.getHeader("Authorization");
		String token = null;
		if (headers != null && headers.startsWith("Token ")) {
			token = headers.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		UserDTO user = userService.getById(user_id);
		groupDTO.setCreateBy(user);
		Group group = groupService.save(groupDTO);
		response.setData(group);
		response.setMessage("CREATE GROUP");
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	@PutMapping(value = "/admin/groups")
	private ResponseEntity<ApiResponse> update(@RequestBody GroupDTO groupDTO, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		String headers = request.getHeader("Authorization");
		String token = null;
		if (headers != null && headers.startsWith("Token ")) {
			token = headers.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		UserDTO user = userService.getById(user_id);
		groupDTO.setUpdateBy(user);
		Group group = groupService.update(groupDTO);
		response.setData(group);
		response.setMessage("UPDATE GROUP");
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	@DeleteMapping(value = "/admin/groups")
	private ResponseEntity<ApiResponse> deleteGroup(@RequestParam(name = "id") Integer id, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		
		String header = request.getHeader("Authorization");
		String token = null;
		if(header!=null&& header.startsWith("Token ")) {
			token = header.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		GroupDTO groupDTO = groupService.getById(id);
		groupDTO.setUpdateBy(userService.getById(user_id));
		groupService.delete(groupDTO);
		for (UserDTO userDTO : groupDTO.getListUser()) {
			UserDTO user = userService.getById(userDTO.getId());
			user.setGroup(groupService.getById(ID_null));
			user.setUpdateBy(userService.getById(user_id));
			userService.update(userDTO);
		}
		
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("delete", "OK");
		response.setData(data);
		response.setMessage("DELETE GROUP");
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}
}
