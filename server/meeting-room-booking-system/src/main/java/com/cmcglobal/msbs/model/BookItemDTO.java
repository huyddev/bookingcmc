package com.cmcglobal.msbs.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class BookItemDTO extends AuditingDTO {
	private int id;
	private String name;
	private String description;
	private BookTypeDTO bookType;
	private String status;
	private UserDTO createBy;
	private UserDTO updateBy;

	public BookItemDTO() {

	}

	public BookItemDTO(int id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BookTypeDTO getBookType() {
		return bookType;
	}

	public void setBookType(BookTypeDTO bookType) {
		this.bookType = bookType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public UserDTO getCreateBy() {
		return createBy;
	}

	public void setCreateBy(UserDTO createBy) {
		this.createBy = createBy;
	}

	public UserDTO getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(UserDTO updateBy) {
		this.updateBy = updateBy;
	}

}
