package com.cmcglobal.msbs.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cmcglobal.msbs.config.JwtTokenUtils;
import com.cmcglobal.msbs.entity.Role;
import com.cmcglobal.msbs.model.ApiResponse;
import com.cmcglobal.msbs.model.ErrorObject;
import com.cmcglobal.msbs.model.RoleDTO;
import com.cmcglobal.msbs.model.UserDTO;
import com.cmcglobal.msbs.service.RoleService;
import com.cmcglobal.msbs.service.UserService;

@CrossOrigin(origins = "*")
@RestController
public class RoleController {
	@Autowired
	private RoleService roleService;

	@Autowired
	private UserService userService;

	@Autowired
	private JwtTokenUtils jwtTokenUtils;

	@PostMapping(value = "/admin/roles")
	private ResponseEntity<ApiResponse> create(@RequestBody RoleDTO roleDTO, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		String headers = request.getHeader("Authorization");
		String token = null;
		if (headers != null && headers.startsWith("Token ")) {
			token = headers.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		UserDTO user = userService.getById(user_id);
		roleDTO.setCreateBy(user);
		Role role = roleService.save(roleDTO);
		response.setData(role);
		response.setMessage("CREATE ROLE");
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);

	}

	@PutMapping(value = "/admin/roles")
	private ResponseEntity<ApiResponse> update(@RequestBody RoleDTO roleDTO, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		String headers = request.getHeader("Authorization");
		String token = null;
		if (headers != null && headers.startsWith("Token ")) {
			token = headers.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		UserDTO user = userService.getById(user_id);
		roleDTO.setUpdateBy(user);
		Role role = roleService.update(roleDTO);
		response.setData(role);
		response.setMessage("UPDATE ROLE");
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	@DeleteMapping(value = "/admin/roles")
	private ResponseEntity<ApiResponse> delete(@RequestBody RoleDTO roleDTO) {
		ApiResponse response = new ApiResponse();
		roleService.delete(roleDTO);
		Map<String, Object> data = new HashMap<String, Object>();
		response.setData(data);
		response.setMessage("DELETE ROLE");
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	@GetMapping(value = "/admin/roles")
	private ResponseEntity<List<RoleDTO>> getAll() {
		ApiResponse response = new ApiResponse();
		if (roleService.getAll() != null) {
			response.setData(roleService.getAll());
			response.setMessage("GET ALL");
		} else {
			ErrorObject error = new ErrorObject(1, "list role not found");
			response.setError(error);
			response.setMessage("error");
		}
		return new ResponseEntity<List<RoleDTO>>(roleService.getAll(),
				roleService.getAll() != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}


	@GetMapping(value = "/role")
	private ResponseEntity<ApiResponse> getById(@RequestParam("id") Integer id) {
		ApiResponse response = new ApiResponse();
		if (roleService.getById(id) != null) {
			response.setData(roleService.getById(id));
			response.setMessage("GET BY ID");
		} else {
			ErrorObject error = new ErrorObject(1, "role not found");
			response.setError(error);
			response.setMessage("error");
		}
		return new ResponseEntity<ApiResponse>(response,
				roleService.getById(id) != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
