package com.cmcglobal.msbs.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cmcglobal.msbs.config.JwtTokenUtils;
import com.cmcglobal.msbs.entity.BookItem;
import com.cmcglobal.msbs.entity.BookType;
import com.cmcglobal.msbs.model.ApiResponse;
import com.cmcglobal.msbs.model.BookItemDTO;
import com.cmcglobal.msbs.model.BookTypeDTO;
import com.cmcglobal.msbs.model.ErrorObject;
import com.cmcglobal.msbs.model.UserDTO;
import com.cmcglobal.msbs.service.BookItemService;
import com.cmcglobal.msbs.service.BookTypeService;
import com.cmcglobal.msbs.service.UserService;

@CrossOrigin(origins = "*")
@RestController
public class BookItemController {
	@Autowired
	private BookItemService bookItemService;

	@Autowired
	private UserService userService;

	@Autowired
	private BookTypeService bookTypeService;

	@Autowired
	private JwtTokenUtils jwtTokenUtils;

	@GetMapping(value = "/list-book-item")
	private ResponseEntity<List<BookItemDTO>> getAll() {
		ApiResponse response = new ApiResponse();
		if (bookItemService.getAll() != null) {
			response.setData(bookItemService.getAll());
			response.setMessage("GET ALL");
		} else {
			ErrorObject error = new ErrorObject(1, "list bookItem not found");
			response.setError(error);
			response.setMessage("error");
		}
		return new ResponseEntity<List<BookItemDTO>>(bookItemService.getAll(),
				bookItemService.getAll() != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@GetMapping(value = "/book-item")
	private ResponseEntity<ApiResponse> getById(@RequestParam(name = "id") Integer id) {
		ApiResponse response = new ApiResponse();
		if (bookItemService.getById(id) != null) {
			response.setData(bookItemService.getById(id));
			response.setMessage("GET BY ID");
		} else {
			ErrorObject error = new ErrorObject(1, "book-item not found");
			response.setError(error);
			response.setMessage("error");
		}
		return new ResponseEntity<ApiResponse>(response,
				bookItemService.getById(id) != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping(value = "/admin/book-item")
	private ResponseEntity<ApiResponse> createRoom(@RequestBody BookItemDTO bookItemDTO, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		String headers = request.getHeader("Authorization");
		String token = null;
		if (headers != null && headers.startsWith("Token ")) {
			token = headers.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		UserDTO user = userService.getById(user_id);
		bookItemDTO.setCreateBy(user);
		BookItem bookItem = bookItemService.save(bookItemDTO);
		response.setData(bookItemService.findById(bookItem.getId()));
		response.setMessage("CREATE BOOK-ITEM");
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	@PutMapping(value = "/admin/book-item")
	private ResponseEntity<ApiResponse> updateRoom(@RequestBody BookItemDTO bookItemDTO, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();
		String headers = request.getHeader("Authorization");
		String token = null;
		if (headers != null && headers.startsWith("Token ")) {
			token = headers.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		UserDTO user = userService.getById(user_id);
		bookItemDTO.setUpdateBy(user);
		BookItem bookItem = bookItemService.update(bookItemDTO);
		response.setData(bookItemService.getById(bookItem.getId()));
		response.setMessage("UPDATE ROOM");
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	@DeleteMapping(value = "/admin/book-item")
	private ResponseEntity<ApiResponse> deleteRoom(@RequestParam("id") Integer id, HttpServletRequest request) {
		ApiResponse response = new ApiResponse();

		String header = request.getHeader("Authorization");
		String token = null;
		if (header != null && header.startsWith("Token ")) {
			token = header.substring(6);
		}
		int user_id = jwtTokenUtils.getIdFromToken(token);
		BookItemDTO bookItemDTO = bookItemService.getById(id);
		bookItemDTO.setUpdateBy(userService.getById(user_id));
		bookItemService.delete(bookItemDTO);

		Map<String, Object> data = new HashMap<String, Object>();
		data.put("delete", "OK");
		response.setData(data);
		response.setMessage("DELETE BOOK ITEM");
		return new ResponseEntity<ApiResponse>(response, HttpStatus.OK);
	}

	@GetMapping(value = "/get-by-book-type")
	private ResponseEntity<ApiResponse> getByBookType(@RequestParam("id") Integer id) {
		ApiResponse response = new ApiResponse();
		BookTypeDTO bookTypeDTO = bookTypeService.getById(id);
		response.setData(bookItemService.getByBookType(bookTypeDTO));

		return new ResponseEntity<ApiResponse>(response,
				bookItemService.getByBookType(bookTypeDTO) != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
