import { User } from './user';

export class Role {
    public id: number;
    public name: string;
    public users: User[];
    public status: string;
    public createBy: User;
    public updateBy: User;
    public createTime: Date;
    public updateTime: Date;
    // public member: boolean;
    // public admin: boolean;
    // public manager: boolean;
}
