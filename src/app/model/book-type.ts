import { BookItem } from './book-item';
import { User } from './user';

export class BookType {
    public id: number;
    public name: string;
    public description: string;
    public bookItems: BookItem[];
    public status: string;
    public createBy: User;
    public updateBy: User;
    public createTime: Date;
    public updateTime: Date;
}
