import { Group } from './group';
import { User } from './user';
import { UserBook } from './user-book';
import { BookType } from './book-type';

export class Dashboard {
    public group: any;
    public timeFrom: string;
    public timeTo: string;
    public bookType: BookType;
    public list: any[];
    public total: number;
    public totalActive: number;
    public totalInactive: number;
    public totalHourse: number;
    public totalHourseActive: number;
    public totalHourseInactice: number;
}
