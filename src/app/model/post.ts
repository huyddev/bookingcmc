import { User } from './user';

export class Post {
    public id: number;
    public title?: string;
    public brief?: string;
    public content?: string;
    public author?: User;
    public image?: string;
    public status?: string;
    public updateBy?: User;
}
