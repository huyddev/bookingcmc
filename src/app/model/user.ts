import { Group } from './group';
import { Role } from './role';

export class User {
    public id: number;
    public mail: string;
    public password: string;
    public fullname: string;
    public phone: string;
    public avata: string;
    public imageFile: File;
    public group: Group;
    public listRole: Role[] = [];
    public status: string;
    public createBy: User;
    public updateBy: User;
    public createTime: Date;
    public updateTime: Date;
}
