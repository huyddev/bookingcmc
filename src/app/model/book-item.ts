import { BookType } from './book-type';
import { User } from './user';

export class BookItem {
    public id: number;
    public name: string;
    public description: string;
    public bookType: BookType;
    public status: string;
    public createBy: User;
    public updateBy: User;
    public createTime: Date;
    public updateTime: Date;
}
