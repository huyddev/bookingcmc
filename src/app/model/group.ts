import { User } from './user';

export class Group {
    public id: number;
    public name: string;
    public parentGroup: Group;
    public subGroup: Group[];
    public listUser: User[];
    public status: string;
    public createBy: User;
    public updateBy: User;
    public createTime: Date;
    public updateTime: Date;
}
