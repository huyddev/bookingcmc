export class Mail {
    public from: string;
    public to: string;
    public subject: string;
    public model: object;
}
