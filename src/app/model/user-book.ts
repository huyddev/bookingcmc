import { User } from './user';
import { BookItem } from './book-item';
import { BookType } from './book-type';

export class UserBook {
    public id: number;
    public title: string;
    public user: User;
    public admin: User;
    public bookItem: BookItem;
    public bookType: BookType;
    public bookingDate: Date;
    public timeFrom: string;
    public timeTo: string;
    public purpose: string;
    public rejectReason: string;
    public status: string;
    public createBy: User;
    public updateBy: User;
    public createTime: Date;
    public updateTime: Date;
    public isDisabled: boolean;

}
