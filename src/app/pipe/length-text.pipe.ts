import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lengthText'
})
export class LengthTextPipe implements PipeTransform {

  transform(value: string, num: number): string {
    if (value) {
      if (value.length > num) {
        return value.slice(0, num) + '...';
      }
    }
    return value;
  }

}
