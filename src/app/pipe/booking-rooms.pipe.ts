import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bookingRooms'
})
export class BookingRoomsPipe implements PipeTransform {

  transform(value) {
    return value.slice(0, 19);
  }

}
