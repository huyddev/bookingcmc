import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { BookingHomeComponent } from "./pages/booking-home/booking-home.component";
import { UserProfileComponent } from "./pages/user-profile/user-profile.component";
import { LoginComponent } from "./pages/login/login.component";
import { RegisterComponent } from "./pages/register/register.component";
import { ForgotPasswordComponent } from "./pages/forgot-password/forgot-password.component";
import { TimelineComponent } from "./pages/timeline/timeline.component";
import { ConfirmForgotPasswordComponent } from './pages/confirm-forgot-password/confirm-forgot-password.component';
import { ConfirmRegisterComponent } from './pages/confirm-register/confirm-register.component';
import { PostsComponent } from './pages/posts/posts.component';
import { PostDetailComponent } from './pages/post-detail/post-detail.component';

const routes: Routes = [
  {
    path: "",
    redirectTo: "/home",
    pathMatch: "full"
  },
  {
    path: "home",
    component: BookingHomeComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "register",
    component: RegisterComponent
  },
  {
    path: "confirm-register",
    component: ConfirmRegisterComponent
  },
  {
    path: "forgot-password",
    component: ForgotPasswordComponent
  },
  {
    path: "confirm-forgot-password",
    component: ConfirmForgotPasswordComponent
  },
  {
    path: "user-profile",
    component: UserProfileComponent
  },
  {
    path: "timeline",
    component: TimelineComponent
  },
  {
    path: "posts",
    component: PostsComponent
  },
  {
    path: "post-detail/:id",
    component: PostDetailComponent
  }
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookingRoutingModule { }
