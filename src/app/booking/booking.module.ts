import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { BookingRoutingModule } from "./booking-routing.module";
import { BookingComponent } from "./booking.component";
import { BookingHomeComponent } from "./pages/booking-home/booking-home.component";
import { HeaderComponent } from "./shared/components/header/header.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgZorroAntdModule } from "ng-zorro-antd";
//import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import { SplitUserNamePipe } from './shared/components/header/split-user-name.pipe';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';

import '@progress/kendo-ui';
import { TimelineComponent } from './pages/timeline/timeline.component';
import { ConfirmForgotPasswordComponent } from './pages/confirm-forgot-password/confirm-forgot-password.component';
import { ConfirmRegisterComponent } from './pages/confirm-register/confirm-register.component';
import { MobileComponent } from './pages/mobile/mobile.component';
import { PostsComponent } from './pages/posts/posts.component';
import { PostDetailComponent } from './pages/post-detail/post-detail.component';

@NgModule({
  declarations: [
    BookingComponent,
    BookingHomeComponent,
    HeaderComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    UserProfileComponent,
    SplitUserNamePipe,
    TimelineComponent,
    ConfirmForgotPasswordComponent,
    ConfirmRegisterComponent,
    MobileComponent,
    PostsComponent,
    PostDetailComponent
  ],
  imports: [
    CommonModule,
    BookingRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgZorroAntdModule,
    NgbModule
  ]
})
export class BookingModule { }
