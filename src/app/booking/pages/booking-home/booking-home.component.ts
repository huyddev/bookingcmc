import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { UploadFile } from 'ng-zorro-antd/upload';
import { Router } from '@angular/router';

@Component({
  selector: 'app-booking-home',
  templateUrl: './booking-home.component.html',
  styleUrls: ['./booking-home.component.css']
})
export class BookingHomeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    let user = localStorage.getItem('user');
    if (user) {
      return this.router.navigate(["/home"])
    }
    return this.router.navigate(["/login"])
  }

}
