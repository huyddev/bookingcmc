import { Component, ElementRef, ViewChild, AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { _url } from 'src/app/service/url';

declare let kendo: any;

@Component({
    selector: 'app-timeline',
    templateUrl: './timeline.component.html',
    styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements AfterViewInit, OnDestroy, OnInit {

    @ViewChild('scheduler') schedulerEl: ElementRef;
    private token = '';
    selectedType: any = 1;
    constructor(private hostEl: ElementRef
        // private floorService: GetfloorService
    ) { }

    ngOnInit() {
    }

    ngAfterViewInit() {
        let dataLogin = JSON.parse(localStorage.getItem('user'));
        let token = dataLogin.token;
        let domain = _url.domain;
        let selectedType = 1;
        $.ajax({
            url: domain + 'list-book-type-active',
            contentType: 'application/json',
            type: 'GET',
            beforeSend: (req) => {
                req.setRequestHeader('Authorization', 'Token ' + token);
            },
            success: (result) => {
                let options;
                $.each(result.data, (idx, item) => {
                    options += `<option value=${item.id}>${item.name}</option>`;
                });
                $('#bookTypeSelect').html(options);
            }
        });
        $('#bookTypeSelect').change(() => {
            this.selectedType = $('#bookTypeSelect').children("option:selected").val();
            var filter = {
                logic: "or",
                filters: $.map([this.selectedType], function (value) {
                    return {
                        operator: "eq",
                        field: "bookType.id",
                        value: value
                    };
                })
            };
            scheduler.resources[0].dataSource.filter(filter);
        });
        let scheduler = kendo.jQuery(this.schedulerEl.nativeElement).kendoScheduler({
            height: '750',
            footer: false,
            date: new Date(),
            startTime: new Date('2013/6/13 07:00 AM'),
            endTime: new Date('2013/6/13 07:00 PM'),
            workDayStart: new Date("2013/1/1 7:00 AM"),
            workDayEnd: new Date("2013/1/1 7:00 PM"),
            eventHeight: 30,
            majorTick: 60,
            editable: {
                confirmation: 'Are you sure you want to delete this event?'
            },
            eventTemplate: function (event) {
                if (event.status === 'APPROVE') {
                    return `<div class="event-template" style="background-color:#27ae60; height:100%;">
                                <span>${event.title}</span>
                            </div>`
                }
                else if (event.status === 'REJECT') {
                    return `<div class="event-template" style="background-color:#c0392b; height:100%;">
                                <span>${event.title}</span>
                            </div>`
                }
                else {
                    // $('.k-event-actions').append(`<a href="#" class="k-link k-event-approve" title="Approve" aria-label="Approve"><span class="k-icon k-i-check"></span></a>`)
                    return `<div class="event-template" style="height:100%;">
                                <span>${event.title}</span>
                            </div>`;
                }
            },
            views: [
                {
                    type: 'timeline',
                    columnWidth: 30,
                    selectedDateFormat: "{0:dddd dd MMMM yyyy}",
                    selectedShortDateFormat: "{0:dd/MM/yyyy}"
                },
                {
                    type: 'timelineWeek',
                    columnWidth: 30,
                    selectedDateFormat: "{0:dddd dd MMMM yyyy} - {1:dddd dd MMMM yyyy}",
                    selectedShortDateFormat: "{0:dd/MM/yyyy} - {1:dd/MM/yyyy}"
                },
                {
                    type: 'timelineMonth',
                    majorTick: 720,
                    selectedDateFormat: "{0:MMMM yyyy}",
                    selectedShortDateFormat: "{0:MM/yyyy}"
                }],
            selectable: true,
            cancel: function (e) {
                // $('[role=gridcell]').removeClass('k-state-selected');
                // $('[role=gridcell]').attr('aria-selected', false);
                // scheduler.refresh();
                // e.preventDefault();
                // scheduler.dataSource.read();
                var event = scheduler.data()[0];
                scheduler.select([event.uid]);
                setTimeout(function () {
                    scheduler.select(null);
                }, 1000);
            },
            dataBinding: function (e) {
                this.view().element.on('mouseup', function (el) {
                    e.preventDefault();
                    let selection = scheduler.select();
                    if (selection.events && !selection.events.length) {
                        // no events are selected than this is create
                        scheduler.select(null);
                        scheduler.addEvent({
                            start: selection.start,
                            end: selection.end,
                            title: '',
                            bookItemId: selection.resources.bookItemId
                        });
                    }
                }.bind(this));
            },
            dataBound: function (e) {
                $(".k-event").each(function () {
                    var uid = $(this).data("uid");
                    var event = e.sender.occurrenceByUid(uid);
                    if (event.user && event.user.id !== dataLogin.user.id && !isAdmin(dataLogin.user.listRole)) {
                        $(this).find(".k-event-delete").hide();
                    }
                    // if (event.user && event.status === 'PENDING' && isAdmin(dataLogin.user.listRole)) {
                    //     $(this).find('.k-event-delete')
                    //         .parent()
                    //         .append(`<a class="k-link k-event-approve" title="Approve"
                    //                 aria-label="Approve" >
                    //                     <span class="k-icon k-i-check"></span>
                    //                 </a>`);
                    //     $('.k-event-approve').on('click', () => {
                    //         let uid = $(this).data('uid');
                    //         let event = scheduler.occurrenceByUid(uid);
                    //         let idx = scheduler.dataSource.indexOf(event);
                    //         console.log(idx);
                    //     });
                    // }
                });
            },
            edit: function (e) {
                if (e.event.user && e.event.user.id !== dataLogin.user.id) {
                    e.preventDefault();
                }
                else {
                    let container = e.container;
                    container.find('[name=isAllDay]').hide();
                    container.find('[for=isAllDay]').parent().remove();
                    container.find('[data-container-for=isAllDay]').remove();
                    container.find('[name=description]').hide();
                    container.find('[for=description]').parent().remove();
                    container.find('[data-container-for=description]').remove();
                    container.find('[name=recurrenceRule]').hide();
                    container.find('[for=recurrenceRule]').parent().remove();
                    container.find('[data-container-for=recurrenceRule]').remove();
                    container.find('[data-container-for=bookItemId]').find('span').first().width('100%');
                }

            },
            remove: function (e) {
                if (e.event.user && e.event.user.id !== dataLogin.user.id && !isAdmin(dataLogin.user.listRole)) {
                    e.preventDefault();
                }
            },
            resize: function (e) {
                if (e.event.user && e.event.user.id !== dataLogin.user.id && !isAdmin(dataLogin.user.listRole)) {
                    e.preventDefault();
                }
                if (roomIsOccupied(e.event.start, e.event.end, e.event, e.sender.resources)) {
                    this.wrapper.find(".k-marquee-color").addClass("invalid-slot");
                    e.preventDefault();
                }
            },
            resizeEnd: function (e) {
                if (e.event.user && e.event.user.id !== dataLogin.user.id && !isAdmin(dataLogin.user.listRole)) {
                    e.preventDefault();
                }
                if (roomIsOccupied(e.event.start, e.event.end, e.event, e.sender.resources)) {
                    this.wrapper.find(".k-marquee-color").addClass("invalid-slot");
                    e.preventDefault();
                }
            },
            move: function (e) {
                if (e.event.user && e.event.user.id !== dataLogin.user.id && !isAdmin(dataLogin.user.listRole)) {
                    e.preventDefault();
                }
                if (roomIsOccupied(e.event.start, e.event.end, e.event, e.sender.resources)) {
                    this.wrapper.find(".k-marquee-color").addClass("invalid-slot");
                    e.preventDefault();
                }
            },
            moveEnd: function (e) {
                if (e.event.user && e.event.user.id !== dataLogin.user.id && !isAdmin(dataLogin.user.listRole)) {
                    e.preventDefault();
                }
                if (roomIsOccupied(e.event.start, e.event.end, e.event, e.sender.resources)) {
                    this.wrapper.find(".k-marquee-color").addClass("invalid-slot");
                    e.preventDefault();
                }
            },
            add: function (e) {
                if (roomIsOccupied(e.event.start, e.event.end, e.event, e.sender.resources)) {
                    this.wrapper.find(".k-marquee-color").addClass("invalid-slot");
                    e.preventDefault();
                }
            },
            save: function (e) {

            },
            dataSource: {
                batch: true,
                transport: {
                    read: {
                        url: domain + 'list-user-book',
                        contentType: 'application/json',
                        type: 'GET',
                        beforeSend: (req) => {
                            req.setRequestHeader('Authorization', 'Token ' + token);
                        }
                    },
                    update: {
                        url: domain + 'user-book',
                        type: 'PUT',
                        contentType: 'application/json',
                        beforeSend: (req) => {
                            req.setRequestHeader('Authorization', 'Token ' + token);
                        },
                        complete: function () {
                            scheduler.dataSource.read();
                            var event = scheduler.data()[0];
                            scheduler.select([event.uid]);
                            setTimeout(function () {
                                scheduler.select(null);
                            }, 1000)
                        }
                    },
                    create: {
                        url: domain + 'user-book',
                        type: 'POST',
                        contentType: 'application/json',
                        beforeSend: (req) => {
                            req.setRequestHeader('Authorization', 'Token ' + token);
                        },
                        complete: function () {
                            scheduler.dataSource.read();
                            var event = scheduler.data()[0];
                            scheduler.select([event.uid]);
                            setTimeout(function () {
                                scheduler.select(null);
                            }, 1000)
                        }
                    },
                    destroy: {
                        url: domain + 'user-book',
                        type: 'DELETE',
                        contentType: 'application/json',
                        beforeSend: (req) => {
                            req.setRequestHeader('Authorization', 'Token ' + token);
                        },
                        complete: function () {
                            scheduler.dataSource.read();
                            var event = scheduler.data()[0];
                            scheduler.select([event.uid]);
                            setTimeout(function () {
                                scheduler.select(null);
                            }, 1000)
                        },
                    },

                    parameterMap: function (options, operation) {
                        if (operation !== 'read' && options.models) {
                            options.models[0].bookingDate = new Date();
                            console.log('user-book', options.models[0])
                            return kendo.stringify(options.models[0]);
                        }
                    }
                },
                schema: {
                    data: 'data',
                    model: {
                        id: 'id',
                        fields: {
                            id: { from: 'id', type: 'number' },
                            title: { from: 'title', defaultValue: 'Nội dung', validation: { required: true } },
                            start: { type: 'date', from: 'timeFrom' },
                            end: { type: 'date', from: 'timeTo' },
                            bookItemId: { from: 'bookItemId', nullable: true },
                            bookingDate: { from: 'bookingDate' },
                            status: { from: 'status' }
                        }
                    }
                }
            },
            group: {
                resources: ['BookType'],
                orientation: 'vertical'
            },
            resources: [
                {
                    field: 'bookItemId',
                    name: 'BookType',
                    dataTextField: 'name',
                    dataValueField: 'id',
                    dataSource: {
                        transport: {
                            read: {
                                url: domain + 'list-book-item',
                                contentType: 'application/json',
                                type: 'GET',
                                data: {
                                    id: this.selectedType
                                },
                                beforeSend: (req) => {
                                    req.setRequestHeader('Authorization', 'Token ' + token);
                                }
                            }
                        },
                        // schema: {
                        //     data: 'data'
                        // }
                    },
                    title: 'Book Type' // label
                }
            ]
        }).data('kendoScheduler');
        scheduler.resources[0].dataSource.filter({
            logic: "or",
            filters: $.map([this.selectedType], function (value) {
                return {
                    operator: "eq",
                    field: "bookType.id",
                    value: value
                };
            })
        });

        kendo.jQuery(this.schedulerEl.nativeElement).kendoTooltip({
            filter: ".k-event:not(.k-event-drag-hint) > div, .k-task",
            position: "top",
            content: kendo.template(`#var element = target.is(".k-task") ? target : target.parent();#
            #var uid = element.attr("data-uid");#
            #var scheduler = target.closest("[data-role=scheduler]").data("kendoScheduler");#
            #var model = scheduler.occurrenceByUid(uid);#
            #if(model) {#
                <strong>Title:</strong> #=model.title#<br />
                <strong>Start:</strong> #=kendo.format('{0:g}',model.start)#<br />
                <strong>End:</strong> #=kendo.format('{0:g}',model.end)#<br />
                <strong>User:</strong> #=model.user.fullname# - #=model.user.group.name#<br />
            #} else {#
                <strong>No event data is available</strong>
            #}#`)
        });

        function roomIsOccupied(start, end, event, resources) {
            var occurences = occurrencesInRangeByResource(start, end, "bookItemId", event, resources);
            if (occurences.length > 0)
                return true;
            return false;
        };

        function occurrencesInRangeByResource(start, end, resourceFieldName, event, resources) {
            var occurences = scheduler.occurrencesInRange(start, end);
            var idx = occurences.indexOf(event);
            if (idx > -1) {
                occurences.splice(idx, 1);
            }
            event = $.extend({}, event, resources);
            return filterByResource(occurences, resourceFieldName, event[resourceFieldName])
        }

        function filterByResource(occurences, resouceFiledName, value) {
            var result = [];
            var occurence;
            for (var idx = 0, length = occurences.length; idx < length; idx++) {
                occurence = occurences[idx];
                if (occurence[resouceFiledName] === value && occurence.status === 'APPROVE') {
                    result.push(occurence);
                }
            }
            return result;
        }

        function isAdmin(lstRole) {
            if (lstRole.filter(e => e.name === 'ADMIN').length > 0) {
                return true
            }
            else return false;
        }
    }

    ngOnDestroy() {
        kendo.destroy(this.hostEl.nativeElement);
    }
}
