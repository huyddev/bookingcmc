import { Component, OnInit } from "@angular/core";
import { RegisterService } from "src/app/service/register.service";
import { Router } from "@angular/router";
import { NzMessageService } from "ng-zorro-antd";
import { Group } from "src/app/model/group";
import { GroupService } from "src/app/service/group.service";

@Component({
  selector: "fmyp-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent implements OnInit {
  passwordVisible = false;
  password: string;
  groups: Group[] = [];
  selectedValue = null

  constructor(
    private registerService: RegisterService,
    private router: Router,
    private message: NzMessageService,
    private groupService: GroupService
  ) { }

  ngOnInit(): void {
    this.getAllGroup();
  }

  getAllGroup() {
    this.groupService._getAllGroup().subscribe(
      data => {
        const d: any = data;
        this.groups = d.data;
        console.log("group", this.groups);
      },
      error => {
        console.log("error", error);
      }
    );
  }

  submitForm(registerForm) {
    console.log("registerForm", registerForm);
    this.registerService.getUserRegistered(registerForm).subscribe(
      data => {
        console.log("user", data.data);
        localStorage.setItem("user", JSON.stringify(data.data));
        this.message.success("Check your mail");

        this.router.navigate(["/login"]);
      },
      error => {
        this.message.error("Register fail"),
          console.log("error", error);
      }
    );
  }
}
