import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/service/post.service';
import { User } from 'src/app/model/user';
import { Router } from '@angular/router';
import { HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { NzMessageService } from 'ng-zorro-antd';
import { UploadImageService } from 'src/app/service/upload-image.service';
import { Post } from 'src/app/model/post';

@Component({
  selector: 'fmyp-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts: Post[] = [];
  user: User = new User;
  totalPage: number = 10;
  pageIndex: number = 1;
  private token: string;

  constructor(
    private postService: PostService,
    private router: Router,
    private message: NzMessageService,
    private urlImage: UploadImageService
  ) { }

  ngOnInit(): void {
    let data = localStorage.getItem('user');
    if (data) {
      this.token = JSON.parse(data).token;
      this.user = JSON.parse(data).user;
    } else {
      this.router.navigate(['/login'])
    }
    console.log(this.token)
    this.postService._getAll(this.token).subscribe(
      res => {
        let d: any = res;
        this.posts = d.data;
        this.posts.map(
          e => {
            e.image = this.urlImage._urlImage(e.image);
            e.author.avata = this.urlImage._urlImage(e.author.avata);
          });
        this.totalPage = Math.ceil(this.posts.length / 5) * 10;
        console.log(d);
        console.log('List post', this.posts);
      },
      err => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.router.navigate(['/login'])
          }
        }
        this.message.error('Data not found')
      })
  }
}
