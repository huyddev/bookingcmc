import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RegisterService } from 'src/app/service/register.service';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'fmyp-confirm-register',
  templateUrl: "../confirm-register/confirm-register.component.html",
  styleUrls: ["../confirm-register/confirm-register.component.css"]
})
export class ConfirmRegisterComponent implements OnInit {

  token: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private r: RegisterService,
    private router: Router,
    private message: NzMessageService,
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.token = params['token'];
      console.log(this.token);
    });
  }

  ngOnInit() {
    this.r.confirm(this.token).subscribe(
      data => {
        console.log("data", data);
        if (data.data) {
          if (data.data.verify == 1) {
            this.message.success("success");
            this.router.navigate(["/login"]);
          }
        } else {
          this.message.error("Link fail", { nzDuration: 10000 });
          this.router.navigate(["/login"]);
        }
      },
      error => {
        this.message.error("fail"),
          console.log("error", error);
      }
    );
  }

}
