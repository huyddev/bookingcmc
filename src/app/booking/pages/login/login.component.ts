import { Component, OnInit } from "@angular/core";
import { LoginService } from "src/app/service/login.service";
import { Router } from "@angular/router";
import { NzMessageService } from "ng-zorro-antd";
import { role_ID } from 'src/app/service/url';

@Component({
  selector: "fmyp-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  passwordVisible: boolean = false;
  password: any;
  isSpinning: boolean = false;
  constructor(
    private loginService: LoginService,
    private router: Router,
    private message: NzMessageService
  ) { }

  ngOnInit(): void { }

  submitForm(loginForm) {
    this.isSpinning = true;
    console.log("loginForm", loginForm);
    this.loginService.getUserLogged(loginForm).subscribe(
      data => {
        console.log("user", data.data);
        localStorage.setItem("user", JSON.stringify(data.data));

        let isRole = true;
        data.data.user.listRole.forEach(e => {
          if (e.id == role_ID.MEMBER) {
            isRole = false;
          }
        });
        if (isRole) {
          this.router.navigate(["/admin/dashboard"]);
        } else {
          this.router.navigate(["/home"])
        }

      },
      error => {
        this.message.error("Login fail ");
        console.log("error", error);
        this.isSpinning = false;

      },
      () => {
        this.isSpinning = false;
        console.log("complete")
      }
    );
  }
}
