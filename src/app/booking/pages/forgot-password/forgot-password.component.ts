import { Component, OnInit } from "@angular/core";
import { NzMessageService } from "ng-zorro-antd";
import { ForgotPasswordService } from "src/app/service/forgot-password.service";

@Component({
  selector: "fmyp-forgot-password",
  templateUrl: "./forgot-password.component.html",
  styleUrls: ["./forgot-password.component.css"]
})
export class ForgotPasswordComponent implements OnInit {
  constructor(
    private message: NzMessageService,
    private forgotPasswordService: ForgotPasswordService
  ) { }

  ngOnInit() { }

  submitForm(formData) {
    console.log("formData", formData);
    this.forgotPasswordService.sendMail(formData).subscribe(
      data => {
        console.log("", data.data);
        this.message.success("Sent Mail ");
      },
      error => {
        console.log("error", error);
      }
    );
  }
}
