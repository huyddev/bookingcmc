import { Component, OnInit } from '@angular/core';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { PostService } from 'src/app/service/post.service';
import { User } from 'src/app/model/user';
import { Post } from 'src/app/model/post';
import { UploadImageService } from 'src/app/service/upload-image.service';

@Component({
  selector: 'fmyp-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {

  token: string;
  user: User = new User;
  post: Post = new Post;
  private postId: any;
  constructor(
    private urlImage: UploadImageService,
    private router: Router,
    private route: ActivatedRoute,
    private postService: PostService
  ) { }

  ngOnInit() {
    let data = localStorage.getItem('user');
    if (data) {
      this.token = JSON.parse(data).token;
      this.user = JSON.parse(data).user;
    } else {
      this.router.navigate(['/login'])
    }
    this.postId = this.route.snapshot.paramMap.get('id');
    console.log(this.postId)
    this.postService._getById(this.token, this.postId).subscribe(
      response => {
        let d: any = response;
        this.post = d.data;
        console.log(this.post)
        this.post.author.avata = this.urlImage._urlImage(this.post.author.avata);
        this.post.image = this.urlImage._urlImage(this.post.image);
        console.log('Post detail', this.post)
      },
      err=>{
        console.log(err)
      }
    )
  }

}
