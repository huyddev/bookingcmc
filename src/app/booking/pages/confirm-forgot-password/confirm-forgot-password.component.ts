import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { ForgotPasswordService } from 'src/app/service/forgot-password.service';
import { User } from 'src/app/model/user';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'fmyp-confirm-forgot-password',
  templateUrl: './confirm-forgot-password.component.html',
  styleUrls: ['./confirm-forgot-password.component.css']
})
export class ConfirmForgotPasswordComponent implements OnInit {
  passwordVisible = false;
  password: string;
  user: User = new User();
  token: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private message: NzMessageService,
    private forgotPasswordService: ForgotPasswordService,
    private router: Router

  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.token = params['token'];
      console.log(this.token);
    });
  }

  submitForm(formData) {
    const form = { newPass: formData.newPass, token: this.token }
    console.log("form", form);

    this.forgotPasswordService.setNewPassword(form).subscribe(
      data => {
        console.log("data", data);
        if (data.data) {
          if (data.data.status == "success") {
            this.message.success("Success");
            this.router.navigate(["/login"]);
          }
        } else {
          this.message.error("Link fail", { nzDuration: 10000 });
        }
      },
      err => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.router.navigate(["/login"]);
          }
        }
        this.message.error("Fail"),
          console.log("error", err);
      }
    );
  }

  ngOnInit() {
  }

}
