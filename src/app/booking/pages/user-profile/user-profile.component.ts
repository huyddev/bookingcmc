import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/service/user.service';
import { User } from 'src/app/model/user';
import { GroupService } from 'src/app/service/group.service';
import { Group } from 'src/app/model/group';
import { _url } from 'src/app/service/url';
import { NzMessageService } from 'ng-zorro-antd';
import { AdminProfileService } from 'src/app/service/admin-profile.service';
import { UserBook } from 'src/app/model/user-book';
import { UserBookService } from 'src/app/service/user-book.service';


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  change_password_Form: FormGroup;
  userStorage: any;
  userClient: User = new User;
  groups: Group[] = [];
  changepass = false;
  ckeck = false;
  token = "";
  bookinghistory: UserBook[] = [];
  list_history: any;
  new_password: any;
  currentpass: any;
  validPassword: boolean = false;
  userBody : any;

  constructor(
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private userService: UserService,
    private userBookSerivce: UserBookService,
    private groupService: GroupService,
    private message: NzMessageService,
    private profileService: AdminProfileService
  ) {

    this.change_password_Form = this.fb.group({
      current_password: ['', [Validators.required]],
      new_password: ['', [Validators.required]],
      confirm_password: ['', [this.confirmValidator]],
    })
  }
  UploadImageForm = this.fb.group({
    file: [null]
  })
  @ViewChild('fileInput') el: ElementRef;
  imageUrl: any = "";
  editFile: boolean = true;
  removeUpload: boolean = false;
  compareFn = (o1: any, o2: any) => (o1 && o2 ? o1.id === o2.id : o1 === o2);

  ngOnInit() {
    let dataLogin = JSON.parse(localStorage.getItem('user'));
    if (dataLogin) {
      this.userStorage = dataLogin.user;
      this.token = dataLogin.token;
    }

    this.userService._getUserById(this.token, this.userStorage.id).subscribe(
      data => {
        let d: any = data;
        this.userClient = d.data;
        this.userClient.imageFile = null;
        this.imageUrl = _url.domain + 'downloadFile/' + this.userClient.avata;
      },
      error => console.log("error", error),
      () => console.log("complete!")
    )

    this.userBookSerivce._getHistory(this.token).subscribe(
      datahistory => {
        let b: any = datahistory;
        this.bookinghistory = b.data;
        this.list_history = b.data;
      },
      error => console.log("error", error),
      () => console.log("complete!")
    )

    this.groupService._getAllGroup().subscribe(
      datagroup => {
        let g: any = datagroup;
        this.groups = g.data;
        console.log("group", this.groups)
      },
      (e) => console.log(e)
    )
  }

  submitForm = ($event: any, value: any) => {
    $event.preventDefault();
    for (const key in this.change_password_Form.controls) {
      this.change_password_Form.controls[key].markAsDirty();
      this.change_password_Form.controls[key].updateValueAndValidity();
    }
    console.log(value)
  }

  onclick() {
    if (this.ckeck == false) {
      this.changepass = true;
      this.ckeck = true;
    }
    else {
      this.changepass = false;
      this.ckeck = false;
    }
  }
  // change password
  change_password() {
    const formData = { password: this.new_password }
    console.log("formData", formData);

    this.profileService.setNewPassword(formData, this.token).subscribe(
      data => {
        console.log("data", data.data);
        this.message.success("success");
      },
      error => {
        this.message.error("fail"),
          console.log("error", error);
      });
  }

  confirmValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.change_password_Form.controls.new_password.value) {
      return { confirm_password: true, error: true };
    }
    return {};
  }

  checkPassword() {
    const formData = { "password": this.currentpass };
    console.log("data", formData);
    this.profileService.checkOldPassword(formData, this.token).subscribe(
      data => {
        console.log("data", data);
        if (data.message == "PASSWORD VERIFIED") {
          this.validPassword = true;
          return { required: true };

        } else {
          this.validPassword = false;
          this.message.error("Fail");
        }
      },
      error => {
        console.log("Password is wrong", error);
      }
    );
  }

  // upload Image
  changeFile(event) {
    console.log(event);
    let reader = new FileReader();
    if (event.target.files && event.target.files[0]) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      // When file uploads set it to file formcontrol
      reader.onload = () => {
        this.imageUrl = reader.result;
        this.userClient.imageFile = file;
        this.editFile = false;
        this.removeUpload = true;
      }
      // ChangeDetectorRef since file is loading outside the zone
      this.cd.markForCheck();
    }
  }
  // removeUploadedFile() {
  //   let newFileList = Array.from(this.el.nativeElement.files);
  //   this.imageUrl = this.userClient.avata;
  //   this.editFile = true;
  //   this.removeUpload = false;
  //   this.UploadImageForm.patchValue({
  //     file: [null]
  //   });
  // }
  updateUser() {
    let body = {
      fullname: this.userClient.fullname,
      groupId: this.userClient.group.id,
      phone: this.userClient.phone,
      imageFile: this.userClient.imageFile
    }
    console.log("User client", body);
    this.userService._updateUserAvatar(this.token, body).subscribe((data) => {
      this.message.success('Save Success !!!');
      console.log(data)
    }, (e) => {
      console.log(e),
        this.message.error("Update Avatar Failure!!!")
    })
  }

  uploadinfo() {
    this.userService._updateUserClient(this.token, this.userClient).subscribe(
      data => {
        console.log(data)
        this.message.success("Update Success!!!")
        // this._update_avatar()
      }
    ),
      (e) => {
        console.log(e)
        this.message.error("Update false!!!")
      }
    console.log('updateinfo', this.userClient)
  }

}
