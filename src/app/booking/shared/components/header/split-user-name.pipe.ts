import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'splitUserName'
})
export class SplitUserNamePipe implements PipeTransform {

  transform(value: string): string {
    let src = value.split(" ");
    return src[src.length-1];
  }
}
