import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/service/user.service';
import { _url } from 'src/app/service/url'; 
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  userStorage : any;
  userClient : User;
  avatarUrl : string;
  token="";
  isRole = false;
  constructor(
    private userService : UserService,
    private router: Router
  ) { }

  ngOnInit() {
    let data = JSON.parse(localStorage.getItem('user'));
    if(data){
      this.userStorage =data.user;
      this.token = data.token;
      console.log("aa", this.userStorage)
    }
    this.userService._getUserById(this.token, this.userStorage.id).subscribe(
      dataUser => {
        let d :any =dataUser;
        this.userClient = d.data;
        this.avatarUrl =_url.domain+'downloadFile/'+ this.userClient.avata;
      },
      err => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.router.navigate(["/login"]);
          }
        }
      }
    )
    this.checkAdmin();
  }
  change(value: boolean): void {
    // console.log(value);
  }
  logout(){
    localStorage.removeItem("user");
  }
  checkAdmin(){
    if(this.userStorage.listRole.filter(e => e.name === 'ADMIN').length > 0){
      this.isRole=true;
    }
    else{
      this.isRole = false;
    }
  }
}
