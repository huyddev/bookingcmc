import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { _url } from './url';
import * as moment from 'moment'
import { Dashboard } from '../model/dashboard';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    private http: HttpClient
  ) { }

  _getDashboard(token: string, dashboard: Dashboard): Observable<HttpResponse<any>> {
    
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.post<HttpResponse<any>>(_url.domain + 'admin/dashboard-get-all', dashboard, httpOption);
  }
}
