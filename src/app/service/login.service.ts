import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { httpOptions} from "../booking/shared/utils/environment";
import { _url } from './url';

@Injectable({
  providedIn: "root"
})
export class LoginService {
  constructor(private httpClient: HttpClient) {}
  getUserLogged(formData): Observable<any> {
    return this.httpClient.post<any>(_url.domain+'authenticate', formData, httpOptions);
  }
}
