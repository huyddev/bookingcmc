import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { _url } from './url';
import { httpOptions } from '../booking/shared/utils/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ResetPasswordService {
  constructor(private httpClient: HttpClient) { }
  resetPassword(formData): Observable<any> {
    return this.httpClient.post<any>(_url.domain + 'reset-password', formData, httpOptions);
  }
}
