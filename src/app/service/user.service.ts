import { Injectable } from '@angular/core';
import { _url } from './url';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient) { }

  _getAllUser(token: string): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.get<HttpResponse<any>>(_url.domain + 'users', httpOption);
  }

  _getUserById(token: string, id: number): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      }),
      params: new HttpParams().append("id", id.toString())
    }
    return this.http.get<HttpResponse<any>>(_url.domain + 'user', httpOption);
  }
  _updateUserClient(token: string, user: User): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.put<HttpResponse<any>>(_url.domain + 'user', user, httpOption)
  }
  _updateUserAvatar(token: string, user: any): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Token ' + token
      }),

    }
    console.log("User update IMG :", user)
    return this.http.put<HttpResponse<any>>(_url.domain + 'user/avatar', user, httpOption);
  }

  /* api admin */

  _updateUser(token: string, user: User): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.put<HttpResponse<any>>(_url.domain + 'user', user, httpOption);
  }

  _createUser(token: string, user: User): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.post<HttpResponse<any>>(_url.domain + 'admin/user', user, httpOption);
  }

  _deleteUser(token: string, id: number): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.delete<HttpResponse<any>>(_url.domain + 'admin/user?id=' + id, httpOption);
  }

  _checkMail(token: string, user: User): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.put<HttpResponse<any>>(_url.domain + 'mail/check', user, httpOption);
  }

}
