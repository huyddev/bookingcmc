import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpResponse, HttpClient } from '@angular/common/http';
import { _url } from './url';

@Injectable({
  providedIn: 'root'
})
export class UploadImageService {

  constructor(
    private http: HttpClient
  ) { }

  _urlImage(fileName): string {
    return _url.domain + 'downloadFile/' + fileName;
  }
}
