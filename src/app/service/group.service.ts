import { Injectable } from "@angular/core";
import { _url } from "./url";
import { HttpClient, HttpResponse, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Group } from "../model/group";

@Injectable({
  providedIn: "root"
})
export class GroupService {
  constructor(private http: HttpClient) {}

  _getAllGroup(): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    };
    return this.http.get<HttpResponse<any>>(_url.domain + "groups", httpOption);
  }

  _getGroupById(token: string, id: number): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Token " + token
      })
    };
    return this.http.get<HttpResponse<any>>(
      _url.domain + "group?id=" + id,
      httpOption
    );
  }

  /* API ADMIN */

  _createGroup(token: string, group: Group): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Token " + token
      })
    };
    return this.http.post<HttpResponse<any>>(
      _url.domain + "admin/groups",
      group,
      httpOption
    );
  }

  _updateGroup(token: string, group: Group): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Token " + token
      })
    };
    return this.http.put<HttpResponse<any>>(
      _url.domain + "admin/groups",
      group,
      httpOption
    );
  }
  _deleteGroup(token: string, id: number): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Token " + token
      })
    };
    return this.http.delete<HttpResponse<any>>(
      _url.domain + "admin/groups?id=" + id,
      httpOption
    );
  }
}
