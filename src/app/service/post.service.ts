import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { _url } from './url';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(
    private http: HttpClient
  ) { }

  _getAll(token): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': "application/json",
        'Authorization': "Token " + token
      })
    }
<<<<<<< HEAD
    return this.http.get<HttpResponse<any>>(_url.domain + 'admin/posts', httpOption);
=======
    return this.http.get<HttpResponse<any>>(_url.domain + 'posts', httpOption);
>>>>>>> b4047609bd1fe755c05c712c6937cfabf99326c6
  }

  _getIsActive(token): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': "application/json",
        'Authorization': "Token " + token
      })
    }
    return this.http.get<HttpResponse<any>>(_url.domain + 'post/list', httpOption);
  }

  _getById(token, id): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Conten-Type': "application/json",
        'Authorization': "Token " + token
      })
    }
    return this.http.get<HttpResponse<any>>(_url.domain + 'post?id=' + id, httpOption)
  }

  _update(token, post: any): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': "application/json",
        'Authorization': "Token " + token
      })
    }
    return this.http.put<HttpResponse<any>>(_url.domain + 'admin/post', post, httpOption)
  }
  _delete(token, id): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': "application/json",
        'Authorization': "Token " + token
      })
    }
    return this.http.delete<HttpResponse<any>>(_url.domain + 'admin/post?id=' + id, httpOption)
  }
  _create(token, body): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': "application/json",
        'Authorization': "Token " + token
      })
    }
    return this.http.post<HttpResponse<any>>(_url.domain + 'admin/post', body, httpOption);
  }

  _
}
