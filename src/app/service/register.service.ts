import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { httpOptions } from "../booking/shared/utils/environment";
import { _url } from './url';

@Injectable({
  providedIn: "root"
})
export class RegisterService {
  constructor(private httpClient: HttpClient) { }
  getUserRegistered(formData): Observable<any> {
    return this.httpClient.post<any>(_url.domain + 'register', formData, httpOptions);
  }

  confirm(token): Observable<any> {
    return this.httpClient.get<any>(_url.domain + 'confirm-register?token=' + token, httpOptions);
  }
}
