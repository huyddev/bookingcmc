import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { _url } from "./url";
import { httpOptions } from "../booking/shared/utils/environment";

@Injectable({
  providedIn: "root"
})
export class ForgotPasswordService {
  sendMail(formData): Observable<any> {
    return this.httpClient.post<any>(
      _url.domain + "forgot-password",
      formData,
      httpOptions
    );
  }

  setNewPassword(formData): Observable<any> {
    return this.httpClient.post<any>(
      _url.domain + "confirm-forgot-password",
      formData,
      httpOptions
    );
  }

  constructor(private httpClient: HttpClient) { }
}
