import { Injectable } from '@angular/core';
import { _url } from './url';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse, HttpParams } from '@angular/common/http';
import { UserBook } from '../model/user-book';
import { SelectDate } from '../model/select-date';

@Injectable({
  providedIn: 'root'
})
export class UserBookService {

  constructor(
    private http: HttpClient
  ) { }

  _getAll(token: string): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.get<HttpResponse<any>>(_url.domain + 'list-user-book', httpOption);
  }

  _getById(token: string, id: number): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.get<HttpResponse<any>>(_url.domain + 'user-book?id=' + id, httpOption);
  }

  _getHistory(token: string): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      }),
    }
    return this.http.get<HttpResponse<any>>(_url.domain + 'user-book-history', httpOption);
  }



  _create(token: string, userBook: UserBook): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.post<HttpResponse<any>>(_url.domain + 'user-book', userBook, httpOption);
  }

  _update(token: string, userBook: UserBook): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.put<HttpResponse<any>>(_url.domain + 'user-book', userBook, httpOption);
  }

  _delete(token: string, id: number): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.delete<HttpResponse<any>>(_url.domain + 'user-book?id=' + id, httpOption);
  }

  _getByDay(token: string, date: any): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.post<HttpResponse<any>>(_url.domain + 'user-book-by-date', date, httpOption);
  }

  _getByWeek(token: string, date: any): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.post<HttpResponse<any>>(_url.domain + 'user-book-by-week', date, httpOption);
  }

  _getByMonth(token: string, date: any): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.post<HttpResponse<any>>(_url.domain + 'user-book-by-month', date, httpOption);
  }

  _updateStatus(token: string, id: string, status: string): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.get<HttpResponse<any>>(_url.domain + 'user-book-update-status?id=' + id + "&status=" + status, httpOption);
  }

  //ADMIN
  _getByDayAdmin(token: string, date: SelectDate): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.post<HttpResponse<any>>(_url.domain + 'admin/user-book-by-date', date, httpOption);
  }

  _getByWeekAdmin(token: string, date: SelectDate): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.post<HttpResponse<any>>(_url.domain + 'admin/user-book-by-week', date, httpOption);
  }

  _getByMonthAdmin(token: string, date: SelectDate): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.post<HttpResponse<any>>(_url.domain + 'admin/user-book-by-month', date, httpOption);
  }

  _updateStatusAdmin(token: string, id: string, status: string): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.get<HttpResponse<any>>(_url.domain + 'admin/user-book-update-status?id=' + id + "&status=" + status, httpOption);
  }

  _getBookingSameTime(token: string, userBook: UserBook): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.post<HttpResponse<any>>(_url.domain + 'user-book-by-time', userBook, httpOption);
  }
}
