import { Injectable } from "@angular/core";
import { _url } from "./url";
import { HttpResponse, HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Role } from "../model/role";

@Injectable({
  providedIn: "root"
})
export class RoleService {
  constructor(private http: HttpClient) {}

  _getAllRole(token: string): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Token " + token
      })
    };
    return this.http.get<HttpResponse<any>>(
      _url.domain + "admin/roles",
      httpOption
    );
  }

  _getRoleById(token: string, id: number): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Token " + token
      })
    };
    return this.http.get<HttpResponse<any>>(
      _url.domain + "admin/roles?id" + id,
      httpOption
    );
  }

  /* API ADMIN */

  _createRole(token: string, role: Role): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Token " + token
      })
    };
    return this.http.post<HttpResponse<any>>(
      _url.domain + "admin/roles",
      role,
      httpOption
    );
  }

  _updateRole(token: string, role: Role): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Token " + token
      })
    };
    return this.http.put<HttpResponse<any>>(
      _url.domain + "admin/roles",
      role,
      httpOption
    );
  }

  _deleteRole(token: string, id: number): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Token " + token
      }),
      body: { id: id }
    };
    return this.http.delete<HttpResponse<any>>(
      _url.domain + "admin/roles",
      httpOption
    );
  }
}
