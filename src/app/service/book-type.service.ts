import { Injectable } from '@angular/core';
import { _url } from './url';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BookType } from '../model/book-type';

@Injectable({
  providedIn: 'root'
})
export class BookTypeService {

  constructor(
    private http: HttpClient
  ) { }

  _getAll(token: string): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.get<HttpResponse<any>>(_url.domain + 'list-book-type', httpOption);
  }

  _getAllActive(token: string): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.get<HttpResponse<any>>(_url.domain + 'list-book-type-active', httpOption);
  }


  _getById(token: string, id: number): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.get<HttpResponse<any>>(_url.domain + 'book-type?id=' + id, httpOption);
  }

  /* API ADMIN */

  _create(token: string, bookType: BookType): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.post<HttpResponse<any>>(_url.domain + 'admin/book-type', bookType, httpOption);
  }

  _update(token: string, bookType: BookType): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.put<HttpResponse<any>>(_url.domain + 'admin/book-type', bookType, httpOption);
  }

  _delete(token: string, id: number): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.delete<HttpResponse<any>>(_url.domain + 'admin/book-type?id=' + id, httpOption);
  }


}
