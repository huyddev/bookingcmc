import { Injectable } from '@angular/core';
import { _url } from './url';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BookItem } from '../model/book-item';

@Injectable({
  providedIn: 'root'
})
export class BookItemService {

  constructor(
    private http: HttpClient) { }

  _getAll(token: string): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.get<HttpResponse<any>>(_url.domain + 'list-book-item', httpOption);
  }

  _getById(token: string, id: number): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.get<HttpResponse<any>>(_url.domain + 'book-item?id=' + id, httpOption);
  }

  /* API ADMIN */

  _create(token: string, bookItem: BookItem): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.post<HttpResponse<any>>(_url.domain + 'admin/book-item', bookItem, httpOption);
  }

  _update(token: string, bookItem: BookItem): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.put<HttpResponse<any>>(_url.domain + 'admin/book-item', bookItem, httpOption);
  }

  _delete(token: string, id: number): Observable<HttpResponse<any>> {
    let httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + token
      })
    }
    return this.http.delete<HttpResponse<any>>(_url.domain + 'admin/book-item?id=' + id, httpOption);
  }
}
