import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { httpOptions } from "src/app/booking/shared/utils/environment";
import { User } from "../model/user";
import { _url } from "./url";

@Injectable({
  providedIn: "root"
})
export class AdminProfileService {
  constructor(private httpClient: HttpClient) { }

  getUser(): User {
    return JSON.parse(localStorage.getItem("user")).user;
  }

  changeProfile(formData, token: string): Observable<any> {
    let httpOption = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Token " + token
      })
    };
    return this.httpClient.put<any>(
      _url.domain + "user",
      formData,
      httpOption
    );
  }

  setNewPassword(formData, token: string): Observable<any> {
    let httpOption = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Token " + token
      })
    };
    return this.httpClient.put<any>(
      _url.domain + "user/change-password",
      formData,
      httpOption
    );
  }

  checkOldPassword(formData, token: string): Observable<any> {
    let httpOption = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "Token " + token
      })
    };
    return this.httpClient.put<any>(
      _url.domain + "user/check-password",
      formData,
      httpOption
    );
  }
}
