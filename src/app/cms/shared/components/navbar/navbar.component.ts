import { Component, OnInit } from "@angular/core";
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/service/user.service';
import { Router } from '@angular/router';
import { role_ID, _url } from 'src/app/service/url';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css"]
})
export class NavbarComponent implements OnInit {
  user: User = new User();
  token = JSON.parse(localStorage.getItem("user")).token;
  userId = JSON.parse(localStorage.getItem("user")).user.id;

  constructor(
    private userService: UserService,
    private router: Router) { }

  ngOnInit() {
    //check ADMIN or MANAGER
    this.getUser();
    let isRole = false;
    if (this.user.listRole !== undefined) {
      this.user.listRole.forEach(e => {
        if (e.id == role_ID.MEMBER) {
          isRole = true;
          return;
        }
      });
    }

    if (isRole || !this.user) {
      this.router.navigate(["/login"]);
    }
  }
  getUser() {
    this.userService._getUserById(this.token, this.userId).subscribe(
      data => {
        const d: any = data;
        this.user = d.data;
        this.user.avata = _url.domain + 'downloadFile/' + this.user.avata;
        console.log("User: ", this.user)
      }, 
      err => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.router.navigate(["/login"]);
          }
        }
      }
    );
  }

  signOut() {
    localStorage.removeItem("user");
  }
}
