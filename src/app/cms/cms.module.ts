import { NgModule } from "@angular/core";
import { CommonModule, registerLocaleData } from "@angular/common";

import { CmsRoutingModule } from "./cms-routing.module";
import { CmsComponent } from "./cms.component";
import { DashboardComponent } from "./pages/dashboard/dashboard.component";
import { SidebarComponent } from "./shared/components/sidebar/sidebar.component";
import { UsersComponent } from "./pages/users/users.component";
import { RoleComponent } from "./pages/role/role.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgZorroAntdModule, NZ_MESSAGE_CONFIG, NZ_I18N, NZ_ICONS, en_US } from "ng-zorro-antd";
import { AdminProfileComponent } from "./pages/admin-profile/admin-profile.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NavbarComponent } from "./shared/components/navbar/navbar.component";
import { FooterComponent } from "./shared/components/footer/footer.component";
import { BookingRoomsPipe } from '../pipe/booking-rooms.pipe';
import { BookingRooms2Pipe } from '../pipe/booking-rooms-2.pipe';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { BookItemComponent } from './pages/book-item/book-item.component';
import { UserBookComponent } from './pages/user-book/user-book.component';
import { BookTypeComponent } from './pages/book-type/book-type.component';
import * as AllIcons from '@ant-design/icons-angular/icons';
import { IconDefinition } from '@ant-design/icons-angular';
import en from '@angular/common/locales/en';
import * as moment from 'moment';
import { NgxPaginationModule } from 'ngx-pagination';
import { LengthTextPipe } from '../pipe/length-text.pipe';
import { PostComponent } from './pages/post/post.component';
import { PostAddComponent } from './pages/post-add/post-add.component';
import { PostEditComponent } from './pages/post-edit/post-edit.component';
import { CKEditorModule } from 'ng2-ckeditor';

registerLocaleData(en);
const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};
const icons: IconDefinition[] = Object.keys(antDesignIcons).map(key => antDesignIcons[key])

@NgModule({
  declarations: [
    CmsComponent,
    DashboardComponent,
    SidebarComponent,
    UsersComponent,
    RoleComponent,
    AdminProfileComponent,
    NavbarComponent,
    FooterComponent,
    BookingRoomsPipe,
    BookingRooms2Pipe,
    ChangePasswordComponent,
    BookItemComponent,
    UserBookComponent,
    BookTypeComponent,
    LengthTextPipe,
    PostComponent,
    PostAddComponent,
    PostEditComponent
  ],
  imports: [
    CommonModule,
    CmsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgZorroAntdModule,
    NgbModule,
    FormsModule,
    NgxPaginationModule,
    CKEditorModule
  ],
  providers: [
    { provide: NZ_MESSAGE_CONFIG, useValue: { nzTop: 64 } },
    { provide: NZ_I18N, useValue: en_US },
    { provide: NZ_ICONS, useValue: icons }
  ]
})
export class CmsModule { }
