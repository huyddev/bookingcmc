import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CmsComponent } from "./cms.component";
import { DashboardComponent } from "./pages/dashboard/dashboard.component";
import { UsersComponent } from "./pages/users/users.component";
import { RoleComponent } from "./pages/role/role.component";
import { AdminProfileComponent } from "./pages/admin-profile/admin-profile.component";
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { UserBookComponent } from './pages/user-book/user-book.component';
import { BookItemComponent } from './pages/book-item/book-item.component';
import { BookTypeComponent } from './pages/book-type/book-type.component';
import { PostComponent } from './pages/post/post.component';
import { PostAddComponent } from './pages/post-add/post-add.component';
import { PostEditComponent } from './pages/post-edit/post-edit.component';

const routes: Routes = [
  {
    path: "admin",
    component: CmsComponent,
    children: [
      {
        path: "dashboard",
        component: DashboardComponent
      },
      {
        path: "users",
        component: UsersComponent
      },
      {
        path: "book-item",
        component: BookItemComponent
      },
      {
        path: "roles",
        component: RoleComponent
      },
      {
        path: "admin-profile",
        component: AdminProfileComponent
      },
      {
        path: "change-password",
        component: ChangePasswordComponent
      },
      {
        path: "user-book",
        component: UserBookComponent
      },
      {
        path: "book-type",
        component: BookTypeComponent
      },
      {
        path: "post",
        component: PostComponent
      },
      {
        path: "post-add",
        component: PostAddComponent
      },
      {
        path: "post-edit",
        component: PostEditComponent
      }
    ]
  },
  {
    path: "admin-dashboard",
    component: DashboardComponent
  },
  {
    path: "admin-users",
    component: UsersComponent
  },
  {
    path: "admin-rooms",
    component: BookItemComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CmsRoutingModule { }
