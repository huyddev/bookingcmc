import { Component, OnInit, PipeTransform } from "@angular/core";
import { UserService } from "src/app/service/user.service";
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { GroupService } from "src/app/service/group.service";
import { RoleService } from "src/app/service/role.service";
import { HttpResponse } from "@angular/common/http";
import { User } from "src/app/model/user";
import { NgbModal, ModalDismissReasons, NgbModalConfig } from "@ng-bootstrap/ng-bootstrap";
import { NzModalRef, NzModalService, NzMessageService } from "ng-zorro-antd";
import { Subscription } from "rxjs/internal/Subscription";
import { Group } from "src/app/model/group";
import { Role } from "src/app/model/role";
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { role_ID } from 'src/app/service/url';

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.css"],
  providers: [DecimalPipe]
})
export class UsersComponent implements OnInit {
  confirmModal: NzModalRef;
  role_filter: string = "Role";

  status_member: boolean = true;
  status_admin: boolean = false;
  status_manager: boolean = false;
  message: string = "";
  status_mail: boolean = false;
  status_password: boolean;
  status_role: boolean;
  status_phone: boolean;
  repeatPassword: string;
  token: string = "";
  status_filter: string = "Status";
  group_user: string = "Group";
  group_filter: string = "Group";
  fieldSort: string = "";
  listUsers: User[] = [];
  listUsers_copy: User[] = [];
  listGroups: Group[] = [];
  listRoles: Role[] = [];
  sub: Subscription;
  user: User = new User();
  group: Group = new Group();
  role: Role;

  admin: User = new User();

  page = 1;
  pageSize = 8;
  collectionSize;

  get users(): User[] {
    return this.listUsers
      .map((user, i) => ({ id: i + 1, ...user }))
      .slice(
        (this.page - 1) * this.pageSize,
        (this.page - 1) * this.pageSize + this.pageSize
      );
  }

  search(text: string, pipe: PipeTransform): User[] {
    return this.listUsers.filter(user => {
      const term = text.toLowerCase();
      return user.mail.toLowerCase().includes(term)
        || pipe.transform(user.id).includes(term)
        || pipe.transform(user.phone).includes(term);
    });
  }

  constructor(
    private userService: UserService,
    private groupService: GroupService,
    private modalService: NgbModal,
    private config: NgbModalConfig,
    private roleService: RoleService,
    private pipe: DecimalPipe,
    private router: Router,
    private nzModalService: NzModalService,
    private nzMessageService: NzMessageService
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
    // this.countries$ = this.filter.valueChanges.pipe(
    //   startWith(''),
    //   map(text => this.search(text, pipe))
    // );
  }

  ngOnInit() {
    this.token = JSON.parse(localStorage.getItem("user")).token;
    this.admin = JSON.parse(localStorage.getItem("user")).user;
    //check ADMIN or MANAGER
    let isRole = false;
    if (this.admin.listRole !== undefined) {
      this.admin.listRole.forEach(e => {
        if (e.id == role_ID.ADMIN || e.id == role_ID.MANAGER) {
          isRole = true;
        }
      });
    }

    if (!isRole || !this.user) {
      this.router.navigate(["/login"]);
    }

    this.getAllUsers();
    this.getAllRoles();
    this.getAllGroups();
  }

  getAllUsers() {
    this.userService._getAllUser(this.token).subscribe(
      (res: HttpResponse<any>) => {
        let d: any = res;
        this.listUsers = d.data;
        this.listUsers_copy = d.data;
        this.collectionSize = this.listUsers.length;
        console.log("List Users: ", this.listUsers);
      },
      error => {
        console.log("Error", error);
      }
    );
  }

  getAllRoles() {
    this.roleService._getAllRole(this.token).subscribe(
      (res: HttpResponse<any>) => {
        let d: any = res;
        this.listRoles = d.data;
        console.log("list Groups: ", this.listRoles);
      },
      error => {
        console.log("Error", error);
      }
    );
  }

  getAllGroups() {
    this.groupService._getAllGroup().subscribe(
      (res: HttpResponse<any>) => {
        let d: any = res;
        this.listGroups = d.data;
        this.group_user = this.listGroups[0].name;
        console.log("list Groups: ", this.listGroups);
      },
      error => {
        console.log("Error", error);
      }
    );
  }

  onSubmit(formAddUser) { }

  filterByAll1() {
    let data = this.listUsers_copy.filter(b => {
      return (this.status_filter != 'Status' ? b.status == this.status_filter : true) && (this.group_filter != 'Group' ? b.group.name == this.group_filter : true)
    })
    console.log("condition", this.group_filter, this.status_filter)
    console.log("data, listUsers_copy", data, this.listUsers_copy)
    this.listUsers = data;
    this.collectionSize = this.listUsers.length;
  }

  openFormAdd(content) {
    this.modalService.open(content);
    this.resetForm();
  }

  addUser(formAddUser) {
    let values = formAddUser.value;
    console.log(values);
    if (values.Admin) {
      let role_admin: Role = new Role();
      role_admin.id = 1;
      this.user.listRole.push(role_admin);
    }
    if (values.Member) {
      let role_member: Role = new Role();
      role_member.id = 2;
      this.user.listRole.push(role_member);
    }
    if (values.Manager) {
      let role_manager: Role = new Role();
      role_manager.id = 3;
      this.user.listRole.push(role_manager);
    }
    for (let i = 0; i < this.listGroups.length; i++) {
      if (this.listGroups[i].name == values.groupName) {
        this.group.id = this.listGroups[i].id;
        this.user.group = this.group;
        break;
      }
    }
    console.log("User Add: ", this.user);
    this.sub = this.userService._createUser(this.token, this.user).subscribe((res: HttpResponse<any>) => {
      let d: any = res;
      d.data.id = this.listUsers[this.listUsers.length - 1].id + 1;
      this.listUsers = [...this.listUsers, d.data];
      console.log("update list user add:", this.listUsers);
      this.collectionSize = this.listUsers.length;
      this.nzMessageService.success("Add successful user!");
    }
    );
    // this.resetForm();
  }

  resetForm() {
    let user_reset: User = new User();
    this.user = user_reset;
    (document.getElementById("form-add") as HTMLFormElement).reset();
    this.status_member = true;
    this.status_admin = false;
    this.status_manager = false;
    this.group_user = this.listGroups[0].name;
    this.status_mail = false;
    this.user.listRole.splice(0);
  }

  openFormEdit(content, user) {
    this.modalService.open(content);
    this.resetForm();
    this.user.id = user.id;
    this.user.mail = user.mail;
    this.user.fullname = user.fullname;
    this.user.phone = user.phone;
    this.user.mail = user.mail;
    this.user.createTime = user.createTime;
    this.user.avata = user.avata;

    let d = 0;
    if (user.listRole.length == 0) {
      this.status_member = false;
    }
    user.listRole.forEach(element => {
      if (element.id == 1) {
        this.status_admin = true;
      };
      if (element.id != 2 && d == 0) {
        this.status_member = false;
      } else if (element.id == 2) {
        console.log("role member ok");
        this.status_member = true;
        d++;
      }
      if (element.id == 3) {
        this.status_manager = true;
      };
    });
    if (user.status == "PENDING") {
      (document.getElementById("defaultInline1") as HTMLInputElement).checked = true;
    } else if (user.status == "ACTIVE") {
      (document.getElementById("defaultInline2") as HTMLInputElement).checked = true;
      (document.getElementById("defaultInline1") as HTMLInputElement).disabled = true;
    } else if (user.status == "INACTIVE") {
      (document.getElementById("defaultInline3") as HTMLInputElement).checked = true;
      (document.getElementById("defaultInline1") as HTMLInputElement).disabled = true;
    }
    this.group_user = user.group.name;
    this.status_mail = true;
    this.status_phone = true;
    this.status_password = true;
    // this.modalService.open(content);
  }

  editUser(formAddUser) {
    // console.log(formAddUser.value);
    let values = formAddUser.value;
    console.log(values);
    if (values.Admin) {
      let role_admin: Role = new Role();
      role_admin.id = 1;
      this.user.listRole.push(role_admin);
    }
    if (values.Member) {
      let role_member: Role = new Role();
      role_member.id = 2;
      this.user.listRole.push(role_member);
    }
    if (values.Manager) {
      let role_manager: Role = new Role();
      role_manager.id = 3;
      this.user.listRole.push(role_manager);
    }
    for (let i = 0; i < this.listGroups.length; i++) {
      if (this.listGroups[i].name == values.groupName) {
        this.group.id = this.listGroups[i].id;
        this.user.group = this.group;
        break;
      }
    }
    let listStatus = document.getElementsByName("status");
    for (let i = 0; i < listStatus.length; i++) {
      if ((listStatus[i] as HTMLInputElement).checked == true) {
        this.user.status = (listStatus[i] as HTMLInputElement).value;
        break;
      }
    }
    console.log("User Edit: ", this.user);
    this.sub = this.userService._updateUser(this.token, this.user).subscribe((res: HttpResponse<any>) => {
      let d: any = res;
      console.log("res: ", d);
      let user_find = this.listUsers.find(e => e.id == this.user.id);
      let index = this.listUsers.indexOf(user_find);
      this.listUsers[index] = d.data;
      // this.collectionSize = this.listUsers.length;
      this.nzMessageService.success("Edit successful user!");
    }
    );
    // this.resetForm();
  }

  deleteUser(user) {
    this.nzModalService.confirm({
      nzTitle: "Are you sure delete this user?",
      nzOkText: "Yes",
      nzOkType: "danger",
      nzOnOk: () => {
        this.userService._deleteUser(this.token, user.id).subscribe(
          (res: HttpResponse<any>) => {
            let d: any = res;
            if (d.data.delete == "OK") {
              let user_find = this.listUsers.find(e => e.id == user.id);
              let index = this.listUsers.indexOf(user_find);
              console.log(index);
              this.listUsers[index].status = "INACTIVE";
              // this.collectionSize = this.listUsers.length;
              this.nzMessageService.success("Delete successful user!");
            }
          },
          error => {
            console.log("Error", error);
          }
        );
      },
      nzCancelText: "Cancel",
    });
  }

  resetSortEvent() {
    if (document.getElementById("sort-mail").className != "fa fa-sort") {
      document.getElementById("sort-mail").className = "fa fa-sort";
    }
    if (document.getElementById("sort-fullname").className != "fa fa-sort") {
      document.getElementById("sort-fullname").className = "fa fa-sort";
    }
  }

  sort(fieldSort) {
    let d = 0;
    if (this.fieldSort != fieldSort) {
      this.resetSortEvent();
      this.fieldSort = fieldSort;
    }
    if (document.getElementById(fieldSort).className == "fa fa-sort" && d == 0) {
      d++;
      document.getElementById(fieldSort).className = "fa fa-sort-asc";
      this.listUsers.sort(function (a, b) {
        if (fieldSort == "sort-mail") {
          return a.mail.localeCompare(b.mail);
        }
        if (fieldSort == "sort-fullname") {
          return a.fullname.localeCompare(b.fullname);
        }
      });
    }

    if (document.getElementById(fieldSort).className == "fa fa-sort-asc" && d == 0) {
      d++;
      document.getElementById(fieldSort).className = "fa fa-sort-desc";
      this.listUsers.sort(function (a, b) {
        if (fieldSort == "sort-mail") {
          return b.mail.localeCompare(a.mail);
        }
        if (fieldSort == "sort-fullname") {
          return b.fullname.localeCompare(a.fullname);
        }
      });
    }

    if (document.getElementById(fieldSort).className == "fa fa-sort-desc" && d == 0) {
      d++;
      document.getElementById(fieldSort).className = "fa fa-sort";
      this.getAllUsers();
    }
  }

  resetTable() {
    this.resetSortEvent();
    this.status_filter = "Status";
    this.group_filter = "Group";
    this.listUsers = this.listUsers_copy;
  }

  validateRequired(value1, value2) {
    document.getElementById(value1).className = "form-control is-invalid";
    document.getElementById(value2).innerHTML = "Không bỏ trống trường này ";
  }

  validate(value1, value2, value3) {
    if (value1 == "mail") {
      if (this.user.mail == undefined || this.user.mail == "") {
        this.validateRequired(value1, value2);
      } else {
        this.validateEmail(value1, value2, value3);
      }
    } else if (value1 == "password") {
      console.log("pass:", this.user.password);
      if (this.user.password == undefined || this.user.password == "") {
        this.validateRequired(value1, value2);
      } else {
        document.getElementById(value1).className = "form-control";
        this.validateRepeatPassword("repeat-password", "alert-repeat-password", value3);
      }
    } else if (value1 == "repeat-password") {
      console.log(value3);
      if (this.repeatPassword == undefined || this.repeatPassword == "") {
        this.validateRequired(value1, value2);
      } else {
        this.validateRepeatPassword(value1, value2, value3);
      }
    } else if (value1 == "fullname") {
      if (this.user.fullname == undefined || this.user.fullname == "") {
        this.validateRequired(value1, value2);
      } else {
        document.getElementById(value1).className = "form-control is-valid";
      }
    } else if (value1 == "phone") {
      if (this.user.phone == undefined || this.user.phone == "") {
        this.validateRequired(value1, value2);
      } else {
        this.validatePhone(value1, value2, value3);
      }
    }
  }

  validateEmail(value1, value2, value3) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/;
    this.userService._checkMail(this.token, this.user).subscribe(
      (res: HttpResponse<any>) => {
        let d: any = res;
        this.message = d.message;
        console.log(this.message);
        this.status_mail = (this.message == "YOU CAN USE THIS MAIL");
        console.log(this.status_mail);
        if (this.user.mail != undefined) {
          if (re.test(this.user.mail.toLowerCase()) == true) {
            if (!this.status_mail) {
              document.getElementById(value1).className = "form-control is-invalid";
              document.getElementById(value2).innerHTML = "Email này đã có tài khoản";
              (document.getElementById("accept-add") as HTMLButtonElement).disabled = true;
            } else {
              document.getElementById(value1).className = "form-control is-valid";
              if (!value3 && this.status_password && !this.status_role && this.status_phone) {
                (document.getElementById("accept-add") as HTMLButtonElement).disabled = false;
              }
            }
          }
          else {
            document.getElementById(value1).className = "form-control is-invalid";
            document.getElementById(value2).innerHTML = "Vui lòng nhập email hợp lệ";
          }
        }
      });
  }

  validateRepeatPassword(value1, value2, value3) {
    this.status_password = (this.repeatPassword == this.user.password);
    if (!this.status_password) {
      document.getElementById(value1).className = "form-control is-invalid";
      document.getElementById(value2).innerHTML = "Password không khớp nhau";
      (document.getElementById("accept-add") as HTMLButtonElement).disabled = true;
    } else {
      document.getElementById(value1).className = "form-control is-valid";
      document.getElementById("password").className = "form-control is-valid";
      if (!value3 && !this.status_role && this.status_phone && this.status_mail) {
        (document.getElementById("accept-add") as HTMLButtonElement).disabled = false;
      }
    }
  }

  validateRole(value) {
    this.status_role = (!this.status_member && !this.status_admin && !this.status_manager);
    if (this.status_role) {
      document.getElementById("alert-role").innerHTML = "Vui lòng chọn role của user";
      (document.getElementById("accept-add") as HTMLButtonElement).disabled = true;
    } else {
      document.getElementById("alert-role").innerHTML = "";
      if (!value && this.status_phone && this.status_password && this.status_mail) {
        (document.getElementById("accept-add") as HTMLButtonElement).disabled = false;
      }
    }
  }

  validatePhone(value1, value2, value3) {
    let re = /^\d{8,11}$/;
    this.status_phone = re.test(this.user.phone);
    if (!this.status_phone) {
      document.getElementById(value1).className = "form-control is-invalid";
      document.getElementById(value2).innerHTML = "Số điện thoại có từ 8 đến 11 chữ số";
      (document.getElementById("accept-add") as HTMLButtonElement).disabled = true;
    } else {
      document.getElementById(value1).className = "form-control is-valid";
      if (!value3 && !this.status_role && this.status_password && this.status_mail) {
        console.log("vao day roi!");
        (document.getElementById("accept-add") as HTMLButtonElement).disabled = false;
      }
    }
  }

}
