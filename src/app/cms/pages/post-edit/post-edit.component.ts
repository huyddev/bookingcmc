import { Component, OnInit, ViewChild } from '@angular/core';
import { Post } from 'src/app/model/post';
import { PostService } from 'src/app/service/post.service';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user';
import { role_ID } from 'src/app/service/url';
import { HttpResponse } from '@angular/common/http';
import { NzMessageService, NzModalService, NzModalRef } from 'ng-zorro-antd';

@Component({
  selector: 'fmyp-post-edit',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.css']
})
export class PostEditComponent implements OnInit {
  confirmModal: NzModalRef; // For testing by now
  token: any;
  
  user: User = new User;
  post: Post = new Post();
  name = 'ng2-ckeditor';
  ckeConfig: any;
  mycontent: string;
  log: string = '';
  @ViewChild("myckeditor") ckeditor: any;

  constructor(private postService: PostService, private router: Router, private modal: NzModalService,
    private message: NzMessageService) {
    this.mycontent = `<p>My html content</p>`;
  }

  ngOnInit() {
    this.token = JSON.parse(localStorage.getItem("user")).token;
    this.user = JSON.parse(localStorage.getItem("user")).user;
    let isRole = false;
    this.user.listRole.forEach(e => {
      if (e.id == role_ID.ADMIN || e.id == role_ID.MANAGER) {
        isRole = true;
        return;
      }
    });

    if (!isRole || !this.user) {
      this.router.navigate(["/login"]);
    }
    (document.getElementById("defaultInline1") as HTMLInputElement).checked = true;
    this.ckeConfig = {
      allowedContent: false,
      forcePasteAsPlainText: true,
      font_names: 'Arial;Times New Roman;Verdana',
      toolbarGroups: [
        { name: 'document', groups: ['mode', 'document', 'doctools'] },
        { name: 'clipboard', groups: ['clipboard', 'undo'] },
        { name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing'] },
        { name: 'forms', groups: ['forms'] },
        '/',
        { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
        { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
        { name: 'links', groups: ['links'] },
        { name: 'insert', groups: ['insert'] },
        '/',
        { name: 'styles', groups: ['styles'] },
        { name: 'colors', groups: ['colors'] },
        { name: 'tools', groups: ['tools'] },
        { name: 'others', groups: ['others'] },
        { name: 'about', groups: ['about'] }
      ],
      removeButtons: 'Source,Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Undo,Redo,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Strike,Subscript,Superscript,CopyFormatting,RemoveFormat,Outdent,Indent,CreateDiv,Blockquote,BidiLtr,BidiRtl,Language,Unlink,Anchor,Image,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Maximize,ShowBlocks,About'
    };
  }

  onChange($event: any): void {
    console.log("onChange", $event);
  }

  editPost(formAddPost) {
    console.log("formAddPost: ", formAddPost.value);
    let listStatus = document.getElementsByName("status");
    for (let i = 0; i < listStatus.length; i++) {
      if ((listStatus[i] as HTMLInputElement).checked == true) {
        this.post.status = (listStatus[i] as HTMLInputElement).value;
        break;
      }
    }
    console.log("post: ", this.post);
    this.postService
      ._update(this.token, this.post)
      .subscribe((res: HttpResponse<any>) => {
        let d: any = res;
        console.log("edit", d.data);
        this.router.navigate(["/admin/post"]);
        this.message.success("success");
      });
  }

  validateRequired(value1, value2) {
    document.getElementById(value1).className = "form-control is-invalid";
    document.getElementById(value2).innerHTML = "Không bỏ trống trường này ";
  }

  validate(value1, value2) {
    if (value1 == "title") {
      if (this.post.title == undefined || this.post.title == "") {
        this.validateRequired(value1, value2);
      } else {
        document.getElementById(value1).className = "form-control is-valid";
      }
    } else if (value1 == "brief") {
      if (this.post.brief == undefined || this.post.brief == "") {
        this.validateRequired(value1, value2);
      } else {
        document.getElementById(value1).className = "form-control is-valid";
      }
    }
  }
}
