import { Component, OnInit, Input } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { UploadFile } from "ng-zorro-antd/upload";
import { BookItemService } from 'src/app/service/book-item.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { Group } from 'src/app/model/group';
import { User } from 'src/app/model/user';
import { Router } from '@angular/router';
import { role_ID } from 'src/app/service/url';
import { BookItem } from 'src/app/model/book-item';
import { BookType } from 'src/app/model/book-type';
import { BookTypeService } from 'src/app/service/book-type.service';
import { NzModalRef, NzModalService, NzMessageService } from "ng-zorro-antd";
@Component({
  selector: "app-rooms",
  templateUrl: "./book-item.component.html",
  styleUrls: ["./book-item.component.css"]
})
export class BookItemComponent implements OnInit {
  confirmModal: NzModalRef;
  page = 1;
  pageSize = 8;
  collectionSize;
  booktype_filter: string = "Book Type";
  status_filter: string = "Status";
  booktype_form: string = "Book Type";
  listBookItem: BookItem[] = [];
  listBookItem_Copy: BookItem[] = [];
  listBookType: BookType[] = [];
  bookType: BookType = new BookType();
  bookItem: BookItem = new BookItem();
  user: User = new User;
  sub: Subscription;
  private token: any = "";

  get items(): BookItem[] {
    return this.listBookItem.map((room, i) => ({ id: i + 1, ...room })).slice(
      (this.page - 1) * this.pageSize,
      (this.page - 1) * this.pageSize + this.pageSize
    );
  }

  constructor(
    private bookItemService: BookItemService,
    private modalService: NgbModal,
    private bookTypeService: BookTypeService,
    private router: Router,
    private nzModalService: NzModalService,
    private nzMessageService: NzMessageService
  ) { }


  ngOnInit() {
    this.token = JSON.parse(localStorage.getItem('user')).token;
    this.user = JSON.parse(localStorage.getItem('user')).user;
    //check ADMIN or MANAGER
    let isRole = false;
    this.user.listRole.forEach(e => {
      if (e.id == role_ID.ADMIN || e.id == role_ID.MANAGER) {
        isRole = true;
      }
    });

    if (!isRole || !this.user) {
      this.router.navigate(["/login"]);
    }

    this.getAllBookItem();
    this.getAllBookType();
  }

  getAllBookItem() {
    this.bookItemService._getAll(this.token).subscribe((respone: HttpResponse<any>) => {
      let data: any = respone;
      this.listBookItem = data;
      this.listBookItem_Copy = data;
      if (this.listBookItem.length !== 0) {
        this.collectionSize = this.listBookItem.length;
      }
    },
      (e: HttpErrorResponse) => {

        console.log(e);
      });
  }

  getAllBookType() {
    this.bookTypeService._getAllActive(this.token).subscribe((respone: HttpResponse<any>) => {
      const dataRT: any = respone;
      this.listBookType = dataRT.data;
      this.booktype_form = this.listBookType[0].name;
      console.log("hehehe:", this.booktype_form);
    },
      (err: HttpErrorResponse) => {
        console.log(err);
      });
  }

  getBookItem(id) {

  }

  resetTable() {
    document.getElementById("sort-name").className = "fa fa-sort";
    this.status_filter = "Status";
    this.booktype_filter = "Book Type";
    this.listBookItem = this.listBookItem_Copy;
  }

  openFormAdd(content) {
    this.modalService.open(content);
    this.resetForm();
    (document.getElementById("add-accept") as HTMLButtonElement).disabled = true;
  }

  createBookItem(formCreateItem) {
    let values = formCreateItem.value;
    // let bookType_find = this.listBookType.find(e => e.name == values.bookTypeName);
    // let index = this.listBookType.indexOf(bookType_find);
    // this.bookType.id = this.listBookType[index].id;
    // this.bookItem.bookType = this.bookType;
    for (let i = 0; i < this.listBookType.length; i++) {
      if (this.listBookType[i].name == values.bookTypeName) {
        this.bookType.id = this.listBookType[i].id;
        this.bookItem.bookType = this.bookType;
        break;
      }
    }

    let listStatus = document.getElementsByName("status");
    for (let i = 0; i < listStatus.length; i++) {
      if ((listStatus[i] as HTMLInputElement).checked == true) {
        this.bookItem.status = (listStatus[i] as HTMLInputElement).value;
        break;
      }
    }
    console.log("Items Add: ", this.bookItem);
    this.sub = this.bookItemService._create(this.token, this.bookItem)
      .subscribe((res: HttpResponse<any>) => {
        let datas: any = res;
        datas.data.id = this.listBookItem[this.listBookItem.length - 1].id + 1;
        this.listBookItem = [...this.listBookItem, datas.data];
        this.collectionSize = this.listBookItem.length;
        this.nzMessageService.success("Add successful Item!");
      }
      );
  }

  resetForm() {
    let bookItem_reset: BookItem = new BookItem();
    this.bookItem = bookItem_reset;
    (document.getElementById("form-add") as HTMLFormElement).reset();
    this.booktype_form = this.listBookType[0].name;
    (document.getElementById("defaultInline1") as HTMLInputElement).checked = true;
  }

  openFormEdit(content, bookItem) {
    this.modalService.open(content);
    this.resetForm();
    this.bookItem = bookItem;
    if (bookItem.status == "ACTIVE") {
      (document.getElementById("defaultInline1") as HTMLInputElement).checked = true;
    } else if (bookItem.status == "INACTIVE") {
      (document.getElementById("defaultInline2") as HTMLInputElement).checked = true;
    }
    this.booktype_form = bookItem.bookType.name;
    (document.getElementById("add-accept") as HTMLButtonElement).disabled = false;
  }

  updateBookItem(formCreateItem) {
    let values = formCreateItem.value;
    for (let i = 0; i < this.listBookType.length; i++) {
      if (this.listBookType[i].name == values.bookTypeName) {
        this.bookType.id = this.listBookType[i].id;
        this.bookItem.bookType = this.bookType;
        break;
      }
    }

    let listStatus = document.getElementsByName("status");
    for (let i = 0; i < listStatus.length; i++) {
      if ((listStatus[i] as HTMLInputElement).checked == true) {
        this.bookItem.status = (listStatus[i] as HTMLInputElement).value;
        break;
      }
    }
    console.log("Items Edit: ", this.bookItem);
    this.bookItemService._update(this.token, this.bookItem)
      .subscribe((respone: HttpResponse<any>) => {
        const datas: any = respone;
        let bookItem_find = this.listBookItem.find(e => e.id == this.bookItem.id);
        let index = this.listBookItem.indexOf(bookItem_find);
        this.listBookItem[index] = datas.data;
        this.nzMessageService.success("Edit successful Item!");
      },
        (e: HttpErrorResponse) => {
          console.log(e);
        });
  }

  deleteBookItem(bookItem) {
    this.nzModalService.confirm({
      nzTitle: "Are you sure delete this Item?",
      nzOkText: "Yes",
      nzOkType: "danger",
      nzOnOk: () => {
        this.bookItemService._delete(this.token, bookItem.id).subscribe(
          (res: HttpResponse<any>) => {
            let d: any = res;
            if (d.data.delete == "OK") {
              let bookItem_find = this.listBookItem.find(e => e.id == bookItem.id);
              let index = this.listBookItem.indexOf(bookItem_find);
              this.listBookItem[index].status = "INACTIVE";
              // this.collectionSize = this.listUsers.length;
              this.nzMessageService.success("Delete successful Item!");
            }
          },
          error => {
            console.log("Error", error);
          }
        );
      },
      nzCancelText: "Cancel",
    });
  }

  filterByAll1() {
    let data = this.listBookItem_Copy.filter(b => {
      return (this.status_filter != 'Status' ? b.status == this.status_filter : true) && (this.booktype_filter != 'Book Type' ? b.bookType.name == this.booktype_filter : true)
    })
    console.log("condition", this.booktype_filter, this.status_filter);
    console.log("data, listUsers_copy", data, this.listBookItem_Copy);
    this.listBookItem = data;
    this.collectionSize = this.listBookItem.length;
  }

  sort(fieldSort) {
    let d = 0;
    if (document.getElementById(fieldSort).className == "fa fa-sort" && d == 0) {
      d++;
      document.getElementById(fieldSort).className = "fa fa-sort-asc";
      this.listBookItem.sort(function (a, b) {
        return a.name.localeCompare(b.name);
      });
    }

    if (document.getElementById(fieldSort).className == "fa fa-sort-asc" && d == 0) {
      d++;
      document.getElementById(fieldSort).className = "fa fa-sort-desc";
      this.listBookItem.sort(function (a, b) {
        return b.name.localeCompare(a.name);
      });
    }

    if (document.getElementById(fieldSort).className == "fa fa-sort-desc" && d == 0) {
      d++;
      document.getElementById(fieldSort).className = "fa fa-sort";
      this.getAllBookItem();
    }
  }

  validate(value1, value2) {
    if (value1 == "name") {
      if (this.bookItem.name == undefined || this.bookItem.name == "") {
        document.getElementById(value1).className = "form-control is-invalid";
        document.getElementById(value2).innerHTML = "Không bỏ trống trường này ";
        (document.getElementById("add-accept") as HTMLButtonElement).disabled = true;
      } else {
        document.getElementById(value1).className = "form-control is-valid";
        (document.getElementById("add-accept") as HTMLButtonElement).disabled = false;
      }
    } else if (value1 == "description") {
      if (this.bookItem.description == undefined || this.bookItem.description == "") {
        document.getElementById(value1).className = "form-control is-valid";
        document.getElementById(value2).innerHTML = "Có thể bỏ trống trường này";
      } else {
        document.getElementById(value1).className = "form-control is-valid";
        document.getElementById(value2).innerHTML = "";
      }
    }
  }
}


