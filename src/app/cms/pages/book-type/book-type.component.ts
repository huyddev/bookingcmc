import { Component, OnInit } from '@angular/core';
import { BookTypeService } from 'src/app/service/book-type.service';
import { User } from 'src/app/model/user';
import { NzModalRef, NzModalService, NzMessageService } from 'ng-zorro-antd';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { role_ID } from 'src/app/service/url';
import { HttpResponse } from '@angular/common/http';
import { BookType } from 'src/app/model/book-type';

@Component({
  selector: 'fmyp-book-type',
  templateUrl: './book-type.component.html',
  styleUrls: ['./book-type.component.css']
})
export class BookTypeComponent implements OnInit {
  token: any;
  user: User = new User;
  bookingTypes: BookType[] = [];
  bookingTypesFilter: BookType[] = [];
  confirmModal: NzModalRef; // For testing by now
  status_filter: string = "Status";
  loading: boolean = false;

  constructor(
    private modalService: NgbModal,
    private bookingTypeService: BookTypeService,
    private modal: NzModalService,
    private message: NzMessageService,
    private router: Router) { }

  ngOnInit() {
    this.token = JSON.parse(localStorage.getItem("user")).token;
    this.user = JSON.parse(localStorage.getItem("user")).user;
    let isRole = false;
    this.user.listRole.forEach(e => {
      if (e.id == role_ID.MEMBER) {
        isRole = true;
        return;
      }
    });

    if (isRole || !this.user) {
      this.router.navigate(["/login"]);
    }
    this.getAllBookingType();
  }
  getAllBookingType() {
    this.loading = true;
    this.bookingTypeService._getAll(this.token).subscribe(
      (res: HttpResponse<any>) => {
        let d: any = res;
        this.bookingTypes = d.data;
        this.bookingTypesFilter = d.data;
        console.log("list booking types: ", this.bookingTypes);
      },
      error => {
        console.log("Error", error);
        this.loading = false;
      },
      () =>
        this.loading = false
    );
  }

  addBookingType(formData) {
    console.log("Add Form: ", formData);
    this.bookingTypeService
      ._create(this.token, formData)
      .subscribe((res: HttpResponse<any>) => {
        let d: any = res;
        console.log("add: ", d.data);
        d.data.id = this.bookingTypes[this.bookingTypes.length - 1].id + 1;
        this.bookingTypes = [...this.bookingTypes, d.data];
      });
  }

  editBookingType(formData) {
    //const data = { id: role.id, name: role.name };
    console.log("Edit Form: ", formData);
    this.bookingTypeService._update(this.token, formData).subscribe(
      (res: HttpResponse<any>) => {
        let d: any = res;
        let bookingType = this.bookingTypes.find(u => u.id == formData.id);
        let index = this.bookingTypes.indexOf(bookingType);
        this.bookingTypes[index] = d.data;
        this.message.success("Success");
      },
      error => {
        console.log("Error", error);
      }
    );
  }

  deleteBookingType(id) {
    this.modal.confirm({
      nzTitle: "Are you sure delete this booking type?",
      //nzContent: '<b style="color: red;">Some descriptions</b>',
      nzOkText: "Yes",
      nzOkType: "danger",
      nzOnOk: () => {
        this.bookingTypeService._delete(this.token, id).subscribe(
          (res: HttpResponse<any>) => {
            let d: any = res;
            let bookingType = this.bookingTypes.find(u => u.id == id);
            let index = this.bookingTypes.indexOf(bookingType);
            this.bookingTypes[index].status = "INACTIVE";
            this.message.success("Success");
          },
          error => {
            console.log("Error", error);
          }
        );
      },
      nzCancelText: "No",
      nzOnCancel: () => console.log("Cancel")
    });
  }

  open(content) {
    this.modalService.open(content);
  }

  resetSortEvent() {
    if (document.getElementById("sort-name").className != "fa fa-sort") {
      document.getElementById("sort-name").className = "fa fa-sort";
    }
  }

  sort(fieldSort) {
    let d = 0;
    if (document.getElementById(fieldSort).className == "fa fa-sort" && d == 0) {
      d++;
      document.getElementById(fieldSort).className = "fa fa-sort-asc";
      this.bookingTypes.sort(function (a, b) {
        return a.name.localeCompare(b.name);
      });
    }

    if (document.getElementById(fieldSort).className == "fa fa-sort-asc" && d == 0) {
      d++;
      document.getElementById(fieldSort).className = "fa fa-sort-desc";
      this.bookingTypes.sort(function (a, b) {
        return b.name.localeCompare(a.name);
      });
    }

    if (document.getElementById(fieldSort).className == "fa fa-sort-desc" && d == 0) {
      d++;
      document.getElementById(fieldSort).className = "fa fa-sort";
      this.getAllBookingType();
    }
  }

  filterByAll1() {
    let data = this.bookingTypesFilter.filter(b => {
      return this.status_filter != 'Status' ? b.status == this.status_filter : true
    })
    console.log("data, listFilter", data, this.bookingTypesFilter)
    this.bookingTypes = data;
  }

  resetTable() {
    this.resetSortEvent();
    this.status_filter = "Status";
    this.bookingTypes = this.bookingTypesFilter;
  }
}
