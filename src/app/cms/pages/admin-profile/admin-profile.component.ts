import { Component, OnInit } from "@angular/core";
import { User } from "src/app/model/user";
import { NzMessageService, da_DK } from "ng-zorro-antd";
import { AdminProfileService } from "src/app/service/admin-profile.service";
import { GroupService } from 'src/app/service/group.service';
import { Group } from 'src/app/model/group';
import { role_ID } from 'src/app/service/url';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';
import { ExcelService } from 'src/app/service/excel.service';

@Component({
  selector: "app-admin-profile",
  templateUrl: "./admin-profile.component.html",
  styleUrls: ["./admin-profile.component.css"]
})
export class AdminProfileComponent implements OnInit {
  user: User = new User();
  groups: Group[] = [];
  token = JSON.parse(localStorage.getItem("user")).token;
  userId = JSON.parse(localStorage.getItem("user")).user.id;

  constructor(
    private adminProfileService: AdminProfileService,
    private message: NzMessageService,
    private groupService: GroupService,
    private userService: UserService,
    private router: Router,
    private excelService: ExcelService
  ) { }


  getAllGroup() {
    this.groupService._getAllGroup().subscribe(
      data => {
        const d: any = data;
        this.groups = d.data;
        console.log("group: ", this.groups);
      },
      error => {
        console.log("error", error);
      }
    );
  }

  compareFn = (o1: any, o2: any) => (o1 && o2 ? o1.id === o2.id : o1 === o2);

  ngOnInit() {
    //check ADMIN or MANAGER
    this.token = JSON.parse(localStorage.getItem("user")).token;
    this.user = JSON.parse(localStorage.getItem("user")).user;
    let isRole = false;
    this.user.listRole.forEach(e => {
      if (e.id == role_ID.ADMIN || e.id == role_ID.MANAGER) {
        isRole = true;
        return;
      }
    });

    if (!isRole || !this.user) {
      this.router.navigate(["/login"]);
    }
    this.getUser();
    this.getAllGroup();
  }

  getUser() {
    this.userService._getUserById(this.token, this.userId).subscribe(
      data => {
        const d: any = data;
        this.user = d.data;
        console.log("User: ", this.user)
      }, error => {
        console.log("error", error);
        this.message.error("Error ");
      }
    );
  }

  submitForm(formValue) {
    console.log("formData: ", formValue);
    this.adminProfileService.changeProfile(formValue, this.token).subscribe(
      data => {
        console.log("data: ", data);
        this.message.success("Complete ");
      },
      error => {
        console.log("error", error);
        this.message.error("Error ");
      }
    );
  }

  exportToExcel() { this.excelService.exportAsExcelFile(this.groups, 'sample') }
}
