import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/user';
import { HttpResponse } from '@angular/common/http';
import { DashboardService } from 'src/app/service/dashboard.service';
import { Dashboard } from 'src/app/model/dashboard';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { role_ID } from 'src/app/service/url';
import { GroupService } from 'src/app/service/group.service';
import { Group } from 'src/app/model/group';
import { NzMessageService } from 'ng-zorro-antd';
import { BookTypeService } from 'src/app/service/book-type.service';
import { BookType } from 'src/app/model/book-type';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  user: User = new User;
  dashBoard: Dashboard = new Dashboard;
  listDashboard: Dashboard[];
  groups: Group[];
  bookTypes: BookType[];
  selectGroup: any = "All Group";
  dateRange: any[] = [moment().startOf('month').format("YYYY-MM-DD[T]HH:mm:ss"), moment().format("YYYY-MM-DD[T]HH:mm:ss")];
  private token: string;
  checkUser: boolean = true;

  constructor(
    private bookTypeService: BookTypeService,
    private groupService: GroupService,
    private dashboardService: DashboardService,
    private router: Router,
    private message: NzMessageService
  ) { }

  ngOnInit() {
    let userToken = JSON.parse(localStorage.getItem('user')) || {};
    this.token = userToken.token;
    this.user = userToken.user;
    this.dashBoard.group = null;
    this.dashBoard.timeFrom = this.dateRange[0];
    this.dashBoard.timeTo = this.dateRange[1];
    console.log("Dashboard", this.dashBoard);
    // Check role ADMIN or MANAGER
    let isRole = false;
    this.user.listRole.forEach(e => {
      if (e.id == role_ID.ADMIN || e.id == role_ID.MANAGER) {
        isRole = true;
      }
    });

    if (!isRole || !this.user) {
      this.router.navigate(["/login"]);
    } else {

      console.log('Token', this.token);
      this.dashboardService._getDashboard(this.token, this.dashBoard).subscribe((res: HttpResponse<any>) => {
        let d: any = res;
        this.listDashboard = d.data;

        console.log("DashBoard", this.listDashboard)
      },
        (error) => {
          this.message.error('Data Dashboard Null');
        })

      // Get Group
      this.groupService._getAllGroup().subscribe(
        (res: HttpResponse<any>) => {
          let d: any = res;
          this.groups = d.data;
          console.log("Groups :", this.groups);
        },
        (error) => {
          this.message.error('List group null');
        }
      );

      //Get BookType
      this.bookTypeService._getAllActive(this.token).subscribe(
        (res: HttpResponse<any>) => {
          let d: any = res;
          this.bookTypes = d.data;
          console.log("Book Type: ", this.bookTypes);
        },
        (error) => {
          this.message.error('List Book Type null');
        }
      )
    }
  }
  onChangeDate(result: Date[]): void {
    this.dashBoard.timeFrom = moment(result[0]).format("YYYY-MM-DD[T]HH:mm:ss");
    this.dashBoard.timeTo = moment(result[1]).format("YYYY-MM-DD[T]HH:mm:ss");
    this.dashboardService._getDashboard(this.token, this.dashBoard).subscribe((res: HttpResponse<any>) => {
      let d: any = res;
      this.listDashboard = d.data;
      console.log("DashBoard", this.listDashboard)
    },
      (error) => {
        this.message.error('Data Dashboard Null');
      })
  }
  onChangeGroup() {
    console.log("selectGroup", this.selectGroup);
    if (this.selectGroup == 'All Group') {
      this.dashBoard.group = null;
      this.dashboardService._getDashboard(this.token, this.dashBoard).subscribe((res: HttpResponse<any>) => {
        let d: any = res;
        this.listDashboard = d.data;
        console.log("DashBoard", this.listDashboard)
      },
        (error) => {
          this.message.error('Data Dashboard Null');
        })
    } else {
      this.dashBoard.group = {id:this.selectGroup};
      this.dashboardService._getDashboard(this.token, this.dashBoard).subscribe((res: HttpResponse<any>) => {
        let d: any = res;
        this.listDashboard = d.data;
        console.log("DashBoard", this.listDashboard)
      },
        (error) => {
          this.message.error('Data Dashboard Null');
        })
    }
  }

}
