import { Component, OnInit } from "@angular/core";
import { NzModalRef, NzModalService, NzMessageService } from "ng-zorro-antd";
import { Post } from "src/app/model/post";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { HttpResponse } from "@angular/common/http";
import { User } from 'src/app/model/user';
import { role_ID, _url } from 'src/app/service/url';
import { Router } from '@angular/router';
import { PostService } from 'src/app/service/post.service';

@Component({
  selector: 'fmyp-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  token: any;
  user: User = new User;
  status_filter: string = "Status";
  loading: boolean = false;
  confirmModal: NzModalRef; // For testing by now
  posts: Post[] = [];
  listPosts_copy: Post[] = [];
  post: Post = new Post();
  URL = _url.domain + 'downloadFile/';
  constructor(
    private modalService: NgbModal,
    private postService: PostService,
    private modal: NzModalService,
    private message: NzMessageService,
    private router: Router
  ) { }

  ngOnInit() {
    this.token = JSON.parse(localStorage.getItem("user")).token;
    this.user = JSON.parse(localStorage.getItem("user")).user;
    let isRole = false;
    this.user.listRole.forEach(e => {
      if (e.id == role_ID.ADMIN || e.id == role_ID.MANAGER) {
        isRole = true;
        return;
      }
    });

    if (!isRole || !this.user) {
      this.router.navigate(["/login"]);
    }
    this.getAllPosts();
  }
  getAllPosts() {
    this.loading = true;
    this.postService._getAll(this.token).subscribe(
      (res: HttpResponse<any>) => {
        let d: any = res;
        this.posts = d.data;
        console.log("list Post: ", this.posts);
      },
      error => {
        console.log("Error", error);
        this.loading = false;
      },
      () => this.loading = false
    );
  }
  detailPost(postForm) {
    console.log("Detail Form: ", postForm);
    this.postService._update(this.token, postForm).subscribe(
      (res: HttpResponse<any>) => {
        let d: any = res;
        let post = this.posts.find(u => u.id == postForm.id);
        let index = this.posts.indexOf(post);
        this.posts[index] = d.data;
        this.message.success("Success");
      },
      error => {
        console.log("Error", error);
      }
    );
  }
  deletePost(post) {
    this.modal.confirm({
      nzTitle: "Are you sure delete this post?",
      nzOkText: "Yes",
      nzOkType: "danger",
      nzOnOk: () => {
        this.postService._delete(this.token, post.id).subscribe(
          (res: HttpResponse<any>) => {
            let d: any = res;
            let post_find = this.posts.find(e => e.id == post.id);
            let index = this.posts.indexOf(post_find);
            this.posts[index].status = "INACTIVE";
            this.message.success("Delete successful post!");

          },
          error => {
            console.log("Error", error);
          }
        );
      },
      nzCancelText: "Cancel",
    });
  }

  resetSortEvent() {
    if (document.getElementById("sort-name").className != "fa fa-sort") {
      document.getElementById("sort-name").className = "fa fa-sort";
    }
  }
  open(content) {
    this.modalService.open(content);
  }

  sort(fieldSort) {
    let d = 0;
    if (document.getElementById(fieldSort).className == "fa fa-sort" && d === 0) {
      d++;
      document.getElementById(fieldSort).className = "fa fa-sort-asc";
      this.posts.sort(function (a, b) {
        return a.title.localeCompare(b.title);
      });
    }

    if (document.getElementById(fieldSort).className == "fa fa-sort-asc" && d === 0) {
      d++;
      document.getElementById(fieldSort).className = "fa fa-sort-desc";
      this.posts.sort(function (a, b) {
        return b.title.localeCompare(a.title);
      });
    }

    if (document.getElementById(fieldSort).className == "fa fa-sort-desc" && d === 0) {
      d++;
      document.getElementById(fieldSort).className = "fa fa-sort";
      this.getAllPosts();
    }
  }
}
