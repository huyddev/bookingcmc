import { Component, OnInit } from '@angular/core';
import { AdminProfileService } from 'src/app/service/admin-profile.service';
import { ActivatedRoute } from '@angular/router';
import { NzMessageService, NzMessageConfig, NZ_MESSAGE_CONFIG } from 'ng-zorro-antd';
import { User } from 'src/app/model/user';

@Component({
  selector: 'fmyp-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  passwordVisible = false;
  password: string;
  validPassword = false;
  user: User = new User();
  token = JSON.parse(localStorage.getItem("user")).token;
  oldPass: any;

  constructor(
    private message: NzMessageService,
    private adminProfileService: AdminProfileService
  ) { }



  changePassword(newPassword) {
    const formData = { password: newPassword }
    console.log("formData", formData);

    this.adminProfileService.setNewPassword(formData, this.token).subscribe(
      data => {
        console.log("data", data.data);
        this.message.success("success");
      },
      error => {
        this.message.error("fail"),
          console.log("error", error);
      });
  }

  checkPassword() {
    const formData = { "password": this.oldPass };
    console.log("data", formData);
    this.adminProfileService.checkOldPassword(formData, this.token).subscribe(
      data => {
        console.log("data", data);
        if (data.message == "PASSWORD VERIFIED") {
          this.validPassword = true;

        } else {
          this.validPassword = false;
          this.message.error("Fail");
        }
      },
      error => {
        console.log("Password is wrong", error);
      }
    );
  }

  ngOnInit() {
  }

}
