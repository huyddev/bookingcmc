import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { UploadFile } from "ng-zorro-antd/upload";
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { User } from 'src/app/model/user';
import { role_ID } from 'src/app/service/url';
import { Router } from '@angular/router';
import { UserBook } from 'src/app/model/user-book';
import { BookType } from 'src/app/model/book-type';
import { BookItem } from 'src/app/model/book-item';
import { UserBookService } from 'src/app/service/user-book.service';
import { BookTypeService } from 'src/app/service/book-type.service';
import { BookItemService } from 'src/app/service/book-item.service';
import { NzModalRef, NzModalService, NzMessageService } from "ng-zorro-antd";

@Component({
  selector: 'fmyp-booking-rooms',
  templateUrl: './user-book.component.html',
  styleUrls: ['./user-book.component.css'],
  providers: [DatePipe]
})
export class UserBookComponent implements OnInit {
  confirmModal: NzModalRef;
  admin: User;

  typeDisplay: string = "Week";
  bookType: string = "Book Type";
  bookItem: string = "Book Item";
  status: string = "Status";
  datePick;
  LISTBOOKING_LUUTAM: UserBook[] = [];
  LISTBOOKING: UserBook[] = [];
  listbooking: UserBook[] = [];
  LISTROOMTYPE: BookType[] = [];
  LISTROOM: BookItem[] = [];
  token: string = "";
  currentPage;
  currentPage2 = 1;
  pageFirstPending: number = 0;
  statusPagePending: boolean = true;


  page = 1;
  pageSize = 25;
  collectionSize = 10;

  // get listbooking(): UserBook[] {
  //   return this.LISTBOOKING.map((booking, i) => ({ id: i + 1, ...booking })).slice(
  //     (this.page - 1) * this.pageSize,
  //     (this.page - 1) * this.pageSize + this.pageSize
  //   );
  // }

  constructor(
    private modalService: NgbModal,
    private userBookService: UserBookService,
    private datePipe: DatePipe,
    private bookTypeService: BookTypeService,
    private bookItemService: BookItemService,
    private router: Router,
    private nzModalService: NzModalService,
    private nzMessageService: NzMessageService
  ) { }

  ngOnInit() {
    // let today = new Date();
    // let dd = String(today.getDate()).padStart(2, '0');
    // let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    // let yyyy = today.getFullYear();
    // let today2: string = yyyy + "-" + mm + "-" + dd;

    //check ADMIN or MANAGER

    let userToken = JSON.parse(localStorage.getItem('user')) || {};
    this.token = userToken.token;
    this.admin = userToken.user;

    //check role ADMIN or MANAGER
    let isRole = false;
    this.admin.listRole.forEach(e => {
      if (e.id == role_ID.ADMIN || e.id == role_ID.MANAGER) {
        isRole = true;
        return;
      }
    });

    if (!isRole || !this.admin) {
      this.router.navigate(["/login"]);
    }

    this.datePick = new Date().toISOString().split('T')[0];
    this.getListBookingByWeek();
    this.getListBookType();
    this.getListBookItem();
  }

  checkPendingOfList() {
    let d = 0;
    for (let i = 0; i < this.listbooking.length; i++) {
      if (this.listbooking[i].status === "PENDING") {
        d++;
        this.statusPagePending = true;
        break;
      }
    }
    if (d == 0) {
      this.statusPagePending = false;
    }
  }

  eventButton() {
    this.checkPendingOfList();
    if (!this.statusPagePending) {
      this.pageFirstPending = 0;
    }
    let dem = 0;
    for (let i = 0; i < this.listbooking.length; i++) {
      if (this.listbooking[i].status === "PENDING" && dem === 0 && (this.pageFirstPending == 0 || this.currentPage2 == this.page)) {
        this.pageFirstPending++;
        this.currentPage2 = this.page;
        dem++;
        this.listbooking[i].isDisabled = false;
      }
      else if (this.listbooking[i].status === "REJECT" || this.listbooking[i].status === "APPROVE") {
        console.log("dang vao day");
        this.listbooking[i].isDisabled = false;
        // this.LISTBOOKING[i-1].isDisabled = false;
      }
      else {
        this.listbooking[i].isDisabled = true;
        // document.getElementById("accept").style.backgroundColor = "black";
      }
    }
  }

  getListBookType() {
    this.bookTypeService._getAllActive(this.token).subscribe((res: HttpResponse<any>) => {
      let d: any = res;
      this.LISTROOMTYPE = d.data;
      console.log("list room type: ", this.LISTROOMTYPE);
    },
      (error) => {
        console.log('Error', error);
      });
  }

  getListBookItem() {
    this.bookItemService._getAll(this.token).subscribe((res: HttpResponse<any>) => {
      let d: any = res;
      this.LISTROOM = d;
      console.log("List Book Item Filter: ", this.LISTROOM);
    },
      (error) => {
        console.log('Error', error);
      });
  }

  getListBookingByDay() {
    this.currentPage = this.page;
    console.log("Ngày chọn: ", this.datePick);
    let body: any = {
      "date": this.datePipe.transform(this.datePick, 'yyyy-MM-dd'),
      "size": this.pageSize,
      "selectIndex": this.page - 1
    };
    this.userBookService._getByDayAdmin(this.token, body).subscribe((res: HttpResponse<any>) => {
      let d: any = res;
      console.log("res day: ", d);
      this.listbooking = d.data.list;
      this.LISTBOOKING_LUUTAM = d.data.list;
      this.filterByAll1();
      this.collectionSize = d.data.sizePages * 10;
      console.log("List User Book By Day: ", this.listbooking);
      this.eventButton();
    },
      (error) => {
        console.log('Error', error);
      });
  }

  getListBookingByWeek() {
    this.currentPage = this.page;
    console.log("pickDate: ", this.datePick);
    let body: any = {
      "date": this.datePipe.transform(this.datePick, 'yyyy-MM-dd'),
      "size": this.pageSize,
      "selectIndex": this.page - 1
    };
    this.userBookService._getByWeekAdmin(this.token, body).subscribe((res: HttpResponse<any>) => {
      let d: any = res;
      console.log("res week: ", d);
      this.listbooking = d.data.list;
      this.LISTBOOKING_LUUTAM = d.data.list;
      this.filterByAll1();
      this.collectionSize = d.data.sizePages * 10;
      console.log("List Booking By Week: ", this.listbooking);
      this.eventButton();
    },
      (error) => {
        console.log('Error', error);
      });
  }

  getListBookingByMonth() {
    this.currentPage = this.page;
    let body: any = {
      "date": this.datePipe.transform(this.datePick, 'yyyy-MM-dd'),
      "size": this.pageSize,
      "selectIndex": this.page - 1
    };
    this.userBookService._getByMonthAdmin(this.token, body).subscribe((res: HttpResponse<any>) => {
      let d: any = res;
      console.log("res month: ", d);
      this.listbooking = d.data.list;
      this.LISTBOOKING_LUUTAM = d.data.list;
      this.filterByAll1();
      this.collectionSize = d.data.sizePages * 10;
      console.log("List Booking By Month: ", this.listbooking);
      this.eventButton();
    },
      (error) => {
        console.log('Error', error);
      });
  }

  open(content) {
    this.modalService.open(content);
  }

  rejectClick(booking) {
    this.nzModalService.confirm({
      nzTitle: "Are you sure reject this booking item?",
      nzOkText: "Yes",
      nzOkType: "danger",
      nzOnOk: () => {
        this.userBookService._updateStatusAdmin(this.token, booking.id, "REJECT").subscribe((res: HttpResponse<any>) => {
          let d: any = res;
          let booking_find = this.listbooking.find(e => e.id == booking.id);
          let index = this.listbooking.indexOf(booking_find);
          this.listbooking[index] = d.data[0];
          this.collectionSize = this.listbooking.length;
          console.log("List Booking: ", this.listbooking);
          this.eventButton();
          this.nzMessageService.success("Reject successful this booking item!");
        },
          (error) => {
            console.log('Error', error);
          });
      },
      nzCancelText: "Cancel",
    });
  }

  acceptClick(booking) {
    this.nzModalService.confirm({
      nzTitle: "Are you sure approve this booking item?",
      nzOkText: "Yes",
      nzOkType: "primary",
      nzOnOk: () => {
        this.userBookService._updateStatusAdmin(this.token, booking.id, "APPROVE").subscribe((res: HttpResponse<any>) => {
          let d: any = res;
          d.data.forEach(e1 => {
            let booking_find = this.listbooking.find(e2 => e2.id == e1.id);
            let index = this.listbooking.indexOf(booking_find);
            this.listbooking[index] = e1;
          });
          // this.LISTBOOKING = d.data;
          this.collectionSize = this.listbooking.length;
          console.log("List Booking sau khi Approve: ", this.listbooking);
          this.eventButton();
          this.nzMessageService.success("Approve successful this booking item!");
        },
          (error) => {
            console.log('Error', error);
          });
      },
      nzCancelText: "Cancel",
    });
  }

  getBookingSameTime(booking) {
    this.userBookService._getBookingSameTime(this.token, booking).subscribe((res: HttpResponse<any>) => {
      let d: any = res;
      this.listbooking = d.data;
      this.collectionSize = this.listbooking.length;
      console.log("List Booking: ", this.listbooking);
      this.eventButton();
    });
  }

  displayDataFromDate() {
    if (this.page > this.currentPage + 1 || this.page < this.currentPage - 1) {
      this.page = this.currentPage;
      console.log("current page, page: ", this.currentPage, this.page)
      alert("Only next one page!");
    } else {
      if (this.typeDisplay == "Day") {
        this.getListBookingByDay();
      }
      if (this.typeDisplay == "Week") {
        this.getListBookingByWeek();
      }
      if (this.typeDisplay == "Month") {
        this.getListBookingByMonth();
      }
    }
    // if (this.typeDisplay == "Day") {
    //   this.getListBookingByDay();
    // }
    // if (this.typeDisplay == "Week") {
    //   this.getListBookingByWeek();
    // }
    // if (this.typeDisplay == "Month") {
    //   this.getListBookingByMonth();
    // }
  }

  filterByAll1() {
    let data = this.LISTBOOKING_LUUTAM.filter(b => {
      return (this.status != 'Status' ? b.status == this.status : true) && (this.bookItem != 'Book Item' ? b.bookItem.name == this.bookItem : true) && (this.bookType != 'Book Type' ? b.bookItem.bookType.name == this.bookType : true)
    })
    console.log("condition", this.status, this.bookItem, this.bookType)
    console.log("data", data)
    this.listbooking = data;
  }

  resetTable() {
    this.bookType = "Book Type";
    this.bookItem = "Book Item";
    this.bookType = "Book Type";
    this.status = "Status";
    this.listbooking = this.LISTBOOKING_LUUTAM;
  }
}
