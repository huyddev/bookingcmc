import { Component, OnInit } from "@angular/core";
import { RoleService } from "src/app/service/role.service";
import { NzModalRef, NzModalService, NzMessageService } from "ng-zorro-antd";
import { Role } from "src/app/model/role";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { HttpResponse } from "@angular/common/http";
import { User } from 'src/app/model/user';
import { role_ID } from 'src/app/service/url';
import { Router } from '@angular/router';

@Component({
  selector: "app-role",
  templateUrl: "./role.component.html",
  styleUrls: ["./role.component.css"]
})
export class RoleComponent implements OnInit {
  token: any;
  user: User = new User;
  loading: boolean = false;
  confirmModal: NzModalRef; // For testing by now
  roles: Role[] = [];
  role: Role = new Role();

  constructor(
    private modalService: NgbModal,
    private roleService: RoleService,
    private modal: NzModalService,
    private message: NzMessageService,
    private router: Router
  ) { }

  ngOnInit() {
    this.token = JSON.parse(localStorage.getItem("user")).token;
    this.user = JSON.parse(localStorage.getItem("user")).user;
    let isRole = false;
    this.user.listRole.forEach(e => {
      if (e.id == role_ID.ADMIN || e.id == role_ID.MANAGER) {
        isRole = true;
        return;
      }
    });

    if (!isRole || !this.user) {
      this.router.navigate(["/login"]);
    }
    this.getAllRoles();
  }

  getAllRoles() {
    this.loading = true;
    this.roleService._getAllRole(this.token).subscribe(
      (res: HttpResponse<any>) => {
        let d: any = res;
        this.roles = d;
        console.log("list Role: ", this.roles);
      },
      error => {
        console.log("Error", error);
        this.loading = false;
      },
      () => this.loading = false
    );
  }

  addRole(role) {
    console.log("Add Form: ", role);
    this.roleService
      ._createRole(this.token, role)
      .subscribe((res: HttpResponse<any>) => {
        let d: any = res;
        console.log("add", d.data);
        this.roles = [...this.roles, d.data];
        this.message.success("success");
      });
  }

  editRole(roleForm) {
    console.log("Edit Form: ", roleForm);
    this.roleService._updateRole(this.token, roleForm).subscribe(
      (res: HttpResponse<any>) => {
        let d: any = res;
        let role = this.roles.find(u => u.id == roleForm.id);
        let index = this.roles.indexOf(role);
        this.roles[index] = d.data;
        this.message.success("Success");
      },
      error => {
        console.log("Error", error);
      }
    );
  }

  deleteRole(roleId) {
    this.modal.confirm({
      nzTitle: "Are you sure delete this role?",
      //nzContent: '<b style="color: red;">Some descriptions</b>',
      nzOkText: "Yes",
      nzOkType: "danger",
      nzOnOk: () => {
        this.roleService._deleteRole(this.token, roleId).subscribe(
          (res: HttpResponse<any>) => {
            let d: any = res;
            this.roles = this.roles.filter(u => u.id != roleId);
            this.message.success("Success");
          },
          error => {
            console.log("Error", error);
          }
        );
      },
      nzCancelText: "No",
      nzOnCancel: () => console.log("Cancel")
    });
  }

  open(content) {
    this.modalService.open(content);
  }

  resetSortEvent() {
    if (document.getElementById("sort-name").className != "fa fa-sort") {
      document.getElementById("sort-name").className = "fa fa-sort";
    }
  }

  sort(fieldSort) {
    let d = 0;
    if (document.getElementById(fieldSort).className == "fa fa-sort" && d == 0) {
      d++;
      document.getElementById(fieldSort).className = "fa fa-sort-asc";
      this.roles.sort(function (a, b) {
        return a.name.localeCompare(b.name);
      });
    }

    if (document.getElementById(fieldSort).className == "fa fa-sort-asc" && d == 0) {
      d++;
      document.getElementById(fieldSort).className = "fa fa-sort-desc";
      this.roles.sort(function (a, b) {
        return b.name.localeCompare(a.name);
      });
    }

    if (document.getElementById(fieldSort).className == "fa fa-sort-desc" && d == 0) {
      d++;
      document.getElementById(fieldSort).className = "fa fa-sort";
      this.getAllRoles();
    }
  }
}
