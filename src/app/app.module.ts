import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { BookingModule } from "./booking/booking.module";
import { CmsModule } from "./cms/cms.module";
import { ScrollingModule } from "@angular/cdk/scrolling";
import { NgZorroAntdModule, NZ_I18N, en_US } from "ng-zorro-antd";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { registerLocaleData } from "@angular/common";
import en from "@angular/common/locales/en";
import { ExcelService } from './service/excel.service';
// import { NavbarComponent } from './cms/';

registerLocaleData(en);

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BookingModule,
    CmsModule,
    FormsModule,
    NgZorroAntdModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ScrollingModule,
    NgbModule,
    NgZorroAntdModule,
    NgbModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }, ExcelService],
  bootstrap: [AppComponent]
})
export class AppModule { }
